//
//  MyContacts.h
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyContacts : NSObject

@property (strong, nonatomic) NSMutableArray *myUsersArray;

+ (instancetype)sharedInstance;
- (void)contactScan;
- (void)contactScanAndGetAllusers;

@end

NS_ASSUME_NONNULL_END
