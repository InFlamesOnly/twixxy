//
//  SubscribeView.h
//  Twixi
//
//  Created by macOS on 05.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IAPServices.h"



NS_ASSUME_NONNULL_BEGIN

@interface SubscribeView : UIView <SubscribeViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIView *bcgView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *popLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;

@property BOOL isSelected;

@property (weak, nonatomic) IBOutlet UILabel *tappedLabelTermsOfUse;
@property (weak, nonatomic) IBOutlet UILabel *tappedLabelPoP;

@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *weakLabel;
@property (weak, nonatomic) IBOutlet UILabel *mounthLabel;
@property (weak, nonatomic) IBOutlet UILabel *threemounthLabel;

- (void)open;
- (void)openWithTabBar:(CGFloat)tabBarHeight;
- (void)openWithIphoneX:(CGFloat)tabBarHeight;

@end

NS_ASSUME_NONNULL_END
