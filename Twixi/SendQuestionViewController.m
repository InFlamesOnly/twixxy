//
//  SendQuestionViewController.m
//  Twixi
//
//  Created by macOS on 22.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import "SendQuestionViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface SendQuestionViewController () <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailTittleString;
@property (weak, nonatomic) IBOutlet UILabel *emailString;

@end

@implementation SendQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.emailTittleString.text = NSLocalizedString(@"Do you have questions?", nil);
    self.emailString.text = NSLocalizedString(@"Contact our team for support support@twixi.com.", nil);
    self.navigationLabel.text = NSLocalizedString(@"Support", nil);
    
    NSRange index = [self.emailString.text rangeOfString:@"support@twixi.com"];
    if (index.location != NSNotFound) {
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString: self.emailString.attributedText];
        
        UIColor *color = [UIColor colorWithRed:66.0f/255.0f green:66.0f/255.0f blue:194.0f/255.0f alpha:1];
        [text addAttribute:NSForegroundColorAttributeName value:color range:index];
        [text addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:index];
        [text addAttribute:NSUnderlineColorAttributeName value:color range:index];
        
        [self.emailString setAttributedText: text];
    }
    
    [self detectTapToLabel];
}

- (void)detectTapToLabel {
    UITapGestureRecognizer *tapAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblClick:)];
    
    //Enable the lable UserIntraction
    self.emailString.userInteractionEnabled = YES;
    [self.emailString addGestureRecognizer:tapAction];
}

- (void)lblClick:(UITapGestureRecognizer *)tapGesture {
    [self sendEmail];
}

- (void)sendEmail {
    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
        mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
        
//        [mailCont setSubject:@"Email subject"];
        [mailCont setToRecipients:[NSArray arrayWithObject:@"support@twixi.com"]];
//        [mailCont setMessageBody:@"Email message" isHTML:NO];
        
        [self presentViewController:mailCont animated:YES completion:nil];
    } else {
        NSString *subject = [NSString stringWithFormat:@"Subject"];
        
        /* define email address */
        NSString *mail = [NSString stringWithFormat:@"support@twixi.com"];
        
        /* define allowed character set */
        NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
        
        /* create the URL */
        NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"mailto:?to=%@&subject=%@",
                                                    [mail stringByAddingPercentEncodingWithAllowedCharacters:set],
                                                    [subject stringByAddingPercentEncodingWithAllowedCharacters:set]]];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
