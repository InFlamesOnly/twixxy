//
//  PhotosController.m
//  Twixi
//
//  Created by macOS on 07.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "PhotosController.h"
#import <UIKit/UIKit.h>
#import "UIViewController+CurrentViewController.h"

@interface PhotosController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) NSString *myPhotoPath;
@property (strong, nonatomic) UIImagePickerController *picker;

@end

@implementation PhotosController 

- (instancetype)init {
    self = [super init];
    if (self) {
        [self pickersInit];
    }
    return self;
}

- (void)pickersInit {
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate = self;
    self.picker.allowsEditing = YES;
    self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
}

-(void)showImagePicker {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            UIViewController *viewController = [UIViewController currentViewController];
            [viewController presentModalViewController:self.picker animated:YES];
        });
    });
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.myPhotoPath = [self saveImage:chosenImage withImageName:@"image"];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"showSetting"
     object:self];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString*)saveImage:(UIImage*)image withImageName:(NSString*)imageName {
    NSData *imageData = UIImagePNGRepresentation(image);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]]; //add our image to the path
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
    NSString *string = [NSString stringWithString:fullPath];
    [self.delegate sendPhoto:string];
    return string;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


@end
