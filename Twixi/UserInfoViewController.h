//
//  UserInfoViewController.h
//  Twixi
//
//  Created by macOS on 26.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMFriend.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoViewController : UIViewController

@property (strong, nonatomic) RLMFriend *selectedFriend;

@end

NS_ASSUME_NONNULL_END
