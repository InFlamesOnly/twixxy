//
//  RootStartViewController.m
//  Twixi
//
//  Created by macOS on 02.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RootStartViewController.h"
#import "StartButton.h"

@interface RootStartViewController ()


@property (weak, nonatomic) IBOutlet StartButton *startButton;

@end


@implementation RootStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addContentFromPage];
    [self createPageController];
}

- (void)createPageController {
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, 320);
    self.pageViewController.view.center = self.view.center;
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)addContentFromPage {
    self.arrPageTitles = @[@"Twizzy !",NSLocalizedString(@"Fast", @""),
                           NSLocalizedString(@"Safe", @""),
                           NSLocalizedString(@"New words", @"")];
    self.arrPageImages =@[@"StartScreen1",@"StartScreen2",@"StartScreen3", @"StartScreen4"];
    self.arrPageContent = @[NSLocalizedString(@"Learn languages ​​quickly and easily with the help of Twizzy messenger!", @""),
                            NSLocalizedString(@"Learn languages ​​quickly and easily with the help of Twizzy messenger!", @""),
                            NSLocalizedString(@"Twizzy protects your correspondence from intruders.", @""),
                            NSLocalizedString(@"Every day you will receive new words to learn.", @"")];
}

#pragma mark - Page View Datasource Methods
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index == [self.arrPageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

#pragma mark - Other Methods
- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index {
    if (([self.arrPageTitles count] == 0) || (index >= [self.arrPageTitles count])) {
        return nil;
    }

    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    pageContentViewController.imgFile = self.arrPageImages[index];
    pageContentViewController.txtTitle = self.arrPageTitles[index];
    pageContentViewController.txtContent = self.arrPageContent[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - No of Pages Methods
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return [self.arrPageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (IBAction)btnStartAgain:(id)sender {
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:NO completion:nil];
}

@end
