//
//  RLMScheme.h
//  Twixi
//
//  Created by macOS on 08.06.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMLanguage.h"


@interface RLMScheme : RLMObject

@property NSNumber <RLMInt> *schemeId;
@property NSString *schemeName;
@property RLMLanguage *sourceLang;
@property RLMLanguage *targetLang;


- (RLMScheme*)findSchemeFromArray:(NSArray*)array schemeCode:(NSString*)schemeName;
- (id)initWithServerResponse:(NSDictionary*)response;

- (NSMutableArray*)arrayFromNeedsLanguage:(NSString*)code;
- (NSMutableArray*)arrayFromMyLanguage ;
- (void)replaseMyLang:(NSString*)myLang fromTrnslLang:(NSString*)translateLang;
- (RLMLanguage*)getFromMyCode:(NSString*)myLang fromTrnslLang:(NSString*)translateLang;

+ (void)saveFromServerResponse:(NSDictionary*)response;

+ (RLMScheme*)updateMyScheme;

@end
