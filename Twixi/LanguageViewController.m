//
//  LanguageViewController.m
//  Twixi
//
//  Created by macOS on 18.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "LanguageViewController.h"
#import "LanguageCell.h"
#import "UIView+ShadowToButton.h"
#import "RLMLanguage.h"
#import "RLMUser.h"
#import "RequestManager.h"
#import "RLMScheme.h"

@interface LanguageViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSArray *languageArray;
@property (strong, nonatomic) RLMUser *currentUser;

@end


@implementation LanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentUser = [RLMUser currentUser];
    [self checkLangType];
}

- (void)checkLangType  {
    RLMScheme *sc = [[RLMScheme alloc] init];
    if (self.isMineLanguage == YES || self.isFirstMyLanguage == YES) {
        self.languageArray = [sc arrayFromMyLanguage];
    } else {
        self.languageArray = [sc arrayFromNeedsLanguage:self.currentUser.myLangCode];
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateLang:(RLMLanguage*)lang {
    if (self.isFirstMyLanguage == YES) {
        [self.delegate getLanguage:lang];
        [self saveMyLang:lang];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (self.isFirstTrLanguage == YES) {
        [self.delegate getLanguage:lang];
        [self saveTrnsLang:lang];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (self.isMineLanguage == YES) {
        [self saveMyLang:lang];
        RLMScheme *sc = [[RLMScheme alloc] init];
        [sc replaseMyLang:self.currentUser.myLangCode fromTrnslLang:self.currentUser.translateLangCode];
        [self.delegate getLanguage:lang];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    } else {
        [self saveTrnsLang:lang];
        [self.delegate getTrnLanguage:lang];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
}

- (void)saveMyLang:(RLMLanguage*)lang {
    [RLMUser saveTrnLangCode:lang.languageCode];
}

- (void)saveTrnsLang:(RLMLanguage*)lang {
    [RLMUser saveTrnLangCode:lang.languageCode];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RLMLanguage *lang = self.languageArray[indexPath.row];
    [self updateLang:lang];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.languageArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LanguageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LanguageCell" forIndexPath:indexPath];
    RLMLanguage *lang = self.languageArray[indexPath.row];
    [cell.checkImage setHidden:YES];
    
    if (self.isMineLanguage == YES) {
        if ([lang.languageCode isEqualToString:[RLMUser currentUser].myLangCode]) {
            [cell.checkImage setHidden:NO];
        }
    }
    
    if (self.isTnsLanguage == YES) {
        if ([lang.languageCode isEqualToString:[RLMUser currentUser].translateLangCode]) {
            [cell.checkImage setHidden:NO];
        }
    }
    
    if (self.isFirstMyLanguage == YES) {
        if ([lang.languageCode isEqualToString:[RLMUser currentUser].myLangCode]) {
            [cell.checkImage setHidden:NO];
        }
    }
    
    if (self.isFirstTrLanguage == YES) {
        if ([lang.languageCode isEqualToString:[RLMUser currentUser].translateLangCode]) {
            [cell.checkImage setHidden:NO];
        }
    }
    
    cell.languageText.text = lang.languageName;
    [cell.languageImage setImage:[UIImage imageNamed:lang.languageCode]];
    return cell;
}

@end
