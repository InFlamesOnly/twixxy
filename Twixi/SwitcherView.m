//
//  SwitcherView.m
//  Twixi
//
//  Created by macOS on 15.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "SwitcherView.h"

@implementation SwitcherView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

- (void)initSwitcherWithX:(CGFloat)x width:(CGFloat)width height:(CGFloat)height {
    self.switcher = [[UIView alloc] init];
    self.switcher.frame = CGRectMake(x, 0, width, height);
    self.switcher.backgroundColor = [self colorFromHexString:@"#5070D5"];
    self.switcher.layer.cornerRadius = self.switcher.frame.size.height/2;
    [self addSubview:self.switcher];
    
}

- (void)moveToButton:(UIButton*)button withSpeed:(double)speed {
    [UIView animateWithDuration:speed animations:^{
        CGPoint center = [button center];
        self.switcher.frame = CGRectMake(center.x - 35/2, button.frame.origin.y, self.switcher.frame.size.width, self.switcher.frame.size.height);
        //CGPointMake(button.frame.origin.x, button.frame.origin.y);
    }];
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

//func createSwitcherWithFrame(x : CGFloat, width : CGFloat, height : CGFloat) {
//    switcher.frame = CGRect(x: x, y : 0, width : width, height: height)
//    switcher.backgroundColor = UIColor(red: 117/255, green: 208/255, blue: 164/255, alpha: 1)
//    switcher.layer.cornerRadius = switcher.frame.size.height/2
//    self.addSubview(switcher)
//    
//}
//
//func moveToButton(button : UIButton, speed : Double) {
//    UIView.animate(withDuration: speed) {
//        self.switcher.frame.origin = CGPoint(x: button.frame.origin.x, y: self.switcher.frame.origin.y)
//    }
//    
//}

@end
