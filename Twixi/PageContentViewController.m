//
//  PageContentViewController.m
//  Twixi
//
//  Created by macOS on 02.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "PageContentViewController.h"


@interface PageContentViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *screenImage;
@property (weak, nonatomic) IBOutlet UILabel *screenTittle;
@property (weak, nonatomic) IBOutlet UILabel *screenText;

@end


@implementation PageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configurePage];
}

- (void)configurePage {
    self.screenImage.image = [UIImage imageNamed:self.imgFile];
    self.screenTittle.text = self.txtTitle;
    self.screenText.text = self.txtContent;
}

@end
