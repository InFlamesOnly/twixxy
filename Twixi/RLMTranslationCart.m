//
//  RLMTranslationCart.m
//  Twixi
//
//  Created by macOS on 13.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMTranslationCart.h"

@implementation RLMTranslationCart

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.dictKey = [NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
        self.word = [response valueForKey:@"word"];
        self.translations = [response valueForKey:@"translation"];
        self.verb = [response valueForKey:@"verb"];
        self.naming = [response valueForKey:@"adverb"];
        self.adjective = [response valueForKey:@"adjective"];
        self.pretext = [response valueForKey:@"preposition"];
        self.audioURL = [NSString stringWithFormat:@"http://chat.servey.design/%@",[response valueForKey:@"audio"]];
        self.wordId = [response valueForKey:@"id"];
        self.lvl = [response valueForKey:@"level"];
        NSArray *exmplArray = [response valueForKey:@"examples"];
        if (exmplArray) {
            for (NSDictionary *dic in exmplArray) {
                RLMExamples *exmpl = [[RLMExamples alloc] initWithServerResponse:dic];
                [self.examples addObject:exmpl];
            }
        }
    }
    return self;
}

+ (NSString *)primaryKey {
    return @"dictKey";
}

+ (void)saveFromServerResponse:(NSDictionary*)response {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] deleteObjects:[RLMTranslationCart allObjects]];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    for (NSDictionary *dic in response) {
        NSString *myPrimaryKey = [NSString stringWithFormat:@"%@",[dic valueForKey:@"id"]];
        RLMTranslationCart *tc = [[RLMTranslationCart alloc] initWithServerResponse:dic];
        RLMTranslationCart *findTc = [RLMTranslationCart objectForPrimaryKey: myPrimaryKey];
        if (findTc == nil) {
            [[RLMRealm defaultRealm] beginWriteTransaction];
            [[RLMRealm defaultRealm] addObject:tc];
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
    }
}



@end
