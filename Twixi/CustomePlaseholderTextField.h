//
//  ChatTextField.h
//  Twixi
//
//  Created by macOS on 16.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomePlaseholderTextField : UITextField

- (void)setPlaceholderText:(NSString*)plaseholderText;


@end
