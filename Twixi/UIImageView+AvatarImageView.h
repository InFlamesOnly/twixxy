//
//  UIImageView+AvatarImageView.h
//  Twixi
//
//  Created by macOS on 04.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (AvatarImageView)
+ (void)addGradientToView:(UIView*)view
       withGradientColors:(NSArray*)colors;
+ (void)addGradientToImageView:(UIImageView*)imageView withText:(NSString*)text;

@end
