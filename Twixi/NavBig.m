//
//  NavBig.m
//  Twixi
//
//  Created by macOS on 03.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NavBig.h"
#import "IOSDeviceName.h"

@implementation NavBig

- (CGFloat)constant {
    IOSDeviceName *name = [IOSDeviceName new];
    if ([name.deviceName isEqualToString:@"iPhone X"] || [name.deviceName isEqualToString:@"Simulator"]) {
        return  200;
    } else {
        return 180;
    }
}

@end
