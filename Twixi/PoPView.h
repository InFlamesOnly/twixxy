//
//  PoPView.h
//  Twixi
//
//  Created by macOS on 15.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PoPView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (weak, nonatomic) IBOutlet UIView *bcgView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtittleLabel;

- (void)open;
- (void)openWithTabBar:(CGFloat)tabBarHeight;
- (void)openWithIphoneX:(CGFloat)tabBarHeight;

@end

NS_ASSUME_NONNULL_END
