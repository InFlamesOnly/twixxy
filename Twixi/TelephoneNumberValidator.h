//
//  TelephoneNumberValidator.h
//  Pigulka
//
//  Created by macOS on 07.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TelephoneNumberValidator : NSObject

+ (BOOL)formattedTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end
