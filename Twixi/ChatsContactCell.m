//
//  ChatsContactCell.m
//  Twixi
//
//  Created by macOS on 15.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ChatsContactCell.h"

@implementation ChatsContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.avatar.layer.cornerRadius = self.avatar.frame.size.width / 2;
//    self.avatar.clipsToBounds = YES;
    self.massageCountView.layer.cornerRadius = self.massageCountView.frame.size.width / 2;
    self.massageCountView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void)willTransitionToState:(UITableViewCellStateMask)state{
//    [super willTransitionToState:state];
//    if (state == UITableViewCellStateShowingEditControlMask) {
//        for (UIView *subview in self.subviews) {
//            if ([NSStringFromClass([subview class]) isEqualToString:@"UITableViewCellEditControl"]) {
//                UIImageView *deleteBtn = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 64, 33)];
//                [deleteBtn setImage:[UIImage imageNamed:@"delete"]];
//                [[subview.subviews objectAtIndex:3] addSubview:deleteBtn];
//            }
//        }
//    }
//}

//- (void) setEditing:(BOOL)editing animated:(BOOL)animated
//{
//    
//    [super setEditing: editing animated: YES];
//    
//    self.showsReorderControl = NO;
//    
//    self.editingAccessoryView = editing ? [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"delete"]] : nil;
//    
//}


@end
