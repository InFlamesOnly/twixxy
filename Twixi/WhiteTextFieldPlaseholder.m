//
//  WhiteTextFieldPlaseholder.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "WhiteTextFieldPlaseholder.h"

@implementation WhiteTextFieldPlaseholder

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setPlaseholderColor];
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setPlaseholderColor];
        
    }
    return self;
}

-(void)setPlaseholderColor {
    UIColor *color = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    self.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:self.placeholder
                                    attributes:@{
                                                 NSForegroundColorAttributeName: color
                                                 }
     ];
}

@end
