//
//  SignUp.m
//  Twixi
//
//  Created by macOS on 27.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "SignUp.h"
#import "RequestManager.h"
#import "ThankRegistrationViewController.h"
#import "TelephoneNumberValidator.h"
#import "NSString+PhoneNumber.h"
#import "PCAngularActivityIndicatorView.h"
#import "UIView+ShadowToButton.h"
#import "NSString+EmailValidation.h"


@import Firebase;

@interface SignUp () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

//@property (weak, nonatomic) IBOutlet UITextField *repeatPasswordTextField;
//@property (weak, nonatomic) IBOutlet UIButton *privacyAndPolicy;

@property (weak, nonatomic) IBOutlet UIButton *registrationButton;

@end

@implementation SignUp

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIView addGreyGradientToButton:self.registrationButton];
    [self.registrationButton setEnabled:NO];
    
    if (self.token) {
        self.emailTextField.text = self.email;
        self.nameTextField.text = self.name;
        self.lastNameTextField.text = self.lastName;
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    self.nameTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastNameTextField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
//
//    [self.privacyAndPolicy setImage:[UIImage imageNamed:@"deselect"] forState:UIControlStateNormal];
//
//    [self.privacyAndPolicy setImage:[UIImage imageNamed:@"select"] forState:UIControlStateSelected];
    
}

-(void)viewDidLayoutSubviews {
    [self addShadowToButton:self.registrationButton];
    [UIView addGreyGradientToButton:self.registrationButton];
    CALayer *layer = [self.registrationButton.layer.sublayers firstObject];
    [layer removeFromSuperlayer];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
//    [self addGradientToView];
}

- (void)addShadowToButton:(UIButton*)button {
    button.layer.shadowRadius  = 2.0f;
    button.layer.shadowColor   = [UIColor blackColor].CGColor;
    button.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    button.layer.shadowOpacity = 0.5f;
    button.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -0.7f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(button.bounds, shadowInsets)];
    button.layer.shadowPath    = shadowPath.CGPath;
}

- (void)addGradientToView {
    UIColor *firstColor = [UIColor colorWithRed:254.0f/255.0f green:96.0f/255.0f blue:118.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:68.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.registrationButton.bounds;
    gradient.cornerRadius = 3;
    gradient.masksToBounds = YES;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    [self.registrationButton.layer insertSublayer:gradient atIndex:0];
}

- (void)addActivityIndicator {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    [self.view addSubview:self.activityView];
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
//    [self.activity removeFromSuperview];
}

- (void)removeActivity {
    [self.activity removeFromSuperview];
    [self.activityView removeFromSuperview];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *email = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;
    NSString *lastName = self.lastNameTextField.text;
    NSString *name = self.nameTextField.text;
    NSString *login = self.loginTextField.text;
    NSString *confirmPassword = self.confirmPasswordTextField.text;

    if (textField == self.emailTextField) {
        email = [self.emailTextField.text stringByReplacingCharactersInRange:range
                                                                  withString:string];
    } else if (textField == self.passwordTextField) {
        password = [self.passwordTextField.text stringByReplacingCharactersInRange:range
                                                                        withString:string];
    } else if (textField == self.lastNameTextField) {
        lastName = [self.lastNameTextField.text stringByReplacingCharactersInRange:range
                                                                        withString:string];
    } else if (textField == self.nameTextField) {
        name = [self.nameTextField.text stringByReplacingCharactersInRange:range
                                                                        withString:string];
    } else if (textField == self.loginTextField) {
        login = [self.loginTextField.text stringByReplacingCharactersInRange:range
                                                                        withString:string];
    } else if (textField == self.confirmPasswordTextField) {
        
        confirmPassword = [self.confirmPasswordTextField.text stringByReplacingCharactersInRange:range
                                                                        withString:string];
    }
    
    BOOL emailIsValid = [email isValidEmail];
    
    if (emailIsValid == NO || password.length == 0 || confirmPassword.length == 0 || lastName.length == 0 || name.length == 0 || login.length == 0) {
        CALayer *layer = [self.registrationButton.layer.sublayers firstObject];
        if ([layer.name isEqualToString:@"gradient"] || [layer.name isEqualToString:@"grey"]) {
            NSLog(@"remove gradient");
            [layer removeFromSuperlayer];
            [UIView addGreyGradientToButton:self.registrationButton];
            [self.registrationButton setEnabled:NO];
        }
        
    } else {
        CALayer *layer = [self.registrationButton.layer.sublayers firstObject];
        if ([layer.name isEqualToString:@"grey"]) {
            NSLog(@"remove grey");
            [layer removeFromSuperlayer];
            [self.registrationButton setEnabled:YES];
            [UIView addGradientToLoginButton:self.registrationButton];
        }
    }
    return YES;
    
//    if (self.loginTextField.text.length == 0 || self.passwordTextField.text.length == 0 || self.nameTextField.text.length == 0 || self.lastNameTextField.text.length == 0 || self.emailTextField.text.length == 0) {
//
//    } else {
//        [self.registrationButton setEnabled:YES];
//        [self addGradientToView];
//    }
    
    return YES;
}



- (void)registrationUser {
//    [self addActivityIndicator];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *googleId = [defaults valueForKey:@"googleID"];
//    NSString *facebookId = [defaults valueForKey:@"facebookId"];
////    [defaults synchronize];
//        //TODO test registration
//    if ([self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
//        [[RequestManager sharedManager] reqisterUserWithLogin:self.loginTextField.text googleToken:googleId  facebookToken:facebookId email:self.emailTextField.text name:self.nameTextField.text lastName:self.lastNameTextField.text password:self.passwordTextField.text successBlock:^(NSDictionary *success) {
//            if ([[success valueForKey:@"code"] integerValue] == 0) {
//                RLMUser *user = [[RLMUser alloc] init];
//                user.token = [success valueForKey:@"token"];
//                [RLMUser saveUser:user];
//                NSString *fcmToken = [FIRMessaging messaging].FCMToken;
//                NSLog(@"FCM registration token: %@", fcmToken);
//                [[RequestManager sharedManager] registerNotificationWithFCMToken:fcmToken successBlock:^(NSDictionary *success) {
//                    self.loginTextField.text = nil;
//                    self.nameTextField.text = nil;
//                    self.lastNameTextField.text = nil;
//                    self.emailTextField.text = nil;
//                    self.passwordTextField.text = nil;
//                    //                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//                    //                [userDefaults setObject:self.token forKey:@"facebookId"];
//                    //                [userDefaults synchronize];
//                    [self removeActivity];
//                    [self performSegueWithIdentifier:@"successRegistration" sender:self];
//                } onFalureBlock:^(NSInteger statusCode) {
//                    [self removeActivity];
//                    [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//                }];
//                
//            } else {
//                [self removeActivity];
//                [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//            }
//        } onFalureBlock:^(NSInteger statusCode) {
//            
//        }];
//    } else {
//        [self removeActivity];
//
//        [self showAllertViewWithTittle:@"Error" andMessage:@"Пароли не совпадают"];
//        return;
//    }

}

-(void)dismissKeyboard {
    [self.loginTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
    [self.lastNameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
//    [self.repeatPasswordTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)registerUser:(id)sender {
    if (self.loginTextField.text.length == 0 && self.nameTextField.text.length == 0 && self.passwordTextField.text.length == 0  && self.lastNameTextField.text.length == 0 && self.emailTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Ошибка" andMessage:@"Заполните все поля!"];
    } else {
        [self registrationUser];
    }
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
