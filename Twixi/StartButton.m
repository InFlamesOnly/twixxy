//
//  StartButton.m
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "StartButton.h"

@implementation StartButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customize];
    }
    return self;
}

//-(void)layoutSubviews {
//    [self customize];
//}

- (void)customize {
    UIColor *firstColor = [UIColor colorWithRed:254.0f/255.0f green:96.0f/255.0f blue:118.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:68.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
