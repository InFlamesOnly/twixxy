//
//  NSString+EmailValidation.m
//  Twixi
//
//  Created by macOS on 03.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NSString+EmailValidation.h"

@implementation NSString (EmailValidation)

-(BOOL)isValidEmail {
    NSString *emailRegex = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

@end
