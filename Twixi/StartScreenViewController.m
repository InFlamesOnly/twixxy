//
//  StartScreenViewController.m
//  Twixi
//
//  Created by macOS on 02.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "StartScreenViewController.h"

@interface StartScreenViewController ()

@end

@implementation StartScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addGradientToView];
}

- (void)addGradientToView {
    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    [self.view.layer insertSublayer:gradient atIndex:0];
}

@end
