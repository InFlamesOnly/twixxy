//
//  RegistrationSubscribeViewController.m
//  Twixi
//
//  Created by macOS on 07.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RegistrationSubscribeViewController.h"
#import "IAPServices.h"

@interface RegistrationSubscribeViewController ()

@property (weak, nonatomic) IBOutlet UIView *bckView;
@property (weak, nonatomic) IBOutlet UIView *bckCircleView;

@property (weak, nonatomic) IBOutlet UILabel *popLabel;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;

@property BOOL isSelected;

@property (weak, nonatomic) IBOutlet UILabel *tappedLabelTermsOfUse;
@property (weak, nonatomic) IBOutlet UILabel *tappedLabelPoP;

@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdLabel;
@property (weak, nonatomic) IBOutlet UILabel *weakLabel;
@property (weak, nonatomic) IBOutlet UILabel *mounthLabel;
@property (weak, nonatomic) IBOutlet UILabel *threemounthLabel;

@end

@implementation RegistrationSubscribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setText];
    [self config];
    [self createLogickPopLabel];
    self.isSelected = NO;
}

- (void)config {
    self.bckCircleView.layer.cornerRadius = self.bckCircleView.frame.size.width / 2;
    self.bckCircleView.layer.masksToBounds = YES;
}

- (void)setText {
    [self.tittleLabel setText:NSLocalizedString(@"Buy Premium Subscription!", nil)];
    [self.subtittleLabel setText:NSLocalizedString(@"What gives a premium subscription", nil)];
    [self.firstLabel setText:NSLocalizedString(@"Unlimited real-time words translate from GoogleTranslate", nil)];
    [self.secondLabel setText:NSLocalizedString(@"Push notifications with new words to learn the language.", nil)];
    [self.thirdLabel setText:NSLocalizedString(@"Unlimited languages", nil)];
    [self.weakLabel setText:NSLocalizedString(@"1 Weak", nil)];
    [self.mounthLabel setText:NSLocalizedString(@"1 Month", nil)];
    [self.threemounthLabel setText:NSLocalizedString(@"3 Month", nil)];
}

- (void)createLogickPopLabel {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popTapped)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.tappedLabelPoP addGestureRecognizer:tapGestureRecognizer];
    self.tappedLabelPoP.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsTapped)];
    tapGestureRecognizer2.numberOfTapsRequired = 1;
    [self.tappedLabelTermsOfUse addGestureRecognizer:tapGestureRecognizer2];
    self.tappedLabelTermsOfUse.userInteractionEnabled = YES;
}

- (void)popTapped {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twixymessenger.ucoz.net/twixy_Privacy_Policy.pdf"]];
}

- (void)termsTapped {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twixymessenger.ucoz.net/Terms_of_Use-2.html"]];
}

- (IBAction)changeSelectionState:(id)sender {
    if (self.isSelected == NO) {
        self.isSelected = YES;
        [self.selectedButton setImage:[UIImage imageNamed:@"TermChecked"] forState:UIControlStateNormal];
    } else if (self.isSelected == YES) {
        [self.selectedButton setImage:[UIImage imageNamed:@"TermUnchecked"] forState:UIControlStateNormal];
        self.isSelected = NO;
    }
}

- (IBAction)skip:(id)sender {
    [self performSegueWithIdentifier:@"next" sender:self];
}

- (IBAction)firstPrice:(id)sender {
    if (self.isSelected == YES) {
        [[IAPServices sharedInstance] getProducts:@"DM.twiximassanger.com.Twixi.weaksubscribe"];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Need to accept the rules", nil)];
        
    }
}
- (IBAction)secondPrice:(id)sender {
    if (self.isSelected == YES) {
        [[IAPServices sharedInstance] getProducts:@"DM.twiximassanger.com.Twixi.onemonth"];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Need to accept the rules", nil)];
    }
}
- (IBAction)threePrice:(id)sender {
    if (self.isSelected == YES) {
        [[IAPServices sharedInstance] getProducts:@"DM.twiximassanger.com.Twixi.threemonth"];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Need to accept the rules", nil)];
    }
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    UIViewController *currentTopVC = [self topViewController];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [currentTopVC presentViewController:alertController animated:YES completion:NULL];
}

@end
