//
//  DownArrow.m
//  Twixi
//
//  Created by macOS on 18.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import "DownArrow.h"

@implementation DownArrow

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (void)customeinit {
    [[NSBundle mainBundle] loadNibNamed:@"DownArrow" owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    self.downButton.layer.cornerRadius = self.downButton.frame.size.width / 2;
    self.downButton.clipsToBounds = YES;
    [self addBorder];
    [self addShadow];
}

- (void)addShadow {
    self.downButton.layer.shadowRadius  = 16.0f;
    self.downButton.layer.shadowColor   = [UIColor colorWithRed:145.0f/255.0f green:169.0f/255.0f blue:177.0f/255.0f alpha:0.25].CGColor;
    self.downButton.layer.shadowOffset  = CGSizeMake(0.0f, 8.0f);
    self.downButton.layer.shadowOpacity = 1;
    self.downButton.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -0.7f, 0);
    
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.downButton.bounds, shadowInsets)];
    self.downButton.layer.shadowPath    = shadowPath.CGPath;
}

- (void)addBorder {
    self.downButton.layer.borderWidth = 1;
    self.downButton.layer.borderColor = [UIColor colorWithRed:145.0f/255.0f green:169.0f/255.0f blue:177.0f/255.0f alpha:0.25].CGColor;
}

- (IBAction)buttonTapped:(id)sender {
    [self.delegate tapToDownButton];
}

@end
