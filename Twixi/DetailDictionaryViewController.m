//
//  DetailDictionaryViewController.m
//  Twixi
//
//  Created by macOS on 06.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "DetailDictionaryViewController.h"
#import "DetailCardView.h"
#import "PagedFlowView.h"
#import "RequestManager.h"
#import "RLMTranslationCart.h"
#import "Reachability.h"

@interface DetailDictionaryViewController () <PagedFlowViewDelegate, PagedFlowViewDataSource>

@property (weak, nonatomic) IBOutlet PagedFlowView *pagedView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property(nonatomic, strong) NSArray *contentArray;

@property NSString *lastLvl;

@end

@implementation DetailDictionaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.errorLabel.text = NSLocalizedString(@"There are no words for this study design (only EN-RU is working)", nil);
    [self.errorLabel setHidden:YES];
    [self configurePaged];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self checkInternetConnection];
}

- (void)checkInternetConnection {
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        self.contentArray = (NSArray*)[RLMTranslationCart allObjects];
        self.contentArray = [DetailCardView viewsWithContent:self.contentArray fromView:self.view];
        [self.pagedView reloadData];
    } else {
        [self getLevelsFromsServer];
//        [self getDictFromServer];
    }
}

- (void)configurePaged {
    self.pagedView.delegate = self;
    self.pagedView.dataSource = self;
    self.pagedView.minimumPageAlpha = 1;
    self.pagedView.minimumPageScale = 1;
    self.pagedView.minimumPageScale = 0.97;
    self.pagedView.orientation = PagedFlowViewOrientationVertical;
}

- (void)getDictFromServer {
    [[RequestManager sharedManager] getDictListWithLevel:@"2" successBlock:^(NSDictionary *success) {
        [RLMTranslationCart saveFromServerResponse:[success valueForKey:@"data"]];
        self.contentArray = (NSArray*)[RLMTranslationCart allObjects];
        self.contentArray = [DetailCardView viewsWithContent:self.contentArray fromView:self.view];
        [self.pagedView reloadData];
        [self removeActivity];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];

    }];
//    [[RequestManager sharedManager] getDictListSuccessBlock:^(NSDictionary *success) {
//        [RLMTranslationCart saveFromServerResponse:[success valueForKey:@"data"]];
//        self.contentArray = (NSArray*)[RLMTranslationCart allObjects];
//        self.contentArray = [DetailCardView viewsWithContent:self.contentArray fromView:self.view];
//        [self.pagedView reloadData];
//        [self removeActivity];
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self removeActivity];
//    }];
}

- (void)getLevelsFromsServer {
    [self startActivityFromTabBar];
    [[RequestManager sharedManager] getLevels:^(NSDictionary *success) {
        NSArray *lvlArr = [success valueForKey:@"data"];
        int lastLvlInt = (int)[[lvlArr lastObject] valueForKey:@"level"];
        self.lastLvl = [NSString stringWithFormat:@"%d",lastLvlInt];
        [self getDictFromServer];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)checkCountArraysFromShowErrorLabel:(NSArray*)array {
    if (array.count == 0) {
        [self.errorLabel setHidden:NO];
    } else {
        [self.errorLabel setHidden:YES];
    }
}

#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView;{
    return CGSizeMake(self.pagedView.frame.size.width - 32, self.pagedView.frame.size.height - 42);
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    DetailCardView *cardView =  self.contentArray[index];
    NSLog(@"%@",cardView.word.text);
    NSLog(@"Scrolled to page # %ld", (long)index);
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    NSLog(@"Tapped on page # %ld", (long)index);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Datasource
//返回显示View的个数
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [self.contentArray count];
}

//返回给某列使用的View
- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    return [self configureCardView:flowView fromIndex:index];
}

- (DetailCardView*)configureCardView:(PagedFlowView *)flowView fromIndex:(NSInteger)index {
    DetailCardView *cardView = (DetailCardView *)[flowView dequeueReusableCell];
    cardView = self.contentArray[index];
    cardView.layer.cornerRadius = 15;
    cardView.layer.masksToBounds = YES;
    return cardView;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pageControlValueDidChange:(id)sender {
    UIPageControl *pageControl = sender;
    [self.pagedView scrollToPage:pageControl.currentPage];
}

@end
