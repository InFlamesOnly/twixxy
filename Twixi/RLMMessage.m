//
//  RLMMessage.m
//  Twixi
//
//  Created by macOS on 28.09.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RLMMessage.h"

@implementation RLMMessage

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (id)initWithRole:(NSNumber*)role message:(NSString*)message endTime:(NSString*)time {
    self = [super init];
    if (self) {
        self.role = role;
        self.message = message;
        self.time = time;
    }
    return self;
}

@end
