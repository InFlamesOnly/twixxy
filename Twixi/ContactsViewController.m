//
//  ContactsViewController.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ContactsViewController.h"
#import "ContactCell.h"
#import "User.h"
#import "ChatViewController.h"
#import "RequestManager.h"
#import "RLMFriend.h"
#import <UIImageView+AFNetworking.h>
#import "ChatViewController.h"
#import "UIImageView+AvatarImageView.h"
#import "RLMLanguage.h"
#import "NSDictionary+DictCategory.h"
#import "NSString+TelegramNSDateString.h"
#import "Reachability.h"
#import <MessageUI/MessageUI.h>
#import "AvatarView.h"

#import "MyContacts.h"
#import "UIAlertController+Blocks.h"


@interface ContactsViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate, MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *blackView;
@property (weak, nonatomic) IBOutlet UILabel *contactsLabel;
@property (weak, nonatomic) IBOutlet UILabel *telephoneLabel;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;
@property (weak, nonatomic) IBOutlet UITableView *searchedTableView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) UIView *saveHeaderView;

@property (strong, nonatomic) RLMUser *user;

@property (strong, nonatomic) NSArray *usersArray;

@property BOOL isFiltered;
@property BOOL isSearched;
@property BOOL isCanEdit;

@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (strong, nonatomic) NSMutableArray* searchedTableData;

@property (strong, nonatomic) NSMutableArray* myContactArray;
@property (strong, nonatomic) NSMutableArray* myDictionaryArrays;

@property (strong, nonatomic) NSMutableArray *indexArray;
@property (strong, nonatomic) NSArray *indexFilteredArray;


@property (nonatomic) NSInteger chatId;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIView *searchViewBcg;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationViewConstraint;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet AvatarView *avatarView;

@property (strong, nonatomic) NSLayoutConstraint *widthSearchViewConstraint;
@property (weak, nonatomic) IBOutlet UIView *bckSearchView;
@property int startWidth;
@property BOOL searchIsActive;


@end

//TODO preloader add friend
@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createObjects];
    [self configureStartView];
    [self configureHeaderView];
    [self createTapToDissmissKeyboard];
    [self.avatarView setAvatar:self.user.avatar orInitialsView:self.user.name];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self checkInternetConnection];
}

- (void)createObjects {
    self.myContactArray = [NSMutableArray new];
    self.user = [RLMUser currentUser];
    self.indexFilteredArray = @[NSLocalizedString(@"My users", nil),NSLocalizedString(@"Other users", nil)];
}

- (void)configureStartView {
    [self calculateStartHeightAndAddConstr];
    
    self.isCanEdit = YES;
    self.isSearched = NO;
    self.searchIsActive = NO;
    [self.blackView setHidden:YES];
    self.searchViewBcg.layer.cornerRadius = 10;
    [self.clearButton setHidden:YES];
    [self.errorLabel setHidden:YES];
    [self.searchedTableView setHidden:YES];
    [self.contactsTableView setHidden:NO];
}

- (void)configureHeaderView {
    self.name.text = self.user.name;
    self.telephoneLabel.text = [NSString stringWithFormat:@"%@ : +%@", NSLocalizedString(@"My phone", nil) , self.user.telephone];
    self.saveHeaderView = self.headerView;
}

- (void)checkInternetConnection {
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        [self convertRealmObjectsArray];
        [self.contactsTableView reloadData];
        [self contactScan];
        [self sync];
        [self getAllCharactersArray];
    } else {
        [self contactScan];
        [self getUserFromServer];
    }
}

- (void)createTapToDissmissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.searchTextField resignFirstResponder];
    [self.blackView setHidden:YES];
}

- (void)getAllCharactersArray {
    NSLog(@"localeIdentifier: %@", [[NSLocale currentLocale] localeIdentifier]);
    NSCharacterSet * charSet = [[[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale currentLocale] localeIdentifier]] objectForKey:NSLocaleExemplarCharacterSet];
    NSCharacterSet * enCharSet = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] objectForKey:NSLocaleExemplarCharacterSet];
    
    NSArray * myLocaleChars = [self charactersInCharacterSet:charSet];
    NSArray * enLocaleChars = [self charactersInCharacterSet:enCharSet];
    
    NSMutableArray *allChars = [NSMutableArray new];
    for (NSString *str in enLocaleChars) {
        [allChars addObject:str];
        NSLog(@"%@", str);
    }
    for (NSString *str in myLocaleChars) {
        [allChars addObject:str];
        NSLog(@"%@", str);
    }
    
    self.indexArray = [[NSMutableArray alloc] initWithArray:allChars];
    
    NSMutableArray *charsArray = [NSMutableArray new];
    NSMutableArray *allUsers = [[NSMutableArray alloc] initWithArray:self.usersArray];
    NSMutableArray *myContacts = [[NSMutableArray alloc] initWithArray:self.myContactArray];
    NSMutableArray *dictArray = [NSMutableArray new];
    
    charsArray = [[NSMutableArray alloc] initWithArray:allChars];
    
    
        for (NSString *key in self.indexArray) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@",key];
            NSArray *filteredArray = [myContacts filteredArrayUsingPredicate:predicate];
            if (filteredArray.count > 0) {
                NSMutableDictionary *dic = [NSMutableDictionary new];
                [dic setObject:filteredArray forKey:key];
                [dictArray addObject:dic];
                for (RLMUser *user in filteredArray) {
                    [myContacts removeObject:user];
                }
            } else {
                 [charsArray removeObject:key];
            }
        }
    
    self.indexArray = charsArray;
    
    if (myContacts.count != 0) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setObject:myContacts forKey:@"#"];
        [self.indexArray insertObject:@"#" atIndex:0];
        [dictArray insertObject:dic atIndex:0];
    }
    
    if (allUsers.count != 0) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setObject:allUsers forKey:@" "];
        [self.indexArray insertObject:@" " atIndex:0];
        [dictArray insertObject:dic atIndex:0];
    }
    
    
    //
    NSLog(@"%@",dictArray);
    self.myDictionaryArrays = dictArray;
    [self.contactsTableView reloadData];
}

- (void)getAllCharactersArray1 {
    NSLog(@"localeIdentifier: %@", [[NSLocale currentLocale] localeIdentifier]);
    NSCharacterSet * charSet = [[[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale currentLocale] localeIdentifier]] objectForKey:NSLocaleExemplarCharacterSet];
    NSCharacterSet * enCharSet = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] objectForKey:NSLocaleExemplarCharacterSet];
    
    NSArray * myLocaleChars = [self charactersInCharacterSet:charSet];
    NSArray * enLocaleChars = [self charactersInCharacterSet:enCharSet];
    
    NSMutableArray *allChars = [NSMutableArray new];
    for (NSString *str in enLocaleChars) {
        [allChars addObject:str];
        NSLog(@"%@", str);
    }
    for (NSString *str in myLocaleChars) {
        [allChars addObject:str];
        NSLog(@"%@", str);
    }

    self.indexArray = [[NSMutableArray alloc] initWithArray:allChars];
    
    NSMutableArray *charsArray = [NSMutableArray new];
    NSMutableArray *myContacts = [[NSMutableArray alloc] initWithArray:self.myContactArray];
    NSMutableArray *allUsers = [[NSMutableArray alloc] initWithArray:self.usersArray];
    NSMutableArray *dictArray = [NSMutableArray new];
    
    charsArray = [[NSMutableArray alloc] initWithArray:allChars];
    
    for (NSString *key in self.indexArray) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@",key];
        NSArray *filteredArray = [myContacts filteredArrayUsingPredicate:predicate];
        if (filteredArray.count > 0) {
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic setObject:filteredArray forKey:key];
            [dictArray addObject:dic];
            for (RLMUser *user in filteredArray) {
                [myContacts removeObject:user];
            }
        } else {
            [charsArray removeObject:key];
        }
    }

    
    
//    for (NSString *key in self.indexArray) {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@",key];
//        NSArray *filteredArray = [allUsers filteredArrayUsingPredicate:predicate];
//        if (filteredArray.count > 0) {
//            NSMutableDictionary *dic = [NSMutableDictionary new];
//            [dic setObject:filteredArray forKey:key];
//            [dictArray addObject:dic];
//            for (RLMUser *user in filteredArray) {
//                [allUsers removeObject:user];
//            }
//        } else {
//             [charsArray removeObject:key];
//        }
//    }
    
    self.indexArray = charsArray;
    
    if (myContacts.count != 0) {
        NSMutableDictionary *dic = [NSMutableDictionary new];
        [dic setObject:myContacts forKey:@"#"];
        [self.indexArray insertObject:@"#" atIndex:0];
        [dictArray insertObject:dic atIndex:0];
    }

    NSLog(@"%@",dictArray);
    
    self.myDictionaryArrays = dictArray;
    
    [self.contactsTableView reloadData];
}

- (void)myFriendAndSearchedFriendsfromSearchedString:(NSString*) searchStr {
    NSArray *findUsersArray = [self findUserFromSearchString:searchStr];
    NSMutableArray *allValuesArray = [NSMutableArray new];
    if (findUsersArray.count > 0) {
        NSMutableDictionary *myFriend = [NSMutableDictionary new];
        [myFriend setObject:findUsersArray forKey:@"myUsers"];
        [allValuesArray addObject:myFriend];
    }
    [[RequestManager sharedManager] getUserSearch:searchStr withSuccessBlock:^(NSDictionary *success) {
        self.searchedTableData = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [success valueForKey:@"data"]) {
            RLMFriend *friend = [[RLMFriend alloc] initWithServerResponse:dic];
            [self.searchedTableData addObject:friend];
        }
        if (self.searchedTableData.count > 0) {
            NSMutableDictionary *otherFriend = [NSMutableDictionary new];
            [otherFriend setObject:self.searchedTableData forKey:@"otherUsers"];
            [allValuesArray addObject:otherFriend];
        }
        self.searchedTableData = allValuesArray;
        self.isCanEdit = NO;
        [self.searchedTableView reloadData];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (NSArray *)charactersInCharacterSet:(NSCharacterSet *)charSet {
    NSMutableSet *set = [NSMutableSet new];
    NSData * data = [charSet bitmapRepresentation];
    const char* bytes = [data bytes];
    
    for (int i = 0; i < 8192; ++i)
    {
        if (bytes[i >> 3] & (((unsigned int)1) << (i & 7)))
        {
            NSString *str = [NSString stringWithFormat:@"%C", (unichar)i];
            NSString *upStr = [str uppercaseString];
            [set addObject:upStr];
        }
    }
    NSArray *alphArray = [set allObjects];
    NSArray *sortedArray =[alphArray sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *unSortedArray = [[NSArray alloc]initWithArray:sortedArray];
    return unSortedArray;
}


- (void)calculateStartHeightAndAddConstr {
    UIFont *font = [UIFont systemFontOfSize:14];
    int value = [self widthOfString:self.searchTextField.placeholder withFont:font] + 8 + 12 + 8 + 8 + 16 + 8 + 1;
    self.startWidth = value;
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.bckSearchView
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:value];
    self.widthSearchViewConstraint = widthConstraint;
    [self.bckSearchView addConstraints:@[widthConstraint]];
    [self.view layoutIfNeeded];
}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (void)openSearchView {
    self.widthSearchViewConstraint.constant = self.view.frame.size.width - 32;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)closeSearchView {
    self.widthSearchViewConstraint.constant = self.startWidth;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)convertRealmObjectsArray {
    NSMutableArray *array = [NSMutableArray new];
    for (RLMFriend *friend in [RLMFriend allObjects]) {
        [array addObject:friend];
    }
    self.usersArray = array;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.contactsTableView]) {
        
        [self.searchTextField resignFirstResponder];
        [self.blackView setHidden:YES];
        return NO;
    }
    
    if ([touch.view isDescendantOfView:self.searchedTableView]) {
        
        [self.searchTextField resignFirstResponder];
        [self.blackView setHidden:YES];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self openSearchView];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length == 0) {
        [self closeSearchView];
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.blackView setHidden:NO];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchStr stringByTrimmingCharactersInSet: set] length] != 0) {
        [self.searchedTableView setHidden:NO];
        [self.contactsTableView setHidden:YES];
        self.isSearched = YES;
        self.isFiltered = NO;
        [self myFriendAndSearchedFriendsfromSearchedString:searchStr];
        [self openSearchView];
        self.contactsTableView.tableHeaderView = nil;
        [self.clearButton setHidden:NO];
        [self.blackView setHidden:YES];
    } else {
        [self closeSearchView];
        self.contactsTableView.tableHeaderView = self.saveHeaderView;
        [self.clearButton setHidden:YES];
    }
    
    searchStr = [searchStr lowercaseString];
    
    if ([searchStr isEqualToString:@""] && self.isSearched == YES) {
        self.isSearched = NO;
        [self closeSearchView];
        self.contactsTableView.tableHeaderView = self.saveHeaderView;
        [self.clearButton setHidden:YES];
        [self.searchedTableView setHidden:YES];
        [self.contactsTableView setHidden:NO];
        return YES;
    }
    return YES;
}

- (NSArray*)findUserFromSearchString:(NSString*)string {
    NSArray *usersArray = [NSArray arrayWithArray:self.usersArray];
    NSArray *contactsArray = [NSArray arrayWithArray:self.myContactArray];
    NSArray *all = [usersArray arrayByAddingObjectsFromArray:contactsArray];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name beginswith[c] %@",string];
    NSArray *filteredArray = [all filteredArrayUsingPredicate:predicate];
    return (NSArray*)filteredArray;
}

- (void)reloadData {
    if (self.usersArray.count == 0) {
        [self.contactsTableView setHidden:YES];
        [self.errorLabel setHidden:NO];
    } else {
        [self.contactsTableView setHidden:NO];
        [self.errorLabel setHidden:YES];
    }
    [self.contactsTableView reloadData];
    
}

- (void)getUserFromServer {
    NSMutableArray *mArray = [NSMutableArray new];
    [[RequestManager sharedManager] getContactListSuccessBlock:^(NSDictionary *success) {
        for (NSDictionary *dic in [success valueForKey:@"data"]) {
            RLMRealm *realm = [RLMRealm defaultRealm];
            RLMFriend *friend = [[RLMFriend alloc] initWithServerResponse:dic];
            RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:friend.friendId];
            if (findFriend == nil) {
                [realm beginWriteTransaction];
                [realm addObject:friend];
                [realm commitWriteTransaction];
                [mArray addObject:friend];
            } else {
                [mArray addObject:friend];

            }
        }
        self.usersArray = mArray;
        [self.contactsTableView reloadData];
        [self sync];
        [self getAllCharactersArray];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ChatViewController *vc = segue.destinationViewController;
    if ([segue.identifier isEqualToString:@"cc"]) {
//        vc.chat = self.selectedChat;
        vc.selectedFriend = self.selectedFriend;
    }
}

- (IBAction)cancel:(id)sender {
        self.searchIsActive = NO;
        [self searchViewNotActive];
        self.isFiltered = NO;
        self.isSearched = NO;
        [self.contactsTableView setHidden:NO];
        self.searchTextField.text = nil;
        self.isCanEdit = YES;
        [self reloadData];
}

- (IBAction)clearSearch:(id)sender {
    [self cancel:self];
    [self.clearButton setHidden:YES];
    self.searchTextField.text = @"";
    [self closeSearchView];
    self.contactsTableView.tableHeaderView = self.saveHeaderView;
    [self.contactsTableView setHidden:NO];
    [self.searchedTableView setHidden:YES];
    if (self.isSearched == YES) {
        [self.clearButton setHidden:YES];
        [self.contactsTableView setHidden:YES];
    }
}

- (void)searchViewIsActive {
    [self openSearchView];
    self.textViewConstraint.constant = 70;
    self.navigationViewConstraint.constant = 73;
    [self.cancelButton setHidden:NO];
    [self.addButton setHidden:YES];
    [self.contactsLabel setHidden:YES];
    [self.blackView setHidden:NO];
    
    [self.searchTextField becomeFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
       
    }];
}

- (void)searchViewNotActive {
    [self closeSearchView];
    self.textViewConstraint.constant = 14;
    self.navigationViewConstraint.constant = 160;
    [self.cancelButton setHidden:YES];
    [self.contactsLabel setHidden:NO];
    [self.blackView setHidden:YES];
    [self.searchTextField resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)showOpenSmsControllerFromFriend:(RLMFriend*)friend {
    self.contactsTableView.userInteractionEnabled = NO;
    [self openSMSFromUserPhone:friend.phone];
    self.contactsTableView.userInteractionEnabled = YES;
}

- (NSArray*)myUsers {
    NSDictionary *dic = self.searchedTableData[0];
    NSArray *arrayUsers = [dic valueForKey:@"myUsers"];
    return arrayUsers;
}

- (NSArray*)otherUsers {
    NSDictionary *dic = self.searchedTableData[1];
    NSArray *arrayUsers = [dic valueForKey:@"otherUsers"];
    return arrayUsers;
}

- (NSArray*)allUsersFromIndexPath:(NSIndexPath*)indexPath {
    NSString *sectionTitle = [self.indexArray objectAtIndex:indexPath.section];
    NSDictionary *dic = self.myDictionaryArrays[indexPath.section];
    NSArray *arrayUsers = [dic valueForKey:sectionTitle];
    return arrayUsers;
}

- (void)addFrindToFriendList:(RLMFriend*)friend {
    [[RequestManager sharedManager] addContactFromId:friend.friendId withSuccessBlock:^(NSDictionary *success) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm addObject:friend];
        [realm commitWriteTransaction];
        [self showAllertViewWithTittleWithAction:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"Friends add from your list", nil)];
        [self.clearButton setHidden:YES];
        self.searchedTableData = nil;
        self.contactsTableView.userInteractionEnabled = YES;
        [self cancel:self];
        [self.contactsTableView setHidden:NO];
        [self.searchedTableView setHidden:YES];
        self.contactsTableView.tableHeaderView = self.saveHeaderView;
    } onFalureBlock:^(NSInteger statusCode) {
        self.contactsTableView.userInteractionEnabled = YES;
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RLMFriend *friend = [[RLMFriend alloc] init];
    
    if (self.isSearched == YES) {
        NSArray *dicArrays = self.searchedTableData;
        if (dicArrays.count == 0) {
            
        } else if (dicArrays.count == 1) {
            NSArray *arrayUsers = [NSArray new];
            NSDictionary *dict = dicArrays[0];
            if ([dict valueForKey:@"myUsers"]) {
                arrayUsers = [dict valueForKey:@"myUsers"];
                friend = arrayUsers[indexPath.row];
                if ([friend.friendFromMyList isEqualToNumber:@(1)]) {
                     [self showOpenSmsControllerFromFriend:friend];
                } else {
                    self.selectedFriend = friend;
                    [self performSegueWithIdentifier:@"cc" sender:self];
                    self.contactsTableView.userInteractionEnabled = YES;
                }
                return;
            } else {
                arrayUsers = [dict valueForKey:@"otherUsers"];
                friend = arrayUsers[indexPath.row];
                [self addFrindToFriendList:friend];
                return;
            }
        } else if (dicArrays.count == 2) {
            if (indexPath.section == 0) {
                NSArray *myUser = [self myUsers];
                friend = myUser[indexPath.row];
                if ([friend.friendFromMyList isEqualToNumber:@(1)]) {
                    [self showOpenSmsControllerFromFriend:friend];
                    return;
                }
                self.selectedFriend = friend;
                [self performSegueWithIdentifier:@"cc" sender:self];
                self.contactsTableView.userInteractionEnabled = YES;

            } else if (indexPath.section == 1) {
                NSArray *otherUsers = [self otherUsers];
                friend = otherUsers[indexPath.row];
                [self addFrindToFriendList:friend];
                return;
            }
        }
    } else {
        friend = [self allUsersFromIndexPath:indexPath][indexPath.row];
        if ([friend.friendFromMyList isEqualToNumber:@(1)]) {
            [self openSMSFromUserPhone:friend.phone];
            return;
        }
        self.selectedFriend = friend;
        [self performSegueWithIdentifier:@"cc" sender:self];
        self.contactsTableView.userInteractionEnabled = YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
        [self.searchTextField resignFirstResponder];
    return YES;
}

- (void)contactScan {
    [[MyContacts sharedInstance] contactScanAndGetAllusers];
    self.myContactArray = [MyContacts sharedInstance].myUsersArray;
    NSLog(@"%@",self.myContactArray);
}

- (void)sync1 {
//    NSMutableArray *array = (NSMutableArray*)self.usersArray;
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(compare:)];
    
    self.myContactArray = (NSMutableArray*)[self.myContactArray sortedArrayUsingDescriptors:@[sort]];
    
//    for (NSDictionary *dict in self.myContactArray) {
//        RLMFriend *friend = [[RLMFriend alloc] init];
//        friend.name = [dict valueForKey:@"name"];
//        friend.friendFromMyList = @(1);
//        friend.phone = [dict valueForKey:@"phone"];
//        BOOL find = [self findDuplicate:friend.phone inArray:self.usersArray];
//        if (!find) {
//           [array addObject:friend];
//        }
//    }
//    self.usersArray = array;
    [self.contactsTableView reloadData];
}

- (void)sync {
    NSMutableArray *array = [NSMutableArray new];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(compare:)];
    
    self.myContactArray = (NSMutableArray*)[self.myContactArray sortedArrayUsingDescriptors:@[sort]];
    
        for (NSDictionary *dict in self.myContactArray) {
            RLMFriend *friend = [[RLMFriend alloc] init];
            friend.name = [dict valueForKey:@"name"];
            friend.friendFromMyList = @(1);
            friend.phone = [dict valueForKey:@"phone"];
            BOOL find = [self findDuplicate:friend.phone inArray:self.usersArray];
            if (!find) {
               [array addObject:friend];
            }
        }
    self.myContactArray = array;
    [self.contactsTableView reloadData];
}

- (BOOL)findDuplicate:(NSString*)phone inArray:(NSArray*)array {
    BOOL find = NO;
    int index = 0;
    for (RLMFriend *friend in array) {
        index ++;
        if ([friend.phone isEqualToString:phone]) {
            find = YES;
            return find;
        }
    }
    return find;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.isCanEdit;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self checkInternetConnectionFromRemoveFriendIndexPath:indexPath];
    }
}

- (void)checkInternetConnectionFromRemoveFriendIndexPath:(NSIndexPath*)indexPath {
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Internet connection error", nil)];
        return;
    } else {
        [self removeFriendFromIndexPath:indexPath];
    }
}

- (void)removeFriendFromIndexPath:(NSIndexPath*)indexPath {
    RLMFriend *friend = self.usersArray[indexPath.row];
    RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:friend.friendId];
    if (findFriend != nil) {
        [[RequestManager sharedManager] deleteContactFromId:findFriend.friendId withSuccessBlock:^(NSDictionary *success) {
            NSMutableArray *array = (NSMutableArray*)self.usersArray;
            [array removeObjectAtIndex:indexPath.row];
            self.usersArray = (NSArray*)array;
            [self sync];
            [self getAllCharactersArray];
            RLMRealm *realm = [RLMRealm defaultRealm];
            RLMChat *chat = [[RLMChat alloc] init];
            NSMutableArray *chatArray = [chat findFromFriendId:findFriend.friendId];
            if (chatArray.count != 0) {
                RLMChat *findChat = chatArray[0];
                [realm beginWriteTransaction];
                [realm deleteObject:findChat];
                [realm commitWriteTransaction];
            }
            [realm beginWriteTransaction];
            [realm deleteObject:findFriend];
            [realm commitWriteTransaction];
            [self.contactsTableView reloadData];
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"This user is not in Twixxy", nil)];
    }
}

- (void)hideHeader {
    self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, 0);
    [self.view layoutSubviews];
}

- (void)showHeader {
    self.headerView.frame = CGRectMake(self.headerView.frame.origin.x, self.headerView.frame.origin.y, self.headerView.frame.size.width, 90);
    [self.avatarView setAvatar:@"http://chat.servey.design/" orInitialsView:self.user.name];
    [self.view layoutSubviews];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    //241 247 255
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.contentView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:247.0f/255.0f blue:255.0f/255.0f alpha:1]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.searchedTableView) {
        if (self.isSearched == YES) {
            NSArray *dicArrays = self.searchedTableData;
            if (dicArrays.count == 0) {
               return  0;
            }
            if (dicArrays.count == 1) {
                return  1;
            }
            if (dicArrays.count == 2) {
                return  2;
            }
        }
    }
    return self.indexArray.count;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (tableView == self.searchedTableView) {
        if (self.isSearched == YES) {
            return nil;
        }
    }
    return self.indexArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString*)title atIndex:(NSInteger)index
{
    return index;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == self.searchedTableView) {
        if (self.isSearched == YES) {
            NSArray *dicArrays = self.searchedTableData;
            if (dicArrays.count == 1) {
                NSDictionary *dict = dicArrays[0];
                if ([dict valueForKey:@"myUsers"]) {
                    return NSLocalizedString(@"My users", nil);
                } else {
                    return NSLocalizedString(@"Other users", nil);
                }
            } if (dicArrays.count == 2) {
                NSArray *arrayOne = self.searchedTableData[0];
                NSArray *arrayTwo = self.searchedTableData[1];
                
                if (arrayOne.count > 0 || arrayTwo.count > 0) {
                    return [self.indexFilteredArray objectAtIndex:section];
                } else {
                    if (arrayOne.count > 0) {
                        return NSLocalizedString(@"My users", nil);
                    }
                    if (arrayTwo.count > 0) {
                        return NSLocalizedString(@"Other users", nil);
                    }
                }
                return [self.indexFilteredArray objectAtIndex:section];
            }
        }
        
        }
    return [self.indexArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchedTableView) {
        if (self.isSearched == YES) {
            NSArray *dicArrays = self.searchedTableData;
            if (dicArrays.count == 0) {
                return 0;
            } else if (dicArrays.count == 1) {
                NSArray *arrayUsers = [NSArray new];
                NSDictionary *dict = dicArrays[0];
                if ([dict valueForKey:@"myUsers"]) {
                    arrayUsers = [dict valueForKey:@"myUsers"];
                    return arrayUsers.count;
                } else {
                    arrayUsers = [dict valueForKey:@"otherUsers"];
                    return arrayUsers.count;
                }
                return 1;
            } else if (dicArrays.count == 2) {
                if (section == 0) {
                    NSDictionary *dic = self.searchedTableData[0];
                    NSArray *arrayUsers = [dic valueForKey:@"myUsers"];
                    return arrayUsers.count;
                } else if (section == 1) {
                    NSDictionary *dic = self.searchedTableData[1];
                    NSArray *arrayUsers = [dic valueForKey:@"otherUsers"];
                    return arrayUsers.count;
                }
                return  2;
            }
        }
    }
    NSString *sectionTitle = [self.indexArray objectAtIndex:section];
    NSDictionary *dic = self.myDictionaryArrays[section];
    NSArray *arrayUsers = [dic valueForKey:sectionTitle];
    return [arrayUsers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactCell *cell = (ContactCell*)[self.contactsTableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    RLMFriend *friend = [[RLMFriend alloc] init];
    
    if (tableView == self.searchedTableView) {
        return [self configureCellFromSearchdTableViewIndexPath:indexPath];
    }
    
    if (self.isSearched == NO) {
        NSString *sectionTitle = [self.indexArray objectAtIndex:indexPath.section];
        NSDictionary *dic = self.myDictionaryArrays[indexPath.section];
        NSArray *arrayUsers = [dic valueForKey:sectionTitle];
        friend = arrayUsers[indexPath.row];
    }
    
    [self configureCell:cell fromFriend:friend fromIndexPath:indexPath];
    return cell;
}

- (ContactCell*)configureCellFromSearchdTableViewIndexPath:(NSIndexPath*)indexPath {
    ContactCell *cell = (ContactCell*)[self.contactsTableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    RLMFriend *friend = [[RLMFriend alloc] init];
    if (self.isSearched == YES) {
        NSArray *dicArrays = self.searchedTableData;
        if (dicArrays.count == 0) {
            
        } else if (dicArrays.count == 1) {
            NSArray *arrayUsers = [NSArray new];
            NSDictionary *dict = dicArrays[0];
            if ([dict valueForKey:@"myUsers"]) {
                arrayUsers = [dict valueForKey:@"myUsers"];
                friend = arrayUsers[indexPath.row];
            } else {
                arrayUsers = [dict valueForKey:@"otherUsers"];
                friend = arrayUsers[indexPath.row];
            }
        } else if (dicArrays.count == 2) {
            if (indexPath.section == 0) {
                NSDictionary *dic = self.searchedTableData[0];
                NSArray *arrayUsers = [dic valueForKey:@"myUsers"];
                friend = arrayUsers[indexPath.row];
            } else if (indexPath.section == 1) {
                NSDictionary *dic = self.searchedTableData[1];
                NSArray *arrayUsers = [dic valueForKey:@"otherUsers"];
                friend = arrayUsers[indexPath.row];
            }
        }
    }
    
    [self configureCell:cell fromFriend:friend fromIndexPath:indexPath];
    return cell;
}


- (void)configureCell:(ContactCell*)cell fromFriend:(RLMFriend*)friend fromIndexPath:(NSIndexPath*)indexPath {
    if ([friend.friendFromMyList isEqualToNumber:@(1)]) {
        [cell.plusImageView setHidden:NO];
        [cell.seenAn setHidden:YES];
        cell.topNameConstr.constant = 8;
        [cell.onlineView setHidden:YES];
        NSString *fullName = [NSString stringWithFormat:@"%@", friend.name];
        cell.name.text = fullName;
        [cell.avatarView setAvatar:@"" orInitialsView:fullName];
        [cell.avatarView setNeedsDisplay];
    } else {
        cell.topNameConstr.constant = 2;
        [cell.seenAn setHidden:NO];
        [cell.plusImageView setHidden:YES];
        [cell.onlineView setHidden:NO];
        NSString *fullName = [NSString stringWithFormat:@"%@ %@", friend.name , friend.lastName];
        cell.name.text = fullName;
        [cell.avatarView setAvatar:friend.avatar orInitialsView:fullName gradientColorsArray:(NSArray*)friend.gradient];
        [cell.avatarView setNeedsDisplay];
    }
    
    if ([friend.online intValue] == 1) {
        cell.seenAn.text = @"Online";
        cell.seenAn.textColor = [UIColor colorWithRed:80.0f/255.0f green:181.0f/255.0f blue:171.0f/255.0f alpha:1];
        cell.onlineView.backgroundColor = [UIColor colorWithRed:80.0f/255.0f green:181.0f/255.0f blue:171.0f/255.0f alpha:1];
    } else {
        cell.onlineView.backgroundColor = [UIColor redColor];
        cell.seenAn.textColor = [UIColor blackColor];
        NSString *time = [NSString convertResponseToCellsTimeString:friend.lastOnline];
        cell.seenAn.text = time;
    }
    [cell.onlineView setHidden:YES];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)openSMSFromUserPhone:(NSString*)phone {
    MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
    picker.messageComposeDelegate = self;
    picker.recipients = [NSArray arrayWithObjects:phone,nil];
    picker.body = NSLocalizedString(@"Dude, I found a very cool messenger here, where you can learn languages, come in and download. |reference|", nil);
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure");
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAllertViewWithTittleWithAction:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex == controller.cancelButtonIndex) {
            self.isFiltered = NO;
            self.isSearched = NO;
            [self getUserFromServer];
        }
    }];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

@end
