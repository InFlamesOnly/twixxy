//
//  RequestManager.h
//  Twixi
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RLMUser.h"

@interface RequestManager : NSObject

+ (RequestManager*)sharedManager;

//- (void)reqisterUserWithLogin:(NSString*)login
//                  googleToken:(NSString*)googleToken
//                facebookToken:(NSString*)facebookToken
//                        email:(NSString*)email
//                         name:(NSString*)name
//                     lastName:(NSString*)lastName
//                     password:(NSString*)password
//                 successBlock:(void(^)(NSDictionary*success)) success
//                onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)autorisationUserWithEmail:(NSString*)email
                         password:(NSString*)password
                     successBlock:(void(^)(NSDictionary*success)) success
                    onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)removeUserSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getFriendsList:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)removeUserFromId:(NSInteger)userId
            successBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)addUserFromuserId:(NSNumber*)userId
              succesBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)postAvatarFromBundlePath:(NSString*)path
                     succesBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getInfoUserSuccessBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)creatPublicOrPrivateChat:(NSString*)privatoOrPublic
                      withUserId:(NSInteger)userId
                    orArrayUsers:(NSNumber*)arrayUsers
                     succesBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)removePublicOrPrivateChatWithId:(NSInteger)chatId
                            succesBlock:(void(^)(NSDictionary*success)) success
                          onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getChatListFromUsersuccesBlock:(void(^)(NSDictionary*success)) success
                         onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)sendMessage:(NSInteger)chatId
            message:(NSString*)message
        succesBlock:(void(^)(NSDictionary*success)) success
      onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)registerNotificationWithFCMToken:(NSString*)fcmToken
                            successBlock:(void(^)(NSDictionary*success)) success
                           onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getChatHistoryWithId:(NSNumber*)chatId
                     andType:(NSString*)type
                successBlock:(void(^)(NSDictionary*success)) success
               onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getUsersFromName:(NSString*)name
            successBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)loginWithGoogle:(NSString*)googleToken
           successBlock:(void(^)(NSDictionary*success)) success
          onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)loginWithFacebook:(NSString*)faceBookToken
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)restorePassword:(NSString*)email
           successBlock:(void(^)(NSDictionary*success)) success
          onFalureBlock:(void(^)(NSInteger statusCode)) fail ;

- (void)getTranslationFromSourseLang:(NSString*)sourceLng
                          targetLang:(NSString*)targetLng
                          phraseText:(NSString*)text
                        successBlock:(void(^)(NSDictionary*success)) success
                       onFalureBlock:(void(^)(NSInteger statusCode)) fail ;

- (void)sendServerToConnectUser;
- (void)sendServerToDissconectUsersuccesBlock:(void(^)(NSDictionary*success)) success
                                onFalureBlock:(void(^)(NSInteger statusCode)) fail ;

- (void)subscribeWithStripeToken:(NSString*)stripeToken
                        stripeId:(NSString*)stripeId
                    successBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getSubscribeListWithSuccessBlock:(void(^)(NSDictionary*success)) success
                           onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getLanguageListWithSuccessBlock:(void(^)(NSDictionary*success)) success
                          onFalureBlock:(void(^)(NSInteger statusCode)) fail;

//---------------------------------------------------------- NEW API METHODS

- (void)authUserWithPhone:(NSString*)phone
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)loginUserWithCode:(NSString*)code
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getUserSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getContactListSuccessBlock:(void(^)(NSDictionary*success)) success
                     onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getUserSearch:(NSString*)string
     withSuccessBlock:(void(^)(NSDictionary*success)) success
        onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)addContactFromId:(NSNumber*)userId
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)deleteContactFromId:(NSNumber*)userId
           withSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getChatsWithPage:(NSNumber*)page
              pageOffset:(NSNumber*)pageOffset
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)createChatsWithUserId:(NSNumber*)userId
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)removeChatsWithChatId:(NSNumber*)chatId
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)markChatsWithChatId:(NSNumber*)chatId
           withSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)uploadImageFromBundlePath:(NSString*)path
                 withSuccessBlock:(void(^)(NSDictionary*success)) ok
                    onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)subscribeWithCartToken:(NSString*)cartTokken andSubscribeId:(NSString*)subscribeId
              withSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)updateUserName:(NSString*)name
              lastName:(NSString*)lastName
              nickName:(NSString*)nickName
                avatar:(NSString*)avatar
      withSuccessBlock:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getChatsHistoryFromChatId:(NSNumber*)chatId
                         withPage:(NSNumber*)page
                       pageOffset:(NSNumber*)pageOffset
                 withSuccessBlock:(void(^)(NSDictionary*success)) success
                    onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)sendMessage:(NSString*)message
         fromChatId:(NSNumber*)chatId
   withSuccessBlock:(void(^)(NSDictionary*success)) success
      onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)translateText:(NSString*)text
       withSourceLang:(NSString*)soutceLang
           toParsLang:(NSString*)parseLang
     withSuccessBlock:(void(^)(NSDictionary*success)) success
        onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)updateUserFCMToken:(NSString*)token
          withSuccessBlock:(void(^)(NSDictionary*success)) success
             onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)getNewLanguageListWithSuccessBlock:(void(^)(NSDictionary*success)) success
                             onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)updateCurrentLanguage:(NSString*)langCode
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)updateSettingsLanguage:(NSString*)langCode
               andNotification:(NSNumber*)notification
              withSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)sendContactListFromSync:(NSArray*)contactList
               withSuccessBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)setDisconnectFromServerWithSuccessBlock:(void(^)(NSDictionary*success)) success
                                  onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)removeMessageFromChat:(NSNumber*)chatId
                    messageId:(NSNumber*)messageId
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)changeMessageFromChat:(NSNumber*)chatId
                    messageId:(NSNumber*)messageId
                  fromMessage:(NSString*)message
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)removeChat:(NSNumber*)chatId
  withSuccessBlock:(void(^)(NSDictionary*success)) success
     onFalureBlock:(void(^)(NSInteger statusCode)) fail ;
- (void)getLanguageSchemeWithSuccessBlock:(void(^)(NSDictionary*success)) success
                            onFalureBlock:(void(^)(NSInteger statusCode)) fail;
- (void)updateScheme:(NSNumber*)scheme
    withSuccessBlock:(void(^)(NSDictionary*success)) success
       onFalureBlock:(void(^)(NSInteger statusCode)) fail ;

- (void)checkUserNickName:(NSString*)nickname
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)updateStadyRate:(int)rate
       withSuccessBlock:(void(^)(NSDictionary*success)) success
          onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)subscribeWithSubscribeId:(NSString*)subscribeId
                withSuccessBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getDictListSuccessBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getLevels:(void(^)(NSDictionary*success)) success
    onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getDictListWithLevel:(NSString*)level
                successBlock:(void(^)(NSDictionary*success)) success
               onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)getSubscribeList:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)deleteUserwWthSuccessBlock:(void(^)(NSDictionary*success)) success
                     onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)sendTimezoneWithSuccessBlock:(void(^)(NSDictionary*success)) success
                       onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)addUserToBleckList:(NSNumber*)userId
          withSuccessBlcok:(void(^)(NSDictionary*success)) success
             onFalureBlock:(void(^)(NSInteger statusCode)) fail;

- (void)removeUserToBleckList:(NSNumber*)userId
             withSuccessBlcok:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail;



@end
