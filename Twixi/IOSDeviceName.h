//
//  IOSDeviceName.h
//  Twixi
//
//  Created by macOS on 03.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface IOSDeviceName : NSObject

- (NSString *) deviceName;

@end

NS_ASSUME_NONNULL_END
