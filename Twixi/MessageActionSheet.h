//
//  MessageActionSheet.h
//  Twixi
//
//  Created by macOS on 16.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MessageActionSheetDelegate

- (void)copy;
- (void)translate;
- (void)close;
- (void)change;
- (void)remove;

@end

@interface MessageActionSheet : UIView

@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UIImageView *changeImage;
@property (weak, nonatomic) IBOutlet UILabel *changeText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *changeConstr;

@property (weak, nonatomic) IBOutlet UIButton *removeButton;
@property (weak, nonatomic) IBOutlet UIImageView *removeImage;
@property (weak, nonatomic) IBOutlet UILabel *removeText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *removeConstr;

@property (nonatomic,retain) id <MessageActionSheetDelegate> delegate;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *changeConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allViewConstraint;


- (void)hideChange;
- (void)showChange;
- (void)showChangeFromFriend;

@end
