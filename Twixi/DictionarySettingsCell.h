//
//  DictionarySettingsCell.h
//  Twixi
//
//  Created by macOS on 08.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DictionarySettingsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UIImageView *flagImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *weightConstr;

@end

NS_ASSUME_NONNULL_END
