//
//  RLMSubscribe.h
//  Twixi
//
//  Created by macOS on 04.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMSubscribe : RLMObject

@property NSNumber <RLMInt> *value;
@property float price;

+ (void)saveFromServerResponse:(NSDictionary*)response;

@end
