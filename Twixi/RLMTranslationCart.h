//
//  RLMTranslationCart.h
//  Twixi
//
//  Created by macOS on 13.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMExamples.h"

@interface RLMTranslationCart : RLMObject

@property NSString *dictKey;

@property NSString *word;
@property NSString *translations;
@property NSString *pretext; //предлог
@property NSString *verb; //глагол
@property NSString *naming; //наречение
@property NSString *adjective; //имя прелагательное
@property NSString *audioURL; //имя прелагательное
@property NSNumber <RLMInt> *wordId;
@property NSNumber <RLMInt> *lvl;

@property (nonatomic, strong, readwrite) RLMArray<RLMExamples *>< RLMExamples> *examples;

- (id)initWithServerResponse:(NSDictionary*)response;
+ (void)saveFromServerResponse:(NSDictionary*)response;

@end
