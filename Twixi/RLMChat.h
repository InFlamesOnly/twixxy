//
//  RLMChat.h
//  Twixi
//
//  Created by macOS on 23.09.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMMessage.h"
#import "RLMGradientValue.h"

@interface RLMChat : RLMObject

@property NSString *chatKey;
@property NSNumber <RLMInt> *chatId;
@property NSNumber <RLMInt> *messageCount;
@property NSString *name;
@property NSString *userName;
@property NSString *userLastName;
@property NSString *nickName;
@property NSString *avatar;
@property NSString *createdAt;
@property NSString *updatedAt;
@property NSString *lastMessage;
@property NSString *sentAt;
@property NSString *userLastOnline;
@property NSNumber <RLMInt> *friendId;
//TODO avatar image Realm
@property NSNumber <RLMInt> *type;

@property (nonatomic, strong, readwrite) RLMArray<RLMGradientValue *>< RLMGradientValue> *gradient;
@property (nonatomic, strong, readwrite) RLMArray<RLMMessage *>< RLMMessage> *message;

- (id)initWithServerResponse:(NSDictionary*)response;
- (RLMChat*)findInteresFromArray:(NSArray*)array chatId:(NSNumber*)chatId;
+ (void)updateChat:(RLMChat*)newChat;
- (void)repleceFirstAndAddToLast:(RLMMessage*)message fromChat:(NSNumber*)chatId;

- (id)initWithServerResponseFromPush:(NSDictionary*)response;

- (NSMutableArray*)findFromFriendId:(NSNumber*)friendId;
+ (NSArray*)lastTenChats;
+ (int)arrayObjectsCount;

+ (void)saveFromServerResponse:(NSDictionary*)response;
- (BOOL)findMessage:(RLMMessage*)findMessage;

- (RLMMessage*)findMessageFromChat:(NSNumber*)mId;

@end

RLM_ARRAY_TYPE(RLMChat)
