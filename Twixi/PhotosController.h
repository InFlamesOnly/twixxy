//
//  PhotosController.h
//  Twixi
//
//  Created by macOS on 07.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotosControllerrDelegate

- (void)sendPhoto:(NSString*)photoPath;

@end

NS_ASSUME_NONNULL_BEGIN

@interface PhotosController : NSObject

@property (nonatomic,retain) id <PhotosControllerrDelegate> delegate;

-(void)showImagePicker;

@end

NS_ASSUME_NONNULL_END
