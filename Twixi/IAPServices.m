//
//  IAPServices.m
//  Twixi
//
//  Created by macOS on 01.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "IAPServices.h"
#import "RLMSubscribe.h"
#import "RequestManager.h"
//https://habr.com/post/335070/

//weak 1440984215 DM.twiximassanger.com.Twixi.weak
//mounth 1441531792 DM.twiximassanger.com.Twixi.mounth
//year 1441531832 DM.twiximassanger.com.Twixi.year

@interface IAPServices () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (strong, nonatomic) NSString *code;

@end

@implementation IAPServices

+ (instancetype)sharedInstance {
    static IAPServices *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[IAPServices alloc] init];
    });
    return sharedInstance;
}

- (void)getProducts:(NSString*)code {
    NSLog(@"getProducts");
    if([SKPaymentQueue canMakePayments]){
        NSLog(@"Пользователь может совершить покупку");
        self.code = code;
        //Если у вас больше чем одна встроенная покупка, и вы хотели бы
        //чтобы пользователь купил другой продукт просто объявите
        //другую функцию и измените kRemoveAdsProductIdentifier на
        //id другого продукта
//    SKProductsRequest *productRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithObject:code]];
        SKProductsRequest *productRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithObject:code]];
        //
//        SKProductsRequest *productRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithObject:@"twixxy.test"]];

        
//        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObjects:@"DM.twiximassanger.com.Twixi.weak", @"DM.twiximassanger.com.Twixi.mounth" , @"DM.twiximassanger.com.Twixi.year"]];
        productRequest.delegate = self;
        [productRequest start];
        
    }
    else {
        NSLog(@"Пользователь не может производить платежи из-за родительского контроля");
    }
}



- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    int count = (int)response.products.count;
    if(count > 0) {
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Продукты доступны!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        NSLog(@"Нет доступных продуктов");
        //Это вызывается, если ваш идентификатор продукта недействителен.
    }
}

- (void)purchase:(SKProduct *)product{
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void) restore {
    //Это называется, когда пользователь восстанавливает покупки, вы должны подключить это к кнопке
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSLog(@"Получены восстановленные транзакции: %lu", (unsigned long)queue.transactions.count);
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //вызывается когда пользователь успешно восстанавливает покупку
            NSLog(@"Transaction state -> Restored");
            
            // если у вас более одного продукта для покупки в приложении,
            // вы восстанавливаете правильный продукт для идентификатора.
            // Например, вы можете использовать
            // if (productID == kRemoveAdsProductIdentifier)
            // для получения идентификатора продукта для
            // восстановленных покупок, вы можете использовать
            //
            //NSString *productID = transaction.payment.productIdentifier;
            
            //userBuySubscribe

            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            break;
        }
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        // если у вас несколько покупок в приложении,
        // вы можете получить идентификатор продукта для этой транзакции
        // с помощью transaction.payment.productIdentifier
        //
        // затем, проверьте идентификатор для продукта
        //, который вы определили для проверки какой продукт пользователь
        // только что купил
        
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //Вызывается, когда пользователь находится в процессе покупки, не добавляйте свой код.
                break;
            case SKPaymentTransactionStatePurchased:
                [self userByProduct:self.code];
                //Это вызывается, когда пользователь успешно приобрел продукт
                //Вы можете добавить свой код для того, что вы хотите, чтобы произошло, когда пользователь совершает покупку, для этого урока мы используем удаление объявлений
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                NSLog(@"Transaction state -> Purchased");
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Transaction state -> Restored");
                //добавьте сюда тот же код, что вы делали для SKPaymentTransactionStatePurchased
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                //вызывается когда транзакция не закончена
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    //пользователь отменил покупку ;(
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateDeferred:
                
                break;
        }
    }
}

- (void)userByProduct:(NSString*)code {
    if ([code isEqualToString:@"DM.twiximassanger.com.Twixi.weaksubscribe"]) {
        RLMSubscribe *subscribe = [RLMSubscribe allObjects][0];
//        [[RequestManager sharedManager] subscribeWithSubscribeId:subscribe.value withSuccessBlock:^(NSDictionary *success) {
            [self showAllertViewWithTittle:@"Accept" andMessage:[NSString stringWithFormat:@"Ваша подписка : %@", subscribe.value]];
            RLMUser *currentUser = [RLMUser currentUser];
            [[RLMRealm defaultRealm] beginWriteTransaction];
            currentUser.subType = @"DM.twiximassanger.com.Twixi.weaksubscribe";
            currentUser.userBuySubscribe = @(1);
            [[RLMRealm defaultRealm] commitWriteTransaction];
//        } onFalureBlock:^(NSInteger statusCode) {
//
//        }];
        
    } else if ([code isEqualToString:@"DM.twiximassanger.com.Twixi.onemonth"]) {
        RLMSubscribe *subscribe = [RLMSubscribe allObjects][1];
//        [[RequestManager sharedManager] subscribeWithSubscribeId:subscribe.value withSuccessBlock:^(NSDictionary *success) {
            [self showAllertViewWithTittle:@"Accept" andMessage:[NSString stringWithFormat:@"Ваша подписка : %@", subscribe.value]];
            RLMUser *currentUser = [RLMUser currentUser];
            [[RLMRealm defaultRealm] beginWriteTransaction];
            currentUser.subType = @"DM.twiximassanger.com.Twixi.onemonth";
            currentUser.userBuySubscribe = @(1);
            [[RLMRealm defaultRealm] commitWriteTransaction];
//        } onFalureBlock:^(NSInteger statusCode) {
//
//        }];
        
    } else if ([code isEqualToString:@"DM.twiximassanger.com.Twixi.threemonth"]) {
        RLMSubscribe *subscribe = [RLMSubscribe allObjects][2];
//        [[RequestManager sharedManager] subscribeWithSubscribeId:subscribe.value withSuccessBlock:^(NSDictionary *success) {
            [self showAllertViewWithTittle:@"Accept" andMessage:[NSString stringWithFormat:@"Ваша подписка : %@", subscribe.value]];
            RLMUser *currentUser = [RLMUser currentUser];
            [[RLMRealm defaultRealm] beginWriteTransaction];
            currentUser.subType = @"DM.twiximassanger.com.Twixi.threemonth";
            currentUser.userBuySubscribe = @(1);
            [[RLMRealm defaultRealm] commitWriteTransaction];
//        } onFalureBlock:^(NSInteger statusCode) {
//
//        }];
    }
}

- (void)userBuySubscribe:(NSString*)code {
    //currentUser = UserIsSubscribe = true data = NSdate.currentDate()
    
//    ADBannerView *banner;
//    [banner setAlpha:0];
//    areAdsRemoved = YES;
//    removeAdsButton.hidden = YES;
//    removeAdsButton.enabled = NO;
//    [[NSUserDefaults standardUserDefaults] setBool:areAdsRemoved forKey:@"areAdsRemoved"];
    // используем NSUserDefaults, чтобы мы могли узнать, купил ли пользователь это или нет.
    // было бы лучше использовать доступ KeyChain или что-то более безопасное
    // для хранения пользовательских данных, поскольку NSUserDefaults можно изменить.
    // Обычный пользователь, не сможете легко его изменить, но
    // лучше использовать что-то более безопасное, чем NSUserDefaults.
    // Для этого урока мы будем использовать NSUserDefaults
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    UIViewController *currentTopVC = [self topViewController];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self.delegate closeView];
    }]];
    [currentTopVC presentViewController:alertController animated:YES completion:NULL];
}

@end
