//
//  Payment.h
//  Twixi
//
//  Created by macOS on 03.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Payment : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *zip;

@end
