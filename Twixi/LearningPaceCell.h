//
//  LearningPaceCell.h
//  Twixi
//
//  Created by macOS on 08.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LearningPaceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *value;
@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewConstr;

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;

@property (weak, nonatomic) IBOutlet UIImageView *checkBoxImage;

@end

NS_ASSUME_NONNULL_END
