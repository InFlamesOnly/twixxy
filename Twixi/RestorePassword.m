//
//  RestorePassword.m
//  Twixi
//
//  Created by macOS on 27.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RestorePassword.h"
#import "RequestManager.h"
#import "PCAngularActivityIndicatorView.h"
#import "UIView+ShadowToButton.h"
#import "NSString+EmailValidation.h"

@interface RestorePassword ()

@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UITextField *email;

@property (weak, nonatomic) IBOutlet UIButton *restoreButton;

@end

@implementation RestorePassword

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.restoreButton setEnabled:NO];
    [UIView addGreyGradientToButton:self.restoreButton];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                                  action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewDidLayoutSubviews {
    [UIView addGradientToView:self.view];
    [UIView addGreyGradientToButton:self.restoreButton];
    CALayer *layer = [self.restoreButton.layer.sublayers firstObject];
    [layer removeFromSuperlayer];
    [UIView addShadowToButton:self.restoreButton];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString *restore = self.email.text;
    
    if (textField == self.email) {
        restore = [self.email.text stringByReplacingCharactersInRange:range
                                                                  withString:string];
    }
    
    BOOL emailIsValid = [restore isValidEmail];
    
    if (emailIsValid == NO) {
        CALayer *layer = [self.restoreButton.layer.sublayers firstObject];
        if ([layer.name isEqualToString:@"gradient"] || [layer.name isEqualToString:@"grey"]) {
            NSLog(@"remove gradient");
            [layer removeFromSuperlayer];
            [UIView addGreyGradientToButton:self.restoreButton];
            [self.restoreButton setEnabled:NO];
        }
        
    } else {
        CALayer *layer = [self.restoreButton.layer.sublayers firstObject];
        if ([layer.name isEqualToString:@"grey"]) {
            NSLog(@"remove grey");
            [layer removeFromSuperlayer];
            [self.restoreButton setEnabled:YES];
            [UIView addGradientToLoginButton:self.restoreButton];
        }
    }
    return YES;
}

- (void)addActivityIndicator {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    [self.view addSubview:self.activityView];
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}

- (void)removeActivity {
    [self.activity removeFromSuperview];
    [self.activityView removeFromSuperview];
}

-(void)dismissKeyboard {
    [self.email resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)restorePassword:(id)sender {
//    [self addActivityIndicator];
//    [[RequestManager sharedManager] restorePassword:self.email.text successBlock:^(NSDictionary *success) {
//        [self showAllertViewWithTittleWithAction:@"" andMessage:@"Пароль был успешно отправлен на почту"];
//        [self removeActivity];
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self removeActivity];
//    }];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)showAllertViewWithTittleWithAction:(NSString*) tittle andMessage:(NSString*) message {
//    UIAlertController *alertController = [UIAlertController new];
//
//    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
//    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//        [self.navigationController popViewControllerAnimated:YES];
//        
//    }]];
//}
- (void)showAllertViewWithTittleWithAction:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
//    NSMutableAttributedString *hogan = [[NSMutableAttributedString alloc] initWithString:@"Presenting the great... Hulk Hogan!"];
//    [hogan addAttribute:NSFontAttributeName
//                  value:[UIFont systemFontOfSize:10]
//                  range:NSMakeRange(24, 11)];
//    [alertVC setValue:hogan forKey:@"attributedTitle"];
    
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action){
                                                   [self.navigationController popViewControllerAnimated:YES];

                                               }];
    
    [alertController addAction: ok];
//
//    UIAlertAction *Cancel = [UIAlertAction actionWithTitle:@"Cancel"
//                                                     style:UIAlertActionStyleDestructive
//                                                   handler:^(UIAlertAction *action){
//                                                       //add code to make something happen once tapped
//                                                   }];
//
//    [alertController addAction: Cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
//    alertController.view.tintColor = [UIColor colorWithRed:0.0/255.0 green:99.0/255.0 blue:65.0/255.0 alpha:1.0];
}

@end
