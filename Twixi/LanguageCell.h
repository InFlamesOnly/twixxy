//
//  LanguageCell.h
//  Twixi
//
//  Created by macOS on 18.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *languageText;
@property (weak, nonatomic) IBOutlet UIImageView *languageImage;

@property (weak, nonatomic) IBOutlet UIImageView *checkImage;

@end
