//
//  ChatsContactCell.h
//  Twixi
//
//  Created by macOS on 15.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarView.h"

@interface ChatsContactCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UIView *messageBcgView;
//@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *lastMassage;
@property (weak, nonatomic) IBOutlet UILabel *lastActivity;
@property (weak, nonatomic) IBOutlet UIView *massageCountView;
@property (weak, nonatomic) IBOutlet UILabel *messageCount;

//@property (weak, nonatomic) IBOutlet UILabel *initials;

@end
