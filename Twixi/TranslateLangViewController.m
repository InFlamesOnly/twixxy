//
//  TranslateLangViewController.m
//  Twixi
//
//  Created by macOS on 09.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "TranslateLangViewController.h"
#import "RequestManager.h"
#import "LanguageViewController.h"
#import "RLMScheme.h"

@interface TranslateLangViewController () <LanguageViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *codeNameButton;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;

@property (strong, nonatomic) RLMUser *currentUser;

@property (strong, nonatomic) RLMLanguage *currentLang;


@end

@implementation TranslateLangViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentUser = [RLMUser currentUser];
    [self getLangFromScheme];
    [self setLangInfoToUI:self.currentLang];
    [self saveFromRealm];
}

- (void)getLangFromScheme {
    RLMScheme *sc = [[RLMScheme alloc] init];
    self.currentLang = [sc arrayFromNeedsLanguage:self.currentUser.myLangCode][0];
}

- (void)setLangInfoToUI:(RLMLanguage*)lang {
    [self.codeNameButton setTitle:lang.languageName forState:UIControlStateNormal];
    [self.flagImageView setImage:[UIImage imageNamed:lang.languageCode]];
}

- (void)saveFromRealm {
    [RLMUser saveTrnLangCode:self.currentLang.languageCode];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"language"]) {
        LanguageViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.isFirstTrLanguage = YES;
    }
}

-(IBAction)nextScreen:(id)sender {
    [self startActivity];
    [self saveFromRealm];
    RLMScheme *scheme = [RLMScheme updateMyScheme];
    if (scheme != nil){
        [self updateSchemeFromServer:scheme];
    }
}

- (void)updateSchemeFromServer:(RLMScheme*)scheme {
    [[RequestManager sharedManager] updateScheme:scheme.schemeId withSuccessBlock:^(NSDictionary *success) {
        [self removeActivity];
        [RLMUser userIsRegisted];
        [self showSubscribe];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)showSubscribe {
    [self performSegueWithIdentifier:@"showSubscribeScreen" sender:self];
}


- (IBAction)goToLanguageVC:(id)sender {
    [self performSegueWithIdentifier:@"language" sender:self];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getLanguage:(RLMLanguage *)language {
    self.currentLang = language;
    [self setLangInfoToUI:self.currentLang];
}

@end
