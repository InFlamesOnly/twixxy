//
//  UIView+ShadowToButton.m
//  Twixi
//
//  Created by macOS on 02.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "UIView+ShadowToButton.h"

@implementation UIView (ShadowToButton)

+ (void)addGradientToView:(UIView*)view {
    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    [view.layer insertSublayer:gradient atIndex:0];
}

+ (void)addGradientToViewWithColor:(UIColor*)firstColor secondColor:(UIColor*)secondColor fromView:(UIView*)view {    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    [view.layer insertSublayer:gradient atIndex:0];
}

//+ (void)addGradientToView:(UIView*)view {
//    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
//    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
//    
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    
//    gradient.frame = view.bounds;
//    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
//    
//    gradient.startPoint = CGPointMake(0.0, 0.5);
//    gradient.endPoint = CGPointMake(1.0, 0.5);
//    
//    [view.layer insertSublayer:gradient atIndex:0];
//}

+ (void)addShadowToButton:(UIButton*)button {
    button.layer.shadowRadius  = 2.0f;
    button.layer.shadowColor   = [UIColor blackColor].CGColor;
    button.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    button.layer.shadowOpacity = 0.5f;
    button.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -0.7f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(button.bounds, shadowInsets)];
    button.layer.shadowPath    = shadowPath.CGPath;
}

+ (void)addGradientToLoginButton:(UIButton*)button {
    UIColor *firstColor = [UIColor colorWithRed:254.0f/255.0f green:96.0f/255.0f blue:118.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:68.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = button.bounds;
    gradient.cornerRadius = 3;
    gradient.masksToBounds = YES;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    gradient.name = @"gradient";
    [button.layer insertSublayer:gradient atIndex:0];
}

+ (void)addGreyGradientToButton:(UIButton*)button {
    UIColor *secondColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = button.bounds;
    gradient.cornerRadius = 3;
    gradient.masksToBounds = YES;
    gradient.colors = @[(id)secondColor.CGColor];
    gradient.name = @"grey";
    
    [button.layer insertSublayer:gradient atIndex:0];
}

+ (void)addGradientToCell:(UIView*)view {
    UIColor *firstColor = [UIColor colorWithRed:56.0f/255.0f green:184.0f/255.0f blue:242.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:132.0f/255.0f green:60.0f/255.0f blue:246.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.cornerRadius = 3;
    gradient.masksToBounds = YES;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    gradient.name = @"gradient";
    [view.layer insertSublayer:gradient atIndex:0];
}

+ (void)addGradientToTextField:(UIView*)view {
    UIColor *firstColor = [UIColor colorWithRed:252.0f/255.0f green:96.0f/255.0f blue:118.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:68.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.cornerRadius = 3;
    gradient.masksToBounds = YES;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    gradient.name = @"gradient";
    [view.layer insertSublayer:gradient atIndex:0];
}

@end
