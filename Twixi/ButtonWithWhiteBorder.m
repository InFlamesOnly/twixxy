//
//  ButtonWithWhiteBorder.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ButtonWithWhiteBorder.h"

@implementation ButtonWithWhiteBorder

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addBorder];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self addBorder];
    }
    return self;
}

- (void)addBorder {
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.cornerRadius = 5;
    self.clipsToBounds = YES;
}

@end
