//
//  TelephoneCodeViewController.h
//  Twixi
//
//  Created by macOS on 03.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Code.h"


@protocol TelephoneCodeViewControllerDelegate

- (void)getCode:(Code*)code;

@end


@interface TelephoneCodeViewController : UIViewController

@property (nonatomic,retain) id <TelephoneCodeViewControllerDelegate> delegate;

@end
