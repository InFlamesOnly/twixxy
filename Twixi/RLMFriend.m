//
//  RLMFriend.m
//  Twixi
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RLMFriend.h"
#import "NSDictionary+DictCategory.h"

@implementation RLMFriend

+ (NSString *)primaryKey {
    return @"friendPrimaryKay";
}

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        response = [response dictionaryRemovingNSNullValues];
        self.friendPrimaryKay = [NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
        self.friendId = [response valueForKey:@"id"];
        self.online = [response valueForKey:@"online"];
        self.nickName = [response valueForKey:@"nickname"];
        self.name = [response valueForKey:@"first_name"];
        if ([response valueForKey:@"avatar"] == [NSNull null]) {
            self.avatar = @"";
        } else {
            self.avatar = [response valueForKey:@"avatar"];
        }
        
        self.updateAt = [response valueForKey:@"updated_at"];
        self.phone = [response valueForKey:@"phone"];
        self.lastOnline = [response valueForKey:@"last_login"];
        self.lastName = [response valueForKey:@"last_name"];
        self.addDate = [response valueForKey:@"created_at"];
        //TODO
        if ([self.avatar isEqualToString:@""]) {
            NSArray *gradientOneArray = [NSArray new];
            NSArray *gradientTwoArray = [NSArray new];
            if ([response valueForKey:@"gradient"] == [NSNull null]) {
                gradientOneArray = @[@(92),@(107),@(192)];
                gradientTwoArray = @[@(48),@(63),@(159)];
            } else {
                gradientOneArray = [response valueForKey:@"gradient"][0];
                gradientTwoArray = [response valueForKey:@"gradient"][1];
            }
        
            for (NSNumber *i in gradientOneArray) {
                RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
                [self.gradient addObject:value];
            }
            for (NSNumber *i in gradientTwoArray) {
                RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
                [self.gradient addObject:value];
            }
        }
    }
    return self;
}

- (RLMFriend*)findFriendFromArray:(NSArray*)array chatId:(NSNumber*)friendId {
    NSString *myPrimaryKey = [NSString stringWithFormat:@"%@",friendId];
    RLMFriend *friend = [RLMFriend objectForPrimaryKey:myPrimaryKey];
    return friend;
}

+ (void)saveFromServerResponse:(NSDictionary*)response {
    for (NSDictionary *dic in [response valueForKey:@"data"]) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        RLMFriend *friend = [[RLMFriend alloc] initWithServerResponse:dic];
        RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:friend.friendId];
        if (findFriend == nil) {
            [realm beginWriteTransaction];
            [realm addObject:friend];
            [realm commitWriteTransaction];
        }
    }
}

//'id', 'name', 'avatar', 'updated_at', 'last_online'

@end
