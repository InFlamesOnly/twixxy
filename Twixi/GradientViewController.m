//
//  GradientViewController.m
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "GradientViewController.h"
#import "PCAngularActivityIndicatorView.h"


@interface GradientViewController ()

@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@property (strong, nonatomic) CAGradientLayer *gradient;

@end


@implementation GradientViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addGradientToView];
}

- (void)removeGradient {
    [self.gradient removeFromSuperlayer];
}


- (void)addGradientToView {
    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
    
    self.gradient = [CAGradientLayer layer];
    
    self.gradient.frame = self.view.bounds;
    self.gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    [self.view.layer insertSublayer:self.gradient atIndex:0];
}

- (void)startActivity {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    [self.view addSubview:self.activityView];
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}

- (void)startActivityFromTabBar {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    [self.view addSubview:self.activityView];
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.tabBarController.view addSubview:self.activityView];
    [self.tabBarController.view addSubview:self.activity];
}

- (void)removeActivity {
    [self.activityView removeFromSuperview];
    [self.activity removeFromSuperview];
}


@end
