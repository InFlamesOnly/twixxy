//
//  TelephoneCodeViewController.m
//  Twixi
//
//  Created by macOS on 03.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "TelephoneCodeViewController.h"
#import "CountryListDataSource.h"
#import "CodeCell.h"

@interface TelephoneCodeViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (strong, nonatomic) IBOutlet UITableView *codeTableView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIView *bckSearchView;

@property (strong, nonatomic) NSArray *codeArray;
@property (strong, nonatomic) NSArray *filteredArray;

@property (strong, nonatomic) NSLayoutConstraint *widthSearchViewConstraint;

@property int startWidth;
@property BOOL isSearched;


@end

@implementation TelephoneCodeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self calculateStartHeightAndAddConstr];
    [self getCountryList];
    [self parseCountryList];
}

- (void)getCountryList {
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    self.codeArray = [dataSource countries];
}

- (void)parseCountryList {
    self.codeArray = [Code parseCountryList:self.codeArray];
    [self.codeTableView reloadData];
}


- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UISearchView methods
- (void)calculateStartHeightAndAddConstr {
    int value = [self widthOfString:self.searchTextField.placeholder withFont:[UIFont systemFontOfSize:14]] + 8 + 12 + 8 +8 + 16 + 8 + 1;
    self.startWidth = value;
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.bckSearchView
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:value];
    self.widthSearchViewConstraint = widthConstraint;
    [self.bckSearchView addConstraints:@[widthConstraint]];
    [self.view layoutIfNeeded];
}

- (CGFloat)widthOfString:(NSString *)string withFont:(NSFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.codeTableView]) {
        
        [self.searchTextField resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSUInteger characterCount = [searchStr length];
    
    searchStr = [searchStr lowercaseString];
    
    if (characterCount > 0) {
        [self.clearButton setHidden:NO];
        [self openSearchView];
        self.isSearched = YES;
        NSPredicate *pr = [NSPredicate predicateWithFormat:@"name CONTAINS[c] %@",[searchStr uppercaseString]];
        self.filteredArray = [self.codeArray filteredArrayUsingPredicate:pr];
        [self.codeTableView reloadData];
    } else {
        [self.clearButton setHidden:YES];
        [self closeSearchView];
        self.isSearched = NO;
        [self.codeTableView reloadData];
    }
    return YES;
}

- (void)openSearchView {
    self.widthSearchViewConstraint.constant = UIScreen.mainScreen.bounds.size.width - 32;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)closeSearchView {
    self.widthSearchViewConstraint.constant = self.startWidth;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}


- (IBAction)clearSearch:(id)sender {
    self.searchTextField.text = @"";
    [self closeSearchView];
    self.isSearched = NO;
    [self.clearButton setHidden:YES];
    [self.codeTableView reloadData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self openSearchView];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length == 0) {
        [self closeSearchView];
    }
}

#pragma mark - TableView delegate and datasource
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Code *code ;
    if (self.isSearched == YES) {
        code = self.filteredArray[indexPath.row];
    } else {
        code = self.codeArray[indexPath.row];
    }
    [self.delegate getCode:code];
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearched == YES) {
        return self.filteredArray.count;
    } else {
        return self.codeArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CodeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CodeCell" forIndexPath:indexPath];
    Code *code = [[Code alloc] init];
    if (self.isSearched == YES) {
        code = self.filteredArray[indexPath.row];
    } else {
        code = self.codeArray[indexPath.row];
    }
    cell.codeName.text = code.name;
    cell.codeValue.text = code.code;
    cell.flag.image = [UIImage imageNamed:code.image];
    return cell;
}

@end
