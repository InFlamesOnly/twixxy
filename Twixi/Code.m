//
//  Code.m
//  Twixi
//
//  Created by macOS on 03.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "Code.h"

@implementation Code

- (id)initWithName:(NSString*)name image:(NSString*)image code:(NSString*)code {
    self = [super init];
    if (self) {
        self.name = name;
        self.image = image;
        self.code = code;
    }
    return self;
}

+ (NSArray*)parseCountryList:(NSArray*)countryList {
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in countryList) {
        Code *code = [[Code alloc] initWithName:[dic valueForKey:@"name"] image:[dic valueForKey:@"image"] code:[dic valueForKey:@"dial_code"]];
        [array addObject:code];
    }
    return array;
}

@end
