//
//  AvatarView.m
//  Twixi
//
//  Created by macOS on 23.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "AvatarView.h"
#import <UIImageView+AFNetworking.h>
#import "UIView+ShadowToButton.h"
#import "RLMGradientValue.h"

@implementation AvatarView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customeinit];
        self.gradient = [CAGradientLayer layer];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customeinit];
        self.gradient = [CAGradientLayer layer];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customeinit];
        self.gradient = [CAGradientLayer layer];
    }
    return self;
}

- (void)customeinit {
    [[NSBundle mainBundle] loadNibNamed:@"AvatarView" owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    self.contentView.layer.cornerRadius = self.contentView.frame.size.width / 2;
    self.contentView.clipsToBounds = YES;
}

- (void)setAvatar:(NSString*)avatarPath orInitialsView:(NSString*)name {
    if ([avatarPath isEqualToString:@""]) {
        [self addGradientToBckView];
        [self.gradientView setHidden:NO];
        [self.avatarView setHidden:YES];
        [self.initialsLabel setHidden:NO];
        self.initialsLabel.text = [self textFromGradientView:name];
//        self.initialsLabel.text = [name substringToIndex:1];
    } else {
        self.avatarView.backgroundColor = UIColor.blackColor;
        [self.avatarView setHidden:NO];
        [self.gradientView setHidden:YES];
        [self.initialsLabel setHidden:YES];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:avatarPath]];
        [self.avatarView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            [self.avatarView setImage:image];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
    }
}

- (void)setAvatar:(NSString*)avatarPath orInitialsView:(NSString*)name gradientColorsArray:(NSArray*)array {
    if ([avatarPath isEqualToString:@""]) {
        [self addGradientToBckViewWithGradientArray:array];
        [self.gradientView setHidden:NO];
        [self.avatarView setHidden:YES];
        [self.initialsLabel setHidden:NO];
        self.initialsLabel.text = [self textFromGradientView:name];
//        self.initialsLabel.text = [name substringToIndex:1];
    } else {
        self.avatarView.backgroundColor = UIColor.blackColor;
        [self.avatarView setHidden:NO];
        [self.gradientView setHidden:YES];
        [self.initialsLabel setHidden:YES];
        NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:avatarPath]];
        [self.avatarView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            [self.avatarView setImage:image];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
    }
}

- (NSString*)textFromGradientView:(NSString*)name {
    NSString *initials = @"";
    if ([self wordCount:name] == 1) {
        initials = [name substringToIndex:1];
    } else if ([self wordCount:name] >= 2) {
        // Create array of words in string...
        NSArray *words = [name componentsSeparatedByString:@" "];
        NSMutableArray *firstLetters = [NSMutableArray arrayWithCapacity:[words count]];
        
        int count = 0;
        for (NSString *word in words) {
            if (count <= 2) {
                if ([word length] == 0) continue;
                [firstLetters addObject:[word substringToIndex:1]];
                count++;
            }
        }
        
        NSString *acronym = [firstLetters componentsJoinedByString:@""];
        initials = acronym;
    }
    return initials;
}


- (NSUInteger)wordCount:(NSString*)string {
    NSCharacterSet *separators = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSArray *words = [string componentsSeparatedByCharactersInSet:separators];
    
    NSIndexSet *separatorIndexes = [words indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [obj isEqualToString:@""];
    }];
    
    return [words count] - [separatorIndexes count];
}

- (void)addGradientToBckView {
    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
    
    self.gradient.frame = self.contentView.frame;
    self.gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    self.gradient.startPoint = CGPointMake(0.0, 0.5);
    self.gradient.endPoint = CGPointMake(1.0, 0.5);
    
    
    if (self.gradientView.layer.sublayers.count == 1) {
        [self.gradientView.layer insertSublayer:self.gradient atIndex:0];
    }
}

- (void)addGradientToBckViewWithGradientArray:(NSArray*)colors {
    NSMutableArray *array = [NSMutableArray new];
    for (RLMGradientValue *gr in colors) {
        [array addObject:gr.value];
    }
    
    UIColor *firstColor = [UIColor colorWithRed:[[array objectAtIndex:0] floatValue]/255.0f green:[[array objectAtIndex:1] floatValue]/255.0f blue:[[array objectAtIndex:2] floatValue]/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:[[array objectAtIndex:3] floatValue]/255.0f green:[[array objectAtIndex:4] floatValue]/255.0f blue:[[array objectAtIndex:5] floatValue]/255.0f alpha:1];
    
    self.gradient.frame = self.contentView.frame;
    self.gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    self.gradient.name = @"FriendGradient";
    
    if (self.gradientView.layer.sublayers.count == 1) {
        [self.gradientView.layer insertSublayer:self.gradient atIndex:0];
    }
    if (self.gradientView.layer.sublayers.count == 2) {
        self.gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    }
}

@end
