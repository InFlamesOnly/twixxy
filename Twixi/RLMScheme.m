//
//  RLMScheme.m
//  Twixi
//
//  Created by macOS on 08.06.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMScheme.h"
#import "RLMLanguage.h"
#import "RLMUser.h"

@implementation RLMScheme

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.schemeId = [response valueForKey:@"id"];
        self.schemeName = [response valueForKey:@"name"];
        self.sourceLang = [[RLMLanguage alloc] init];
        self.sourceLang = [self.sourceLang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:[[[response valueForKey:@"sourceLang"] valueForKey:@"data"] valueForKey:@"code"]];
        self.targetLang = [[RLMLanguage alloc] init];
        self.targetLang = [self.sourceLang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:[[[response valueForKey:@"targetLang"] valueForKey:@"data"] valueForKey:@"code"]];
    }
    return self;
}

+ (NSString *)primaryKey {
    return @"schemeName";
}
- (RLMScheme*)findSchemeFromArray:(NSArray*)array schemeCode:(NSString*)schemeName {
    RLMScheme *sc = [RLMScheme objectForPrimaryKey:schemeName];
    return sc;
}

+ (void)saveFromServerResponse:(NSDictionary*)response {
    for (NSDictionary *dic in [response valueForKey:@"data"]) {
        RLMScheme *sh = [[RLMScheme alloc] initWithServerResponse:dic];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [[RLMRealm defaultRealm] addObject:sh];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

+ (RLMScheme*)updateMyScheme {
    RLMUser *currentUser = [RLMUser currentUser];
    NSString *schemeString = [NSString stringWithFormat:@"%@-%@",currentUser.myLangCode,currentUser.translateLangCode];
    RLMScheme *scheme = [[RLMScheme alloc] init];
    scheme = [scheme findSchemeFromArray:(NSArray*)[RLMScheme allObjects] schemeCode:schemeString];
    return scheme;
}

- (NSMutableArray*)arrayFromNeedsLanguage:(NSString*)code {
    NSMutableArray *mArray = [NSMutableArray new];
    for (RLMScheme *lng in [RLMScheme allObjects]) {
        if ([code isEqualToString:lng.sourceLang.languageCode]) {
            RLMLanguage *lan = [[RLMLanguage alloc] init];
            lan = [lan findLanguageFromArray:[RLMLanguage allObjects] languageCode:lng.targetLang.languageCode];
            [mArray addObject:lan];
        }
    }
    return mArray;
}

- (NSMutableArray*)findArrayFromNeedsLanguage:(NSString*)code {
    NSMutableArray *mArray = [NSMutableArray new];
    for (RLMScheme *lng in [RLMScheme allObjects]) {
        if ([code isEqualToString:lng.sourceLang.languageCode]) {
            RLMLanguage *lan = [[RLMLanguage alloc] init];
            lan = [lan findLanguageFromArray:[RLMLanguage allObjects] languageCode:lng.targetLang.languageCode];
            [mArray addObject:lan];
        }
    }
    return mArray;
}

- (NSMutableArray*)arrayFromMyLanguage {
    NSMutableArray *mArray = [NSMutableArray new];
    NSMutableSet *set = [NSMutableSet new];
    for (RLMScheme *lng in [RLMScheme allObjects]) {
        RLMLanguage *lan = [[RLMLanguage alloc] init];
        lan = [lan findLanguageFromArray:[RLMLanguage allObjects] languageCode:lng.sourceLang.languageCode];
        [set addObject:lan];
    }
    
    for (RLMLanguage *lan in set) {
        [mArray addObject:lan];
    }
    NSLog(@"%@",set);
    return mArray;
}

- (void)replaseMyLang:(NSString*)myLang fromTrnslLang:(NSString*)translateLang {
    NSString *full = [NSString stringWithFormat:@"%@-%@",translateLang,myLang];
    RLMScheme *sch = [[RLMScheme alloc] init];
    RLMUser *user = [RLMUser currentUser];
    sch = [sch findSchemeFromArray:[RLMScheme allObjects] schemeCode:full];
    if (sch != nil) {
        RLMLanguage *lng = [[RLMLanguage alloc] init];
        lng = sch.targetLang;
        [[RLMRealm defaultRealm] beginWriteTransaction];
        user.myLangCode = sch.targetLang.languageCode;
        user.translateLangCode = sch.sourceLang.languageCode;
        [[RLMRealm defaultRealm] commitWriteTransaction];
    } else {
        NSArray *array = [self arrayFromNeedsLanguage:myLang];
        RLMLanguage *lng = [[RLMLanguage alloc] init];
        lng = array[0];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        user.translateLangCode = lng.languageCode;
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

- (RLMLanguage*)getFromMyCode:(NSString*)myLang fromTrnslLang:(NSString*)translateLang {
    NSString *full = [NSString stringWithFormat:@"%@-%@",myLang,translateLang];
    RLMScheme *sch = [[RLMScheme alloc] init];
    RLMUser *user = [RLMUser currentUser];
    sch = [sch findSchemeFromArray:[RLMScheme allObjects] schemeCode:full];
    if (sch != nil) {
        RLMLanguage *lng = [[RLMLanguage alloc] init];
        lng = sch.targetLang;
        return lng;
    } else {
        NSArray *array = [self arrayFromNeedsLanguage:myLang];
        RLMLanguage *lng = [[RLMLanguage alloc] init];
        lng = array[0];
        return lng;
    }
}


@end
