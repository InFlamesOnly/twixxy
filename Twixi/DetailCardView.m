//
//  DetailCardView.m
//  Twixi
//
//  Created by macOS on 06.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "DetailCardView.h"
#import "RLMTranslationCart.h"
#import "RLMUser.h"

@implementation DetailCardView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.synthesizer = [[AVSpeechSynthesizer alloc]init];
        self.synthesizer.delegate = self;
        self.isFliped = NO;
        self.countTranslate = 1;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.synthesizer = [[AVSpeechSynthesizer alloc]init];
        self.isFliped = NO;
        self.countTranslate = 1;
        self.synthesizer.delegate = self;
        self.beginTranslate = NO;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.synthesizer = [[AVSpeechSynthesizer alloc]init];
        self.synthesizer.delegate = self;
        self.isFliped = NO;
        self.countTranslate = 1;
        self.beginTranslate = NO;
    }
    return self;
}

- (void)setTextFromCart {
    self.errorTranlsateCountLabel.text = NSLocalizedString(@"No translations available", nil);
//    [self.pretextLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Pretext", nil)]];
//    [self.verbLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Verb", nil)]];
//    [self.adverbLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Adverb", nil)]];
//    [self.adjectiveLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Adjective", nil)]];
    [self.showMoreButton setTitle:NSLocalizedString(@"Show example", nil) forState:UIControlStateNormal];
    
//    [self.trnsPretextLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Pretext", nil)]];
//    [self.trnsVerbLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Verb", nil)]];
//    [self.trnsAdverbLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Adverb", nil)]];
//    [self.trnsAdjectiveLabel setText:[NSString stringWithFormat:@"%@:",NSLocalizedString(@"Adjective", nil)]];
    [self.translateButton setTitle:NSLocalizedString(@"Translate", nil) forState:UIControlStateNormal];
    [self.showMoreTranslateButton setTitle:NSLocalizedString(@"Another example", nil) forState:UIControlStateNormal];
}
- (void)addSpeach {

}

- (void)startSpeech {
    [self.sound setSelected:YES];
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc]initWithString:self.word.text];
    NSString *voicesCode = [self findLanguge];
    AVSpeechSynthesisVoice *voice = [AVSpeechSynthesisVoice voiceWithLanguage:voicesCode];
    utterance.voice = voice;
    [self.synthesizer speakUtterance:utterance];
    
}

- (NSString*)findLanguge {
    //https://stackoverflow.com/questions/35492386/how-to-get-a-list-of-all-voices-on-ios-9
    RLMUser *currentUser = [RLMUser currentUser];
    NSString *trn = currentUser.translateLangCode;
    NSArray *voices = [AVSpeechSynthesisVoice speechVoices];
    NSString *voicesCode = @"";
    for (AVSpeechSynthesisVoice *voice in voices) {
        if ([voice.language rangeOfString:trn].location == NSNotFound) {
            voicesCode = @"en-US";
        } else {
            voicesCode = voice.language;
            break;
        }
    }
    return voicesCode;
}

- (IBAction)sound:(id)sender {
    [self startSpeech];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance {
    
}
- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    [self.sound setSelected:NO];
}

-(IBAction)flipViews:(id)sender {
    NSLog(@"FLIP");
    [UIView transitionWithView:self.flipedView duration:0.65f
                       options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                           if (self.isFliped == NO) {
                               self.bcgView.hidden = YES;
                               self.translateView.hidden = NO;
                               self.isFliped = YES;
                               //flip to translate
                           } else {
                               self.bcgView.hidden = NO;
                               self.translateView.hidden = YES;
                               self.isFliped = NO;
                               //flip to original
                           }
                           
                       } completion:^(BOOL finished) {
                           
                       }];
}

-(IBAction)translateValues:(id)sender {
    self.beginTranslate = !self.beginTranslate;
    
    if (self.beginTranslate == YES) {
        self.translateViewWord.text = self.translate.text;
        self.translateViewTranslate.text = self.word.text;
        [self setAllTranslate];
    } else {
        self.translateViewWord.text = self.word.text;
        self.translateViewTranslate.text = self.translate.text;
        [self setAllOriginal];
    }
}


-(IBAction)moreTranslate:(id)sender {
    if (self.countTranslate < self.tranlsatersObjects.count) {
        if (self.countTranslate == 1) {
            UIView *view = self.translateViews[3];
            UILabel *label = self.tranlsaters[3];
            [view setHidden:NO];
            [label setHidden:NO];
        }
        if (self.countTranslate == 2) {
            UIView *view = self.translateViews[0];
            UILabel *label = self.tranlsaters[0];
            [view setHidden:NO];
            [label setHidden:NO];
        }
        if (self.countTranslate == 3) {
            UIView *view = self.translateViews[2];
            UILabel *label = self.tranlsaters[2];
            [view setHidden:NO];
            [label setHidden:NO];
        }
        self.countTranslate++;
    }
}

- (void)hideTranslate {
    UIView *secondView = self.translateViews[0];
    UIView *thirth = self.translateViews[2];
    UIView *four = self.translateViews[3];
    UILabel *secondTranslate = self.tranlsaters[0];
    UILabel *thirthTranslate = self.tranlsaters[2];
    UILabel *fourTranslate = self.tranlsaters[3];
    [secondView setHidden:YES];
    [thirth setHidden:YES];
    [four setHidden:YES];
    [secondTranslate setHidden:YES];
    [thirthTranslate setHidden:YES];
    [fourTranslate setHidden:YES];
}

- (void)customize {
    self.bcgView.layer.cornerRadius = 15;
    self.bcgView.layer.masksToBounds = YES;
    
    self.translateView.layer.cornerRadius = 15;
    self.translateView.layer.masksToBounds = YES;
    
    self.showMoreButton.layer.cornerRadius = 8;
    self.showMoreButton.layer.masksToBounds = YES;
    self.showMoreButton.layer.borderWidth = 1;
    self.showMoreButton.layer.borderColor = [UIColor colorWithRed:50.0f/255.0f green:67.0f/255.0f blue:179.0f/255.0f alpha:0.2].CGColor;
    
    self.showMoreTranslateButton.layer.cornerRadius = 8;
    self.showMoreTranslateButton.layer.masksToBounds = YES;
    self.showMoreTranslateButton.layer.borderWidth = 1;
    self.showMoreTranslateButton.layer.borderColor = [UIColor colorWithRed:50.0f/255.0f green:67.0f/255.0f blue:179.0f/255.0f alpha:0.2].CGColor;
    
    self.translateButton.layer.cornerRadius = 8;
    self.translateButton.layer.masksToBounds = YES;
    self.translateButton.layer.borderWidth = 1;
    self.translateButton.layer.borderColor = [UIColor colorWithRed:50.0f/255.0f green:67.0f/255.0f blue:179.0f/255.0f alpha:0.2].CGColor;
    
    for (UIView *view in self.bcgViews) {
        view.layer.cornerRadius = 8;
        view.layer.masksToBounds = YES;
        view.layer.borderWidth = 1;
        view.layer.borderColor = [UIColor colorWithRed:227.0f/255.0f green:234.0f/255.0f blue:244.0f/255.0f alpha:1].CGColor;
    }
    
    for (UIView *view in self.translateViews) {
        view.layer.cornerRadius = 8;
        view.layer.masksToBounds = YES;
        view.layer.borderWidth = 1;
        view.layer.borderColor = [UIColor colorWithRed:227.0f/255.0f green:234.0f/255.0f blue:244.0f/255.0f alpha:1].CGColor;
    }
    [self hideTranslate];
    [self setTextFromCart];
}

+ (NSArray*)testCardViewsfromView:(UIView*)view {
    DetailCardView *rootView = [[[NSBundle mainBundle] loadNibNamed:@"DetailCardView" owner:self options:nil] objectAtIndex:0];
    rootView.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [rootView customize];
    DetailCardView *rootView1 = [[[NSBundle mainBundle] loadNibNamed:@"DetailCardView" owner:self options:nil] objectAtIndex:0];
    rootView1.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [rootView1 customize];
    DetailCardView *rootView2 = [[[NSBundle mainBundle] loadNibNamed:@"DetailCardView" owner:self options:nil] objectAtIndex:0];
    rootView2.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [rootView2 customize];
    return [[NSArray alloc] initWithObjects:rootView, rootView1, rootView2 ,nil];
}

+ (NSArray*)viewsWithContent:(NSArray*)contentArray fromView:(UIView*)view {
    NSMutableArray *contents = [NSMutableArray new];
    for (RLMTranslationCart *cart in contentArray) {
        DetailCardView *dictCart = [[[NSBundle mainBundle] loadNibNamed:@"DetailCardView" owner:self options:nil] objectAtIndex:0];
        dictCart.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
        [dictCart customize];
        [dictCart map:dictCart fromObjects:cart];
        [contents addObject:dictCart];
    }
    return (NSArray*)contents;
}

- (void)map:(DetailCardView*)detCart fromObjects:(RLMTranslationCart*)cart {
    self.word.text = cart.word;
    self.translate.text = cart.translations;
    self.pretextLabel.text = cart.pretext;
    self.verbLabel.text = cart.verb;
    self.adjectiveLabel.text = cart.adjective;
    self.adverbLabel.text = cart.naming;
    NSMutableArray *array = [NSMutableArray new];
    for (RLMExamples *ex in cart.examples) {
        [array addObject:ex];
    }
    self.tranlsatersObjects = (NSMutableArray*)array;
//    self.tranlsatersObjects = [self testEx];
    self.translateViewWord.text = cart.word;
    self.translateViewTranslate.text = cart.translations;
    
    [self setTranlsationsValuesFromArray:self.tranlsatersObjects];
    
    if (self.tranlsatersObjects.count == 0) {
        UIView *view = self.translateViews[1];
        UILabel *label = self.tranlsaters[1];
        [view setHidden:YES];
        [label setHidden:YES];
        [self.showMoreTranslateButton setHidden:YES];
        [self.errorTranlsateCountLabel setHidden:NO];
    } else {
        [self.errorTranlsateCountLabel setHidden:YES];
    }
}

- (NSArray*)testEx {
    RLMExamples *ex1 = [[RLMExamples alloc] init];
    ex1.original = @"test1";
    ex1.translate = @"тест1";
    RLMExamples *ex2 = [[RLMExamples alloc] init];
    ex2.original = @"test2";
    ex2.translate = @"тест2";
    RLMExamples *ex3 = [[RLMExamples alloc] init];
    ex3.original = @"test3";
    ex3.translate = @"тест3";
    RLMExamples *ex4 = [[RLMExamples alloc] init];
    ex4.original = @"test4";
    ex4.translate = @"тест4";
    return @[ex1,ex2,ex3,ex4];
}

- (void)setTranlsationsValuesFromArray:(NSArray*)array {
    if (array.count == 1) {
        RLMExamples *ex = array[0];
        self.trnsPretextLabel.text = ex.original;
    } else if (array.count == 2) {
        RLMExamples *ex = array[0];
        self.trnsPretextLabel.text = ex.original;
        RLMExamples *ex2 = array[1];
        self.trnsVerbLabel.text = ex2.original;
    } else if (array.count == 3) {
        RLMExamples *ex = array[0];
        self.trnsPretextLabel.text = ex.original;
        RLMExamples *ex2 = array[1];
        self.trnsVerbLabel.text = ex2.original;
        RLMExamples *ex3 = array[2];
        self.trnsAdverbLabel.text = ex3.original;
    } else if (array.count == 4) {
        RLMExamples *ex = array[0];
        self.trnsPretextLabel.text = ex.original;
        RLMExamples *ex2 = array[1];
        self.trnsVerbLabel.text = ex2.original;
        RLMExamples *ex3 = array[2];
        self.trnsAdverbLabel.text = ex3.original;
        RLMExamples *ex4 = array[3];
        self.trnsAdjectiveLabel.text = ex4.original;
    }
}

- (void)setAllTranslate {
    if (self.tranlsatersObjects.count == 1) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.translate;
    } else if (self.tranlsatersObjects.count == 2) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.translate;
        RLMExamples *ex2 = self.tranlsatersObjects[1];
        self.trnsVerbLabel.text = ex2.translate;
    } else if (self.tranlsatersObjects.count == 3) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.translate;
        RLMExamples *ex2 = self.tranlsatersObjects[1];
        self.trnsVerbLabel.text = ex2.translate;
        RLMExamples *ex3 = self.tranlsatersObjects[2];
        self.trnsAdverbLabel.text = ex3.translate;
    } else if (self.tranlsatersObjects.count == 4) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.translate;
        RLMExamples *ex2 = self.tranlsatersObjects[1];
        self.trnsVerbLabel.text = ex2.translate;
        RLMExamples *ex3 = self.tranlsatersObjects[2];
        self.trnsAdverbLabel.text = ex3.translate;
        RLMExamples *ex4 = self.tranlsatersObjects[3];
        self.trnsAdjectiveLabel.text = ex4.translate;
    }
}

- (void)setAllOriginal {
    if (self.tranlsatersObjects.count == 1) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.original;
    } else if (self.tranlsatersObjects.count == 2) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.original;
        RLMExamples *ex2 = self.tranlsatersObjects[1];
        self.trnsVerbLabel.text = ex2.original;
    } else if (self.tranlsatersObjects.count == 3) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.original;
        RLMExamples *ex2 = self.tranlsatersObjects[1];
        self.trnsVerbLabel.text = ex2.original;
        RLMExamples *ex3 = self.tranlsatersObjects[2];
        self.trnsAdverbLabel.text = ex3.original;
    } else if (self.tranlsatersObjects.count == 4) {
        RLMExamples *ex = self.tranlsatersObjects[0];
        self.trnsPretextLabel.text = ex.original;
        RLMExamples *ex2 = self.tranlsatersObjects[1];
        self.trnsVerbLabel.text = ex2.original;
        RLMExamples *ex3 = self.tranlsatersObjects[2];
        self.trnsAdverbLabel.text = ex3.original;
        RLMExamples *ex4 = self.tranlsatersObjects[3];
        self.trnsAdjectiveLabel.text = ex4.original;
    }
}

@end
