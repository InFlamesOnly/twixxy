//
//  SearchBarWithCustomeFont.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "SearchBarWithCustomeFont.h"

@implementation SearchBarWithCustomeFont

- (instancetype)init {
    self = [super init];
    if (self) {
        [self changePlaseholderFont];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self changePlaseholderFont];
    }
    return self;
}

-(void)changePlaseholderFont {
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:@{
                                                                        NSFontAttributeName: [UIFont fontWithName:@"MaisonNeue-Book" size:14],}];
}

@end
