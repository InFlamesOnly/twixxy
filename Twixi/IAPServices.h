//
//  IAPServices.h
//  Twixi
//
//  Created by macOS on 01.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol SubscribeViewDelegate <NSObject>

- (void) closeView;

@end

NS_ASSUME_NONNULL_BEGIN

@interface IAPServices : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property(nonatomic,weak) id <SubscribeViewDelegate> delegate;

+ (instancetype)sharedInstance;
- (void)getProducts:(NSString*)code;
- (void) restore;

@end

NS_ASSUME_NONNULL_END
