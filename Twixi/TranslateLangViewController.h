//
//  TranslateLangViewController.h
//  Twixi
//
//  Created by macOS on 09.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMLanguage.h"
#import "GradientViewController.h"

@interface TranslateLangViewController : GradientViewController

@property (strong, nonatomic) RLMLanguage *lang;

@end
