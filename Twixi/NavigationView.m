//
//  NavigationView.m
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NavigationView.h"

@implementation NavigationView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addGradientToView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}

-(void)layoutSubviews {
    [self addGradientToView];
}

- (void)addGradientToView {
    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
