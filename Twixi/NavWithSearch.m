//
//  NavWithSearch.m
//  Twixi
//
//  Created by macOS on 03.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NavWithSearch.h"
#import "IOSDeviceName.h"

@implementation NavWithSearch

- (CGFloat)constant {
    IOSDeviceName *name = [IOSDeviceName new];
    if ([name.deviceName isEqualToString:@"iPhone X"] || [name.deviceName isEqualToString:@"Simulator"]) {
        return  131;
    } else {
        return 111;
    }
}

@end
