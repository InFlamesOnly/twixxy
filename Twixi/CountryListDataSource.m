//
//  CountryListDataSource.m
//  Country List
//
//  Created by Pradyumna Doddala on 18/12/13.
//  Copyright (c) 2013 Pradyumna Doddala. All rights reserved.
//

#import "CountryListDataSource.h"

@import CoreTelephony;

#define kCountriesFileName @"countries.json"

@interface CountryListDataSource () {
    NSArray *countriesList;
}

@end

@implementation CountryListDataSource

- (id)init {
    self = [super init];
    if (self) {
        [self parseJSON];
    }
    
    return self;
}

- (void)parseJSON {
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES];
    NSArray *sortedArray = [(NSArray *)parsedObject sortedArrayUsingDescriptors:@[sortDescriptor]];
    countriesList = sortedArray;
}

- (NSArray *)countries {
    return countriesList;
}

- (NSDictionary*)getCurrentLocationCode {
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    if (!carrier.isoCountryCode) {
        NSLog(@"No sim present Or No cellular coverage or phone is on airplane mode.");
        return nil;
    } else {
        NSLog(@"%@",carrier.isoCountryCode);
        return [self getMyCode:carrier.isoCountryCode];
    }
}

- (NSDictionary *)getMyCode:(NSString *)code {
    NSArray *array = [NSArray new];
    array = [self countries];
    
    for (NSDictionary *country in array) {
        if ([[country valueForKey:@"code"] isEqualToString:[code uppercaseString]]) {
            return country;
        }
    }
    return nil;
}

@end
