//
//  AvatarView.h
//  Twixi
//
//  Created by macOS on 23.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AvatarView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *gradientView;
@property (weak, nonatomic) IBOutlet UILabel *initialsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (strong, nonatomic) CAGradientLayer *gradient;

- (void)setAvatar:(NSString*)avatarPath orInitialsView:(NSString*)name gradientColorsArray:(NSArray*)array;
- (void)setAvatar:(NSString*)avatarPath orInitialsView:(NSString*)name;

@end

NS_ASSUME_NONNULL_END
