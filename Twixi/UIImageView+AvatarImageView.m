//
//  UIImageView+AvatarImageView.m
//  Twixi
//
//  Created by macOS on 04.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "UIImageView+AvatarImageView.h"
#import "RLMGradientValue.h"
@implementation UIImageView (AvatarImageView)

+ (void)addGradientToImageView:(UIImageView*)imageView withText:(NSString*)text{
    
    NSInteger aRedValue = arc4random()%255;
    NSInteger aGreenValue = arc4random()%255;
    NSInteger aBlueValue = arc4random()%255;
    
    UIColor *firstColor = [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1];
    imageView.backgroundColor = firstColor;
//    NSString *mystr = [text substringToIndex:1];
//    [self addLabelToImageView:imageView withText:mystr];
    
}

+ (void)addLabelToImageView:(UIImageView*)imageView withText:(NSString*)text{
    UILabel *imageLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.frame.size.height/2,imageView.frame.size.height/2,0,0)];
    
    imageLabel.center = imageView.center;
    [imageLabel setFont:[UIFont systemFontOfSize:20]];
    imageLabel.text = text;
    [imageView addSubview:imageLabel];
}

+ (void)addGradientToView:(UIView*)view
       withGradientColors:(NSArray*)colors {
    
    NSMutableArray *array = [NSMutableArray new];
    for (RLMGradientValue *gr in colors) {
        [array addObject:gr.value];
    }
    
    //    [colorsOne objectAtIndex:0];
    
    UIColor *firstColor = [UIColor colorWithRed:[[array objectAtIndex:0] floatValue]/255.0f green:[[array objectAtIndex:1] floatValue]/255.0f blue:[[array objectAtIndex:2] floatValue]/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:[[array objectAtIndex:3] floatValue]/255.0f green:[[array objectAtIndex:4] floatValue]/255.0f blue:[[array objectAtIndex:5] floatValue]/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    gradient.name = @"FriendGradient";
    //    gradient.startPoint = CGPointMake(0.0, 0.5);
    //    gradient.endPoint = CGPointMake(1.0, 0.5);
    [view.layer addSublayer:gradient];
//    [view.layer insertSublayer:gradient atIndex:0];
}

@end
