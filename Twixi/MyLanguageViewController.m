//
//  MyLanguageViewController.m
//  Twixi
//
//  Created by macOS on 09.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "MyLanguageViewController.h"
#import "RequestManager.h"
#import "LanguageViewController.h"
#import "TranslateLangViewController.h"
#import "RLMScheme.h"

@interface MyLanguageViewController () <LanguageViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *codeNameButton;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;

@property (strong, nonatomic) RLMLanguage *lang;
@property (strong, nonatomic) RLMUser *currentUser;

@end

@implementation MyLanguageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentUser = [RLMUser currentUser];
    [self getLangFromScheme];
    [self setLangInfoToUI:self.lang];
    [self saveFromRealm];
}

- (void)getLangFromScheme {
    RLMScheme *sc = [[RLMScheme alloc] init];
    self.lang = [sc arrayFromMyLanguage][0];
    self.lang = [RLMLanguage findMyLangFromArray:[sc arrayFromMyLanguage]];
}

- (void)setLangInfoToUI:(RLMLanguage*)lang {
    [self.codeNameButton setTitle:lang.languageName forState:UIControlStateNormal];
    [self.flagImageView setImage:[UIImage imageNamed:lang.languageCode]];
}

- (void)saveFromRealm {
    [RLMUser saveMyLangCode:self.lang.languageCode];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"language"]) {
        LanguageViewController *vc = [segue destinationViewController];
        vc.delegate = self;
        vc.isFirstMyLanguage = YES;
    }
    if ([segue.identifier isEqualToString:@"next"]) {
        TranslateLangViewController *vc = [segue destinationViewController];
        vc.lang = self.lang;
    }
}

-(IBAction)nextScreen:(id)sender {
    if (self.lang == nil) {
        self.lang = [RLMLanguage english];
    }
    [self saveFromRealm];
    [self performSegueWithIdentifier:@"next" sender:self];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)goToLanguageVC:(id)sender {
    [self performSegueWithIdentifier:@"language" sender:self];
}

- (void)getLanguage:(RLMLanguage *)language {
    self.lang = language;
    [self setLangInfoToUI:self.lang];
}

@end
