//
//  RLMFriend.h
//  Twixi
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMGradientValue.h"

@interface RLMFriend : RLMObject

@property NSString *friendPrimaryKay;
@property NSString *addDate;
@property NSNumber <RLMInt> *online;
@property NSNumber <RLMInt> *friendId;

@property NSNumber <RLMInt> *userIsBlocked;

@property NSNumber <RLMInt> *friendFromMyList;

@property NSString *name;
@property NSString *lastName;
@property NSString *nickName;
@property NSString *phone;
//TODO avatar image Realm
@property NSString *avatar;

@property NSString *updateAt;
@property NSString *lastOnline;
@property (nonatomic, strong, readwrite) RLMArray<RLMGradientValue *>< RLMGradientValue> *gradient;

- (id)initWithServerResponse:(NSDictionary*)response;
- (RLMFriend*)findFriendFromArray:(NSArray*)array chatId:(NSNumber*)friendId;

+ (void)saveFromServerResponse:(NSDictionary*)response;

@end
