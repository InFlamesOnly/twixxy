//
//  RLMSubscribe.m
//  Twixi
//
//  Created by macOS on 04.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMSubscribe.h"

@implementation RLMSubscribe

+ (void)saveFromServerResponse:(NSDictionary*)response {
    RLMRealm *realm = [RLMRealm defaultRealm];
    if ([RLMSubscribe allObjects].count > 0) {
        [realm beginWriteTransaction];
        [realm deleteObjects:[RLMSubscribe allObjects]];
        [realm commitWriteTransaction];
    }
    for (NSDictionary *dic in [response valueForKey:@"data"]) {
        RLMSubscribe *sub = [[RLMSubscribe alloc] init];
        sub.value = [dic valueForKey:@"id"];
        sub.price = [[dic valueForKey:@"price"] floatValue];
        [realm beginWriteTransaction];
        [realm addObject:sub];
        [realm commitWriteTransaction];
    }
}

@end
