//
//  RLMGradientValue.h
//  Twixi
//
//  Created by macOS on 09.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMGradientValue : RLMObject

@property NSNumber <RLMInt> *value;

- (id)initWithValue:(NSNumber*)value;

@end



RLM_ARRAY_TYPE(RLMGradientValue)
