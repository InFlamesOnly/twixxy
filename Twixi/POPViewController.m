//
//  POPViewController.m
//  Twixi
//
//  Created by macOS on 22.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import "POPViewController.h"
#import "POPCell.h"

@interface POPViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;

@end

@implementation POPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationLabel.text = NSLocalizedString(@"Privacy policy", nil);
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"TermsOfService" sender:self];
    }
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"SendQuestion" sender:self];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    POPCell *cell = [tableView dequeueReusableCellWithIdentifier:@"POPCell"];
    switch (indexPath.row) {
        case 0:
            cell.value.text = NSLocalizedString(@"Terms of Service", nil);
            break;
        case 1:
            cell.value.text = NSLocalizedString(@"Send question", nil);
            break;

//        case 1:
//            cell.value.text = NSLocalizedString(@"Data Policy", nil);
//            break;
//        case 2:
//            cell.value.text = NSLocalizedString(@"Cookie Policy", nil);
//            break;
//        case 3:
//            cell.value.text = NSLocalizedString(@"Third-party notices", nil);
//            break;
//        default:
//            break;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
