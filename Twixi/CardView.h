//
//  CardView.h
//  Twixi
//
//  Created by macOS on 06.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//



#import <UIKit/UIKit.h>

@protocol CardViewDelegate

- (void)tapToStart;

@end

@interface CardView : UIView

@property (nonatomic,retain) id <CardViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *bcgVew;
@property (weak, nonatomic) IBOutlet UIView *wordView;
@property (weak, nonatomic) IBOutlet UIView *repeatView;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (weak, nonatomic) IBOutlet UILabel *lvl;
@property (weak, nonatomic) IBOutlet UILabel *words;
@property (weak, nonatomic) IBOutlet UILabel *repeatsWords;
@property (weak, nonatomic) IBOutlet UILabel *todayPlan;


- (void)customize ;
- (void)setTextFromCart;

+ (NSArray*)testCardViewsfromView:(UIView*)view;

@end
