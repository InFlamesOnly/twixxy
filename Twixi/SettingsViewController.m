//
//  SettingsViewController.m
//  Twixi
//
//  Created by macOS on 15.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "SettingsViewController.h"
#import "SettingCell.h"
#import "RequestManager.h"
#import "RLMUser.h"
#import <UIImageView+AFNetworking.h>
#import "PCAngularActivityIndicatorView.h"
#import "RLMSubscribe.h"
#import "RLMLanguage.h"
#import "LanguageViewController.h"
#import "RLMScheme.h"
#import "AvatarView.h"
#import "NicknameViewController.h"
#import "PhotosController.h"
#import "UIAlertController+Blocks.h"

@interface SettingsViewController () <UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, PhotosControllerrDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *initials;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *avatar2;
@property (strong, nonatomic) UIImage *preloaderAvatar;
@property (weak, nonatomic) IBOutlet UITableView *settingTableView;
@property (weak, nonatomic) IBOutlet UIButton *acceptButton;

@property (strong, nonatomic) RLMUser *user;

@property (strong, nonatomic) NSString *myLang;
@property (strong, nonatomic) NSString *trnLang;
@property (strong, nonatomic) UIActivityIndicatorView *photoActivity;
@property (strong, nonatomic) PhotosController *photos;
@property (strong, nonatomic) NSString *avatarPath;

@property BOOL isMine;

@end


@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configStartView];
}

- (void)configStartView {
    self.user = [RLMUser currentUser];
    self.nickName.text = [NSString stringWithFormat:@"@%@",self.user.nickName];
    self.name.text = self.user.name;
    self.myLang = self.user.myLangCode;
    self.trnLang = self.user.translateLangCode;
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    [self.acceptButton setHidden:YES];
    [self.settingTableView reloadData];
    
    [self createPhotoActivity];
    
    [self notificationStateAdd];
}

- (void)notificationStateAdd {
    if (self.user.notification == nil) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        self.user.notification = @(1);
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.settingTableView]) {
        SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [cell.nameTextField resignFirstResponder];
        [cell.lastNameTextField resignFirstResponder];
        return YES;
    }

    return YES;
}

- (void)createPhotoActivity {
    SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    self.photoActivity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]; [self.view addSubview:self.photoActivity];
    [cell addSubview:self.photoActivity];
    [self.photoActivity setHidden:YES];
    self.photoActivity.center = cell.avatarView.center;
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.settingTableView reloadData];
    [self changedValues:self.user.name lastName:self.user.lastName myLang:self.myLang trnsLang:self.trnLang notificationValue:self.user.notification];
}

- (IBAction)switchValueChanged {
    NSIndexPath *path = [NSIndexPath indexPathForRow:4 inSection:0];
    SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:path];
    NSNumber *switchValue = [NSNumber new];
    if (cell.notification.isOn) {
        switchValue = @(1);
    } else {
        switchValue = @(0);
    }
    [self changedValues:self.user.name lastName:self.user.lastName myLang:self.user.myLangCode trnsLang:self.user.translateLangCode notificationValue:switchValue];
}

//-(BOOL) validateAlphabets: (NSString *)alpha
//{
//    NSString *abnRegex = @"[A-Za-z]+"; // check for one or more occurrence of string you can also use * instead + for ignoring null value
//    NSPredicate *abnTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", abnRegex];
//    BOOL isValid = [abnTest evaluateWithObject:alpha];
//    return isValid;
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSString * actualString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if(textField == cell.nameTextField || textField == cell.lastNameTextField) {
        if([string isEqualToString:@" "]){
            // Returning no here to restrict whitespace
            return NO;
        }
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet letterCharacterSet] invertedSet]].location != NSNotFound) {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[NSCharacterSet letterCharacterSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        if(textField == cell.nameTextField){
            [self changedValues:actualString lastName:self.user.lastName myLang:self.user.myLangCode trnsLang:self.user.translateLangCode notificationValue:self.user.notification];
        }
        if(textField == cell.lastNameTextField){
            [self changedValues:self.user.name lastName:actualString myLang:self.user.myLangCode trnsLang:self.user.translateLangCode notificationValue:self.user.notification];
        }
        
        return [string isEqualToString:filtered];
    }
    return YES;
}

- (void)changedValues:(NSString*)name lastName:(NSString*)lastName myLang:(NSString*)myLang trnsLang:(NSString*)trnsLang notificationValue:(NSNumber*)notificationValue {
    if ([name isEqualToString:self.user.name] &&
        [lastName isEqualToString:self.user.lastName] &&
        [myLang isEqualToString:self.user.myLangCode] &&
        [trnsLang isEqualToString:self.user.translateLangCode] &&
        [notificationValue isEqualToNumber:self.user.notification]) {
        [self.acceptButton setHidden:YES];
    } else {
        [self.acceptButton setHidden:NO];
    }
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"languageChange"]) {
        LanguageViewController *vc = [segue destinationViewController];
        vc.isMineLanguage = self.isMine;
        vc.isTnsLanguage  = !self.isMine;
    }
    if ([segue.identifier isEqualToString:@"nickName"]) {
        NicknameViewController *vc = segue.destinationViewController;
        vc.nickName = self.user.nickName;
    }
}

-(IBAction)accept:(id)sender {
    if ([self checkValidateErrors]) {
        [self startActivityFromTabBar];
        NSNumber *switchValue = [self switchValue];
        RLMScheme *scheme = [self checkScheme];
        if (scheme != nil) {
            [self updateLanguageFromSwitchValue:switchValue withSuccessBlock:^(NSDictionary *success) {
                [self updateScheme:scheme];
            }];
        }
    }
}

- (NSNumber*)switchValue {
    SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
    if (cell.notification.isOn) {
        return @(1);
    } else {
        return @(0);
    }
}

- (BOOL)checkValidateErrors {
    SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.nameTextField resignFirstResponder];
    [cell.lastNameTextField resignFirstResponder];
    
    if (cell.nameTextField.text.length < 5) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The minimum length of the name is 5 characters.", nil)];
        return NO;
    } else if (cell.nameTextField.text.length > 32) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The maximum length of the name is 32 characters.", nil)];
        return NO;
    } else if (cell.lastNameTextField.text.length < 5) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The minimum length of the last name is 5 characters.", nil)];
        return NO;
    } else if (cell.lastNameTextField.text.length > 32) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The maximum length of the last name is 32 characters.", nil)];
        return NO;
    } else {
        return YES;
    }
}

- (RLMScheme*)checkScheme {
    NSString *schemeString = [NSString stringWithFormat:@"%@-%@",self.user.myLangCode,self.user.translateLangCode];
    RLMScheme *scheme = [[RLMScheme alloc] init];
    scheme = [scheme findSchemeFromArray:(NSArray*)[RLMScheme allObjects] schemeCode:schemeString];
    return scheme;
}

- (void)updateLanguageFromSwitchValue:(NSNumber*)switchValue withSuccessBlock:(void(^)(NSDictionary*success)) successBlock {
    [[RequestManager sharedManager] updateSettingsLanguage:self.user.translateLangCode andNotification:self.user.notification withSuccessBlock:^(NSDictionary *success) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        self.user.notification = switchValue;
        [[RLMRealm defaultRealm] commitWriteTransaction];
        successBlock(success);
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)updateScheme:(RLMScheme*)scheme {
    NSString *schemeString = [NSString stringWithFormat:@"%@-%@",self.user.myLangCode,self.user.translateLangCode];
    RLMScheme *schemeObj = [[RLMScheme alloc] init];
    schemeObj = [schemeObj findSchemeFromArray:(NSArray*)[RLMScheme allObjects] schemeCode:schemeString];
    [[RequestManager sharedManager] updateScheme:scheme.schemeId withSuccessBlock:^(NSDictionary *success) {
        [self updateUserInfo];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)updateUserInfo {
    [[RequestManager sharedManager] updateUserName:self.user.name lastName:self.user.lastName nickName:self.user.nickName avatar:self.user.avatar withSuccessBlock:^(NSDictionary *success) {
        SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [RLMUser updateName:cell.nameTextField.text andLastname:cell.lastNameTextField.text];
        [self removeActivity];
        [self.navigationController popViewControllerAnimated:YES];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)sendPhoto:(NSString*)photoPath {
    [self sendPhotoFromServer:photoPath];
}

- (IBAction)changeAvatar:(id)sender {
    self.photos = [[PhotosController alloc] init];
    self.photos.delegate = self;
    [self.photos showImagePicker];
}

- (void)sendPhotoFromServer:(NSString*)photoPath {
    [self.photoActivity startAnimating];
    [self.photoActivity setHidden:NO];
    [[RequestManager sharedManager] uploadImageFromBundlePath:photoPath withSuccessBlock:^(NSDictionary *success) {
        NSString *avatarPath = [[success valueForKey:@"data"] valueForKey:@"url"];
        [self setPhotoFromIcon:avatarPath];
        [self.photoActivity stopAnimating];
        [self.photoActivity setHidden:YES];
    } onFalureBlock:^(NSInteger statusCode) {
        [self.photoActivity stopAnimating];
        [self.photoActivity setHidden:YES];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Server error"];
    }];
}

- (void)setPhotoFromIcon:(NSString*)avatarPath {
    [RLMUser updateAvatar:avatarPath];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[RLMUser currentUser].avatar]];
    SettingCell *cell = [self.settingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.avatarView.avatarView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        [cell.avatarView.avatarView setImage:image];
        [self.settingTableView reloadData];
        [self.photoActivity stopAnimating];
        [self.photoActivity setHidden:YES];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        [self.photoActivity stopAnimating];
        [self.photoActivity setHidden:YES];
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self configHeightFromIndexPAth:indexPath];
}

- (CGFloat)configHeightFromIndexPAth:(NSIndexPath*)indexPath {
    if (indexPath.row == 0) {
        return 80;
    }
    if (indexPath.row == 1) {
        return 55;
    }
    if (indexPath.row == 2) {
        return 80;
    }
    if (indexPath.row == 3) {
        return 13;
    }
    if (indexPath.row == 4) {
        return 77;
    }
    if (indexPath.row == 5) {
        return 13;
    }
    if (indexPath.row == 6) {
        return 67;
    }
    if (indexPath.row == 7) {
        return 67;
    }
    if (indexPath.row == 8) {
        return 34;
    }
    if (indexPath.row == 9) {
        return 48;
    }
    if (indexPath.row == 10) {
        return 48;
    }
    if (indexPath.row == 11) {
        return 13;
    }
    if (indexPath.row == 12) {
        return 48;
    }
    if (indexPath.row == 13) {
        return 13;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 14;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self configureCellFromIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self configureTapToCellAtIndexPath:indexPath];
}

- (void)configureTapToCellAtIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.row == 2) {
//        [self performSegueWithIdentifier:@"nickName" sender:self];
    }
    if (indexPath.row == 6) {
        self.isMine = YES;
        [self performSegueWithIdentifier:@"languageChange" sender:self];
    }
    if (indexPath.row == 7) {
        self.isMine = NO;
        [self performSegueWithIdentifier:@"languageChange" sender:self];
    }
    if (indexPath.row == 9) {
        [self performSegueWithIdentifier:@"showPOP" sender:self];
    }
    if (indexPath.row == 10) {
        [self removeUser];
    }
    
    if (indexPath.row == 12) {
        [self removeAllObjectsAndExit];
    }
}

- (void)removeUser {
    [self startActivity];
    [[RequestManager sharedManager] deleteUserwWthSuccessBlock:^(NSDictionary *success) {
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm deleteAllObjects];
        [realm commitWriteTransaction];
        [self showAllertViewWithTittleWithAction:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"User successfully deleted!", nil)];
    } onFalureBlock:^(NSInteger statusCode) {
        [self showAllertViewWithTittleWithAction:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Server error", nil)];
        [self removeActivity];
    }];
//    [[RequestManager sharedManager] removeUserSuccessBlock:^(NSDictionary *success) {
//        if ([[success valueForKey:@"code"] integerValue] == 0) {
//            RLMRealm *realm = [RLMRealm defaultRealm];
//            [realm beginWriteTransaction];
//            [realm deleteAllObjects];
//            [realm commitWriteTransaction];
//            [self showAllertViewWithTittleWithAction:@"" andMessage:@"Юзер успешно удален!"];
//        } else {
//            [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//        }
//    } onFalureBlock:^(NSInteger statusCode) {
//
//    }];
}

- (void)removeAllObjectsAndExit {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] deleteAllObjects];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    [self performSegueWithIdentifier:@"exit" sender:self];
}

- (SettingCell*)configureCellFromIndexPath:(NSIndexPath*)indexPath {
    SettingCell *cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell0"];
    [cell.avatarView setAvatar:self.user.avatar orInitialsView:self.user.name];
    cell.shadowView.layer.cornerRadius = cell.shadowView.frame.size.width / 2;
    cell.shadowView.clipsToBounds = YES;
    cell.nameTextField.placeholder = NSLocalizedString(@"Name", nil);
    cell.lastNameTextField.placeholder = NSLocalizedString(@"Last name", nil);
    cell.nameTextField.text = self.user.name;
    cell.lastNameTextField.text = self.user.lastName;
    if (indexPath.row == 1) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell1"];
    } else if (indexPath.row == 2) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell2"];
        cell.nickName.text = [NSString stringWithFormat:@"@%@",self.user.nickName];
        cell.nickName.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapLabelWithGesture:)];
        [cell.nickName addGestureRecognizer:tapGesture];
        cell.phone.text = [NSString stringWithFormat:@"+%@",self.user.telephone];
    } else if (indexPath.row == 3) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell3"];
    } else if (indexPath.row == 4) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell4"];
        if ([self.user.notification isEqualToNumber:@(1)]) {
            [cell.notification setOn:YES];
        } else {
            [cell.notification setOn:NO];
        }
    } else if (indexPath.row == 5) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell5"];
    } else if (indexPath.row == 6) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell6"];
        RLMLanguage *lang = [[RLMLanguage alloc] init];
        lang = [lang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:self.user.myLangCode];
        cell.value.text = [NSString stringWithFormat:@"%@",lang.languageName];
        [cell.flagImageView setImage:[UIImage imageNamed:self.user.myLangCode]];
    } else if (indexPath.row == 7) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell7"];
        RLMLanguage *lang = [[RLMLanguage alloc] init];
        lang = [lang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:self.user.translateLangCode];
        cell.value.text = [NSString stringWithFormat:@"%@",lang.languageName];
        [cell.flagImageView setImage:[UIImage imageNamed:self.user.translateLangCode]];
        
    } else if (indexPath.row == 8) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell8"];
    } else if (indexPath.row == 9) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell9"];
    } else if (indexPath.row == 10) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell10"];
    } else if (indexPath.row == 11) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell11"];
    } else if (indexPath.row == 12) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell12"];
    } else if (indexPath.row == 13) {
        cell = [self.settingTableView dequeueReusableCellWithIdentifier:@"SettingCell13"];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    [self performSegueWithIdentifier:@"nickName" sender:self];
}

- (void)showAllertViewWithTittleWithAction:(NSString*) tittle andMessage:(NSString*)message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(UIAlertController * _Nonnull controller, UIAlertAction * _Nonnull action, NSInteger buttonIndex) {
        if (buttonIndex == controller.cancelButtonIndex) {
            [self performSegueWithIdentifier:@"exit" sender:self];
            NSLog(@"Cancel Tapped");
        }
    }];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

@end
