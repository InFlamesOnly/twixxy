//
//  NavConstraint.m
//  Twixi
//
//  Created by macOS on 03.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NavConstraint.h"
#import "IOSDeviceName.h"

@implementation NavConstraint

- (CGFloat)constant {
    IOSDeviceName *name = [IOSDeviceName new];
    if ([name.deviceName isEqualToString:@"iPhone X"] || [name.deviceName isEqualToString:@"Simulator"]) {
        return  84;
    } else {
        return 64;
    }
}

@end
