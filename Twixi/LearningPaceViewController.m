//
//  LearningPaceViewController.m
//  Twixi
//
//  Created by macOS on 08.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "LearningPaceViewController.h"
#import "LearningPaceCell.h"
#import "RLMUser.h"
#import "RequestManager.h"

@interface LearningPaceViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tempTableView;

@property (strong, nonatomic) RLMUser *user;

@end

@implementation LearningPaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.user = [RLMUser currentUser];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//TODO отправка изменения темпа на сервер
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [self updateStudyRate:0];
        [self updateTemp:@(0)];

    }
    if (indexPath.row == 2) {
        [self updateStudyRate:1];
        [self updateTemp:@(1)];

    }
    if (indexPath.row == 3) {
        [self updateStudyRate:2];
        [self updateTemp:@(2)];

    }
    [self.tempTableView reloadData];
}

- (void)updateTemp:(NSNumber*)number {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    self.user.userTemp = number;
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (void)updateStudyRate:(int)rate {
    [[RequestManager sharedManager] updateStadyRate:rate withSuccessBlock:^(NSDictionary *success) {

    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return  20;
    } else {
        return 40;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LearningPaceCell *cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LearningPaceCell0" forIndexPath:indexPath];
    }
    if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LearningPaceCell1" forIndexPath:indexPath];
        [self setValueCell:cell fromIndexPath:indexPath];
    }
    if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LearningPaceCell1" forIndexPath:indexPath];
        [self setValueCell:cell fromIndexPath:indexPath];
    }
    if (indexPath.row == 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LearningPaceCell1" forIndexPath:indexPath];
        [self setValueCell:cell fromIndexPath:indexPath];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setValueCell:(LearningPaceCell*)cell fromIndexPath:(NSIndexPath*)indexPath {
    switch (indexPath.row) {
        case 1:
            [cell.cellImage setImage:[UIImage imageNamed:@"TempEasy"]];
            cell.name.text = NSLocalizedString(@"Easy", nil);//easy
            cell.value.text = [NSString stringWithFormat:@"10 %@",NSLocalizedString(@"words a day", nil)];
            if ([self.user.userTemp  isEqualToNumber:@(0)]) {
                [cell.checkBoxImage setImage:[UIImage imageNamed:@"TempOn"]];
            } else {
                [cell.checkBoxImage setImage:[UIImage imageNamed:@"TempOff"]];
            }
            break;
        case 2:
            [cell.cellImage setImage:[UIImage imageNamed:@"TempNormal"]];
            cell.name.text = NSLocalizedString(@"Medium", nil);
            cell.value.text = [NSString stringWithFormat:@"20 %@",NSLocalizedString(@"words a day", nil)];
            if ([self.user.userTemp  isEqualToNumber:@(1)]) {
                [cell.checkBoxImage setImage:[UIImage imageNamed:@"TempOn"]];
            } else {
                [cell.checkBoxImage setImage:[UIImage imageNamed:@"TempOff"]];
            }
            break;
        case 3:
            [cell.cellImage setImage:[UIImage imageNamed:@"TempHight"]];
            cell.name.text = NSLocalizedString(@"Intensified", nil);
            cell.value.text = [NSString stringWithFormat:@"30 %@",NSLocalizedString(@"words a day", nil)];
            cell.viewConstr.constant = 0.0;
            if ([self.user.userTemp  isEqualToNumber:@(2)]) {
                [cell.checkBoxImage setImage:[UIImage imageNamed:@"TempOn"]];
            } else {
                [cell.checkBoxImage setImage:[UIImage imageNamed:@"TempOff"]];
            }
            break;
        default:
            break;
    }
}

@end
