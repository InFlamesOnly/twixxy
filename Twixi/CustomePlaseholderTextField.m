//
//  ChatTextField.m
//  Twixi
//
//  Created by macOS on 16.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "CustomePlaseholderTextField.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation CustomePlaseholderTextField

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {

    }
    return self;
}

- (void)setPlaceholderText:(NSString*)plaseholderText {
    self.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:plaseholderText
                                    attributes:@{
                                                 NSForegroundColorAttributeName: UIColorFromRGB(0x2F4366),
                                                 NSFontAttributeName : [UIFont fontWithName:@"MaisonNeue-Book" size:16]
                                                 }
    ];
}

@end
