//
//  ChatsViewController.m
//  Twixi
//
//  Created by macOS on 15.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ChatsViewController.h"
#import "SearchBarWithCustomeFont.h"
#import "User.h"
#import "ChatsContactCell.h"
#import "RequestManager.h"
#import <UIImageView+AFNetworking.h>
#import "ChatViewController.h"
#import "RLMChat.h"
#import "PCAngularActivityIndicatorView.h"
#import "UIView+ShadowToButton.m"
#import "RLMChat.h"
#import <INSPullToRefresh/INSDefaultPullToRefresh.h>
#import "NSDictionary+DictCategory.h"
#import "Reachability.h"
#import "AvatarView.h"
#import "SubscribeView.h"
#import <SVPullToRefresh.h>
#import "IOSDeviceName.h"
#import "ErrorChatCell.h"


@interface ChatsViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UITableView *chatsTableView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@property BOOL isFiltered;
@property (strong, nonatomic) NSMutableArray* filteredTableData;

@property (strong, nonatomic) NSArray *usersArray;
@property (nonatomic) NSInteger chatId;

@property (weak, nonatomic) IBOutlet UIView *searchViewBcg;
//@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *premiumButton;
@property (weak, nonatomic) IBOutlet UIView *bckSearchView;

@property (strong, nonatomic) SubscribeView *subscribeView;

@property (strong, nonatomic) NSLayoutConstraint *widthSearchViewConstraint;

@property (weak, nonatomic) IBOutlet UIButton *changeButton;

@property int startWidth;
@property int paging;

@end


@implementation ChatsViewController

//TODO сделать получения списка с сервера один раз, апдейтить список только после бека на этот экран
- (void)viewDidLoad {
    [super viewDidLoad];
    [self calculateStartHeightAndAddConstr];
    [self configureStartView];
    [self createTapToDissmissKeyboard];
}

- (void)createTapToDissmissKeyboard {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

- (void)configureStartView {
    self.paging = 1;
    self.searchViewBcg.layer.cornerRadius = 10;
//    [self.errorLabel setHidden:YES];
    [self.clearButton setHidden:YES];
    [self.chatsTableView setHidden:YES];
    [self.chatsTableView setSeparatorColor:[UIColor colorWithRed:209/255 green:220/255 blue:228/255 alpha:0.2]];
}

- (void)calculateStartHeightAndAddConstr {
    UIFont *font = [UIFont systemFontOfSize:14];
    int value = [self widthOfString:self.searchTextField.placeholder withFont:font] + 8 + 12 + 8 +8 + 16 + 8 + 1;
    self.startWidth = value;
    NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self.bckSearchView
                                                                       attribute:NSLayoutAttributeWidth
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:nil
                                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                                      multiplier:1.0
                                                                        constant:value];
    self.widthSearchViewConstraint = widthConstraint;
    [self.bckSearchView addConstraints:@[widthConstraint]];
    [self.view layoutIfNeeded];
}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (void)openSearchView {
    self.widthSearchViewConstraint.constant = UIScreen.mainScreen.bounds.size.width - 32;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)closeSearchView {
    self.widthSearchViewConstraint.constant = self.startWidth;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (void)insertRowAtTop {
    self.paging = self.paging + 1;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self updateData];
}

- (void)updateData {
    [self fromRealmToArray];
    [self checkEmptyChatsArray];
}

- (void)checkEmptyChatsArray {
    if (self.usersArray.count == 0) {
        [self.changeButton setHidden:YES];
//        [self.errorLabel setHidden:NO];
    } else {
       [self.changeButton setHidden:NO];
    }
    [self.chatsTableView setHidden:NO];
    [self.chatsTableView reloadData];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self openSearchView];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length == 0) {
        [self closeSearchView];
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }
    
}

-(void)viewDidLayoutSubviews {
    [self.navigationController.navigationBar setHidden:YES];
}

- (IBAction)showSubscribeScreen:(id)sender {
    IOSDeviceName *name = [IOSDeviceName new];
    if ([name.deviceName isEqualToString:@"iPhone X"] || [name.deviceName isEqualToString:@"Simulator"]) {
         self.subscribeView = [[SubscribeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.tabBarController.view addSubview:self.subscribeView];
        [self.subscribeView openWithIphoneX:self.tabBarController.tabBar.frame.size.height];
    } else {
         self.subscribeView = [[SubscribeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.tabBarController.view addSubview:self.subscribeView];
        [self.subscribeView openWithTabBar:self.tabBarController.tabBar.frame.size.height];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.chatsTableView]) {
        [self.searchTextField resignFirstResponder];
        if (self.searchTextField.text.length == 0) {
            [self closeSearchView];
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                
            }];
        }
        return NO;
    }
    return YES;
}

-(void)dismissKeyboard {
    [self.searchTextField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    searchStr = [searchStr lowercaseString];
    
    [self checkLenghtSearchString:searchStr];
    [self filterTableView:searchStr];
    
    [self.chatsTableView reloadData];
    return YES;
}

- (void)checkLenghtSearchString:(NSString*)searchStr {
    if (searchStr.length > 0) {
        [self openSearchView];
    } else {
        [self closeSearchView];
    }
}

- (void)filterTableView:(NSString*)searchStr {
    if (searchStr.length == 0) {
        [self.clearButton setHidden:YES];
        self.isFiltered = NO;
    } else {
        [self.clearButton setHidden:NO];
        self.isFiltered = YES;
        self.filteredTableData = [[NSMutableArray alloc] init];
        RLMResults *predicate = [RLMChat objectsWhere:@"name CONTAINS[c] %@ OR lastMessage CONTAINS[c] %@",searchStr,searchStr];
        self.filteredTableData = (NSMutableArray*)predicate;
    }
}


- (IBAction)clearSearch:(id)sender {
    [self.clearButton setHidden:YES];
    self.searchTextField.text = @"";
    self.isFiltered = NO;
    [self.chatsTableView reloadData];
    [self closeSearchView];
}

- (void)fromRealmToArray {
    NSMutableArray *mArray = [NSMutableArray new];
    for (RLMChat *chat in [RLMChat allObjects]) {
        [mArray addObject:chat];
    }
    self.usersArray = mArray;
    self.usersArray = [self sortedArray];
}

- (NSArray*)sortedArray {
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sentAt"
                                                 ascending:YES];
    NSArray *sortedArray = [self.usersArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    return [[sortedArray reverseObjectEnumerator] allObjects];
}

- (IBAction)editTable:(id)sender {
    if (self.usersArray.count != 0) {
        if (self.chatsTableView.editing == NO) {
            [self.chatsTableView setEditing:YES animated:YES];
        } else {
            [self.chatsTableView setEditing:NO animated:YES];
        }
        return;
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"cc"]) {
        ChatViewController *vc = segue.destinationViewController;
        vc.selectedFriend = self.selectedFriend;
    }
}

- (IBAction)clearText:(id)sender {
    self.searchTextField.text = @"";
    [self closeSearchView];
    [self.clearButton setHidden:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.usersArray.count != 0) {
        [self showChatViewFromTapIndexPath:indexPath];
    }
}

- (void)showChatViewFromTapIndexPath:(NSIndexPath*)indexPath {
    RLMChat *chat = [[RLMChat alloc] init];
    if (self.isFiltered == YES) {
        chat = [self.filteredTableData objectAtIndex:indexPath.row];
    } else {
        chat = [self.usersArray objectAtIndex:indexPath.row];
    }
    
    chat = [chat findInteresFromArray:(NSArray*)[RLMChat allObjects] chatId:chat.chatId];
    RLMFriend *friend = [[RLMFriend alloc] init];
    RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:chat.friendId];
    self.selectedFriend = findFriend;
    [self performSegueWithIdentifier:@"cc" sender:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextField resignFirstResponder];
    return YES;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *modifyAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        RLMChat *chat = self.usersArray[indexPath.row];
        [self removeChat:chat];
    }];
    modifyAction.backgroundColor = [UIColor redColor];
    return @[modifyAction];
}

- (void)removeChat:(RLMChat*)chat {
    [[RequestManager sharedManager] removeChat:chat.chatId withSuccessBlock:^(NSDictionary *success) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [[RLMRealm defaultRealm] deleteObject:chat];
        [[RLMRealm defaultRealm] commitWriteTransaction];
        [self updateData];
        if (self.usersArray.count != 0) {
            [self.changeButton setHidden:NO];
            [self.chatsTableView setEditing:YES animated:YES];
        } else {
            [self.changeButton setHidden:YES];
            [self.chatsTableView setEditing:NO animated:YES];
        }
        [self.chatsTableView reloadData];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath   {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isFiltered == YES) {
        return self.filteredTableData.count;
    } else {
        if (self.usersArray.count == 0) {
            return 1;
        } else {
            return self.usersArray.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    return [self configureCellFromIndexPath:indexPath];
}

- (UITableViewCell*)configureCellFromIndexPath:(NSIndexPath*)indexPath {
    ChatsContactCell *cell = (ChatsContactCell*)[self.chatsTableView dequeueReusableCellWithIdentifier:@"ChatsContactCell"];
    
    
    RLMChat *chat;
    
    if (self.isFiltered == YES) {
        chat = [self.filteredTableData objectAtIndex:indexPath.row];
        
    } else {
        if (self.usersArray.count == 0) {
            ErrorChatCell *cell = (ErrorChatCell*)[self.chatsTableView dequeueReusableCellWithIdentifier:@"ErrorChatCell"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        } else {
            chat = [self.usersArray objectAtIndex:indexPath.row];
        }
    }
    RLMMessage *lastMessage = [chat.message lastObject];
    
    if (chat.lastMessage == nil) {
        //@"";
        cell.lastMassage.text = NSLocalizedString(@"Start chat", nil);
    } else {
        cell.lastMassage.text = lastMessage.message;
    }
    
    
    cell.name.text = chat.name;
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *dateFromString = [dateFormatter dateFromString:chat.updatedAt];
//    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
//    NSInteger sec = [tz secondsFromGMTForDate: dateFromString];
//    dateFromString = [NSDate dateWithTimeInterval: sec sinceDate: dateFromString];
//    NSDateFormatter *format = [[NSDateFormatter alloc] init];
//    [format setDateFormat:@"HH:mm:ss"];
//    NSString *str = [format stringFromDate:dateFromString];
    cell.lastActivity.text = lastMessage.time;
    
    cell.messageCount.text = [NSString stringWithFormat:@"%@", chat.messageCount];
    if ([chat.messageCount intValue] == 0) {
        [cell.massageCountView setHidden:YES];
        [cell.messageCount setHidden:YES];
    }
    cell.nickName.text = [NSString stringWithFormat:@"@%@",chat.nickName];
    [cell.avatarView setAvatar:chat.avatar orInitialsView:cell.name.text gradientColorsArray:(NSArray*)chat.gradient];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
