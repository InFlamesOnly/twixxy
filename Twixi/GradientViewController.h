//
//  GradientViewController.h
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GradientViewController : UIViewController

- (void)startActivity;
- (void)removeActivity;
- (void)startActivityFromTabBar;
- (void)removeGradient;

@end

NS_ASSUME_NONNULL_END
