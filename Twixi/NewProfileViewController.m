//
//  NewProfileViewController.m
//  Twixi
//
//  Created by macOS on 02.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NewProfileViewController.h"
#import "UIView+ShadowToButton.h"
#import "RequestManager.h"
#import <UIImageView+AFNetworking.h>
#import "NSDictionary+DictCategory.h"
#import "UIAlertController+Blocks.h"
#import "PhotosController.h"
#import <IQKeyboardManager.h>


@interface NewProfileViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, PhotosControllerrDelegate>

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *nickName;
@property (weak, nonatomic) IBOutlet UIButton *loadPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@property (strong, nonatomic) NSTimer *nicknameTimer;

@property (weak, nonatomic) IBOutlet UILabel *errorsNicknameLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *errorActivity;

@property (strong, nonatomic) NSString *avatarPath;

@property (strong, nonatomic) PhotosController *photos;

@property BOOL acceptRegistration;

@end


@implementation NewProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureStartScreens];
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

- (void)configureStartScreens {
    self.avatar.layer.cornerRadius = self.avatar.frame.size.width / 2;
    self.avatar.clipsToBounds = YES;
    self.name.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    self.lastName.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    
    self.errorsNicknameLabel.text = NSLocalizedString(@"This nickname is already used", nil);
    
    [self hideAllErrorsView];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if([string isEqualToString:@" "]){
        // Returning no here to restrict whitespace
        return NO;
    }
    
    if(textField == self.name || textField == self.lastName) {
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet letterCharacterSet] invertedSet]].location != NSNotFound) {
            return NO;
        }

        NSCharacterSet *invalidCharSet = [[NSCharacterSet letterCharacterSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    
    if (textField == self.nickName) {
        NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_"] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        NSString *textFieldText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [self checkNicknameTextField:textFieldText];
        return [self filteredTextField:textField fromString:filtered];
    }
    return NO;

    
//    return [self filteredTextField:textField fromString:filtered];
}

- (void)checkNicknameTextField:(NSString *)text{
    if ([text length] > 0) {
        [self startErrorActivity];
        self.nicknameTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(startRequest:) userInfo:nil repeats:NO];
    } else {
        [self stopTimer];
        [self hideAllErrorsView];
    }
}

- (void)stopTimer {
    [self.nicknameTimer invalidate];
    self.nicknameTimer = nil;
}

-(void)startRequest:(NSTimer *)time {
    [self checkUserNickname:self.nickName.text];
    [self stopTimer];
}

- (void)startErrorActivity {
    [self.errorActivity setHidden:NO];
    [self.errorActivity startAnimating];
    [self.errorsNicknameLabel setHidden:YES];
}

- (void)stopErrorActivity {
    [self.errorActivity setHidden:YES];
    [self.errorActivity stopAnimating];
    [self.errorsNicknameLabel setHidden:NO];
}

- (void)hideAllErrorsView {
    [self.errorActivity setHidden:YES];
    [self.errorsNicknameLabel setHidden:YES];
}

- (BOOL)filteredTextField:(UITextField*)textField fromString:(NSString*)string {
    if (textField == self.name) {
        return [string isEqualToString:string];
    } else if (textField == self.lastName) {
        return [string isEqualToString:string];
    } else if (textField == self.nickName) {
        return [string isEqualToString:string];
    } else {
        return YES;
    }
}

- (IBAction)login:(id)sender {
    if ([self validateAllFields]) {
        if (self.acceptRegistration == YES) {
            [self startActivity];
            [self updateUserFromServer];
        }
    }
}

- (IBAction)tapToLoadPhoto:(id)sender {
    self.photos = [[PhotosController alloc] init];
    self.photos.delegate = self;
    [self.photos showImagePicker];
}

- (void)sendPhoto:(NSString*)photoPath {
    [self startActivity];
    [self sendPhotoFromServer:photoPath];
}

- (void)setPhotoFromIcon:(NSString*)avatarPath {
    [RLMUser updateAvatar:avatarPath];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString: [RLMUser currentUser].avatar]];
    [self.avatar setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        [self.avatar setImage:image];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
        
    }];
    self.avatarPath =  [RLMUser currentUser].avatar;
    [self.loadPhotoButton setHidden:YES];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

- (BOOL)validateAllFields {
    if(self.name.text.length == 0) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", @"") andMessage:NSLocalizedString(@"Enter your name", @"")];
        return NO;
    }
    else if(self.name.text.length > 32) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", @"") andMessage:NSLocalizedString(@"The name is too long", @"")];
        return NO;
    }
    else if(self.lastName.text.length == 0) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", @"") andMessage:NSLocalizedString(@"Enter last name", @"")];
        return NO;
    }
    else if(self.lastName.text.length > 32) {//Last name too long
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", @"") andMessage:NSLocalizedString(@"Last name too long", @"")];
        return NO;
    }
    else if(self.nickName.text.length == 0) {//Enter nickname
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", @"") andMessage:NSLocalizedString(@"Enter nickname", @"")];
        return NO;
    }
    else if(self.nickName.text.length > 32) {//Nickname too long
        [self showAllertViewWithTittle:@"Error" andMessage:NSLocalizedString(@"Nickname too long", @"")];
        return NO;
    }
    else {
        if (self.avatarPath == nil) {
            self.avatarPath = @"";
        }
        return YES;
    }
}

#pragma mark - Requests

- (void)updateUserFromServer {
    [[RequestManager sharedManager] updateUserName:self.name.text lastName:self.lastName.text nickName:self.nickName.text avatar:self.avatarPath withSuccessBlock:^(NSDictionary *success) {
        [RLMUser updateFromRegistrationResponse:success];
        [self removeActivity];
        [self performSegueWithIdentifier:@"loginIsAccept" sender:self];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)sendPhotoFromServer:(NSString*)photoPath {
    [[RequestManager sharedManager] uploadImageFromBundlePath:photoPath withSuccessBlock:^(NSDictionary *success) {
        NSString *avatarPath = [[success valueForKey:@"data"] valueForKey:@"url"];
        [self setPhotoFromIcon:avatarPath];
        [self removeActivity];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Server error"];
    }];
}

- (void)checkUserNickname:(NSString*)nickName {
    self.acceptRegistration = NO;
    [[RequestManager sharedManager] checkUserNickName:nickName successBlock:^(NSDictionary *success) {
        if ([[success valueForKey:@"exists"] boolValue] == YES) {
            [self stopErrorActivity];
            self.acceptRegistration = NO;
        } else {
            self.acceptRegistration = YES;
            [self hideAllErrorsView];
        }
    } onFalureBlock:^(NSInteger statusCode) {
        [self stopErrorActivity];
        self.acceptRegistration = NO;
    }];
}

@end
