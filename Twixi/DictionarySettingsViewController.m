//
//  DictionarySettingsViewController.m
//  Twixi
//
//  Created by macOS on 08.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "DictionarySettingsViewController.h"
#import "DictionarySettingsCell.h"
#import "RLMUser.h"
#import "RLMLanguage.h"

@interface DictionarySettingsViewController () <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;

@property (strong, nonatomic) RLMUser *user;

@end

@implementation DictionarySettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureStartScreens];
}

- (void)configureStartScreens {
    self.user = [RLMUser currentUser];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.settingsTableView reloadData];
}

-(IBAction)back:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return  20;
    } else {
        return 40;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        [self performSegueWithIdentifier:@"learningPace" sender:self];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc]init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DictionarySettingsCell *cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DictionarySettingsCell0" forIndexPath:indexPath];
    }
    if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DictionarySettingsCell1" forIndexPath:indexPath];
        [self setValueCell:cell fromIndexPath:indexPath];
    }
    if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DictionarySettingsCell2" forIndexPath:indexPath];
        [self setValueCell:cell fromIndexPath:indexPath];
    }
    if (indexPath.row == 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DictionarySettingsCell3" forIndexPath:indexPath];
        [self setValueCell:cell fromIndexPath:indexPath];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)setValueCell:(DictionarySettingsCell*)cell fromIndexPath:(NSIndexPath*)indexPath {
    RLMLanguage *lang = [[RLMLanguage alloc] init];
    switch (indexPath.row) {
        case 1:
            if ([self.user.userTemp isEqualToNumber:@(0)]) {
                [cell.flagImage setImage:[UIImage imageNamed:@"TempEasy"]];
                cell.value.text = NSLocalizedString(@"Learning Rate: Easy", nil);
            } else if ([self.user.userTemp isEqualToNumber:@(1)]) {
                [cell.flagImage setImage:[UIImage imageNamed:@"TempNormal"]];
                cell.value.text = NSLocalizedString(@"Learning Rate: Medium", nil);
            } else if ([self.user.userTemp isEqualToNumber:@(2)]) {
                [cell.flagImage setImage:[UIImage imageNamed:@"TempHight"]];
                cell.value.text = NSLocalizedString(@"Learning Rate: Intensified", nil);
            }
            break;
        case 2:
            lang = [lang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:self.user.translateLangCode];
            cell.value.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Language of Learning:", nil) ,lang.languageName];
            [cell.flagImage setImage:[UIImage imageNamed:self.user.translateLangCode]];
            break;
        case 3:
            lang = [lang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:self.user.myLangCode];
            cell.value.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Translation Language:", nil),lang.languageName];
            [cell.flagImage setImage:[UIImage imageNamed:self.user.myLangCode]];
            break;
        default:
            break;
    }
}

@end
