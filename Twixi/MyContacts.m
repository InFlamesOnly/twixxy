//
//  MyContacts.m
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "MyContacts.h"
#import <Contacts/Contacts.h>


@interface MyContacts ()

@end


@implementation MyContacts

+ (instancetype)sharedInstance {
    static MyContacts *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MyContacts alloc] init];

    });
    return sharedInstance;
}

- (void)contactScan {
    if ([CNContactStore class]) {
        CNEntityType entityType = CNEntityTypeContacts;
        if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined) {
            CNContactStore * contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if(granted){

                }
            }];
        }
        else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized) {

        }
    }
}

- (void)contactScanAndGetAllusers {
    if ([CNContactStore class]) {
        CNEntityType entityType = CNEntityTypeContacts;
        if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
        {
            CNContactStore * contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if(granted){
                    [self getAllContact];
                }
            }];
        }
        else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized) {
            [self getAllContact];
        }
    }
}

-(void)getAllContact {
    self.myUsersArray = [NSMutableArray new];
    if([CNContactStore class]) {
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc]init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            NSDictionary *contactDict = [self parseContactWithContact:contact];
            [self.myUsersArray addObject:contactDict];
        }];
    }
}

- (NSDictionary*)parseContactWithContact :(CNContact* )contact {
    NSDictionary *dict = [NSDictionary new];
    NSString * firstName =  contact.givenName;
    NSString * lastName =  contact.familyName;
    NSArray *phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    dict = @{@"phone" : [NSString stringWithFormat:@"%@",phone[0]], @"name" : [NSString stringWithFormat:@"%@ %@",firstName, lastName]};
    return dict;
}

@end
