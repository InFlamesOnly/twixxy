//
//  RLMExamples.m
//  Twixi
//
//  Created by macOS on 13.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMExamples.h"

@implementation RLMExamples

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.exampleId = [response valueForKey:@"id"];
        self.original = [response valueForKey:@"word"];
        self.translate = [response valueForKey:@"translation"];
    }
    return self;
}

@end
