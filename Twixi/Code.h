//
//  Code.h
//  Twixi
//
//  Created by macOS on 03.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Code : NSObject

@property NSString *name;
@property NSString *code;
@property NSString *image;

- (id)initWithName:(NSString*)name image:(NSString*)image code:(NSString*)code;

+ (NSArray*)parseCountryList:(NSArray*)countryList;

@end
