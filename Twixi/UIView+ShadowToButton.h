//
//  UIView+ShadowToButton.h
//  Twixi
//
//  Created by macOS on 02.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ShadowToButton)

+ (void)addGradientToView:(UIView*)view ;
+ (void)addShadowToButton:(UIButton*)button ;
+ (void)addGradientToLoginButton:(UIButton*)button;
+ (void)addGreyGradientToButton:(UIButton*)button;
+ (void)addGradientToCell:(UIView*)view;
+ (void)addGradientToTextField:(UIView*)view;
+ (void)addGradientToView:(UIView*)view
       withGradientColors:(NSArray*)colors;
+ (void)addGradientToViewWithColor:(UIColor*)firstColor secondColor:(UIColor*)secondColor fromView:(UIView*)view;

@end
