//
//  NicknameViewController.h
//  Twixi
//
//  Created by macOS on 24.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface NicknameViewController : GradientViewController

@property NSString *nickName;

@end

NS_ASSUME_NONNULL_END
