//
//  RLMLanguage.m
//  Twixi
//
//  Created by macOS on 18.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMLanguage.h"
#import "RLMScheme.h"

@import CoreTelephony;

@implementation RLMLanguage

+ (NSString *)primaryKey {
    return @"languageCode";
}

- (id)initWithLanguageName:(NSString*)name andCode:(NSString*)code {
    self = [super init];
    if (self) {
        self.languageName = name;
        self.languageCode = code;
    }
    return self;
}

+ (void)saveFromServerResponse:(NSDictionary*)response {
    for (NSDictionary *dic in [response valueForKey:@"data"]) {
        RLMLanguage *lang = [[RLMLanguage alloc] init];
        
        lang.languageCode = [dic valueForKey:@"code"];
        lang.languageName = [dic valueForKey:@"lang"];
        
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [[RLMRealm defaultRealm] addObject:lang];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

+ (RLMLanguage*)findMyLangFromArray:(NSArray*)array {
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    for (RLMLanguage *lang in array) {
        if ([carrier.isoCountryCode isEqualToString:lang.languageCode]) {
            return lang;
        } else {
            return [RLMLanguage english];
        }
    }
    return nil;
}

+ (RLMLanguage*)english {
    RLMScheme *sc = [[RLMScheme alloc] init];
    RLMLanguage *returnedLang = [[RLMLanguage alloc] init];
    for (RLMLanguage *lang in [sc arrayFromMyLanguage]) {
        if ([lang.languageCode isEqualToString:@"EN"]) {
            returnedLang = lang;
        }
    }
    return returnedLang;
}

- (RLMLanguage*)findLanguageFromArray:(NSArray*)array languageCode:(NSString*)languageCode {
    RLMLanguage *language = [RLMLanguage objectForPrimaryKey: languageCode];
    return language;
}

@end
