//
//  RLMChat.m
//  Twixi
//
//  Created by macOS on 23.09.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RLMChat.h"
#import "RLMUser.h"
#import "NSDictionary+DictCategory.h"

@implementation RLMChat

+ (NSString *)primaryKey {
    return @"chatKey";
}

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        response = [response dictionaryRemovingNSNullValues];
        self.chatKey = [NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
        self.chatId = [response valueForKey:@"id"];
        NSDictionary *sender = [[response valueForKey:@"users"] valueForKey:@"data"][0];
        self.friendId = [sender valueForKey:@"id"];
        NSString *name = [sender valueForKey:@"first_name"];
        NSString *lastName = [sender valueForKey:@"last_name"];
        self.userName = [sender valueForKey:@"first_name"];
        self.userLastName = [sender valueForKey:@"last_name"];
        self.userLastOnline = [sender valueForKey:@"last_login"];
        self.name = [NSString stringWithFormat:@"%@ %@",name, lastName];
        self.nickName = [sender valueForKey:@"nickname"];
        self.type = [response valueForKey:@"private"];
        self.avatar = [sender valueForKey:@"avatar"];
        self.createdAt = [response valueForKey:@"created_at"];
        self.updatedAt = [response valueForKey:@"updated_at"];
        self.messageCount = [response valueForKey:@"unread_count"];
        self.lastMessage  = [[[response valueForKey:@"last_message"] valueForKey:@"data"] valueForKey:@"body"];
        self.sentAt = [[[response valueForKey:@"last_message"] valueForKey:@"data"] valueForKey:@"created_at"];;
        
        if ([self.avatar isEqualToString:@""]) {
            NSArray *gradientOneArray = [sender valueForKey:@"gradient"][0];
            NSArray *gradientTwoArray = [sender valueForKey:@"gradient"][1];

            for (NSNumber *i in gradientOneArray) {
                RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
                [self.gradient addObject:value];
            }
            for (NSNumber *i in gradientTwoArray) {
                RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
                [self.gradient addObject:value];
            }
        }
    }
    return self;
}

- (void)convertTime:(NSString*)time {
    
}

- (id)initWithServerResponseFromPush:(NSDictionary*)response {
    self = [super init];
    if (self) {
        response = [response dictionaryRemovingNSNullValues];
        self.chatKey = [NSString stringWithFormat:@"%@",[response valueForKey:@"id"]];
        self.chatId = [response valueForKey:@"id"];
        NSDictionary *sender = [response valueForKey:@"users"][0];
        self.friendId = [sender valueForKey:@"id"];
        NSString *name = [sender valueForKey:@"first_name"];
        NSString *lastName = [sender valueForKey:@"last_name"];
        self.userName = [sender valueForKey:@"first_name"];
        self.userLastName = [sender valueForKey:@"last_name"];
        self.userLastOnline = [sender valueForKey:@"last_login"];
        self.name = [NSString stringWithFormat:@"%@ %@",name, lastName];
        self.nickName = [sender valueForKey:@"nickname"];
        self.type = [response valueForKey:@"private"];
        self.avatar = [sender valueForKey:@"avatar"];
        self.createdAt = [response valueForKey:@"created_at"];
        self.updatedAt = [response valueForKey:@"updated_at"];
        self.messageCount = [response valueForKey:@"unread_count"];
        self.lastMessage  = [[[response valueForKey:@"last_message"] valueForKey:@"data"] valueForKey:@"body"];
        self.sentAt = [[[response valueForKey:@"last_message"] valueForKey:@"data"] valueForKey:@"created_at"];;
        
        if ([self.avatar isEqualToString:@""]) {
//            RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:@(211)];
//            RLMGradientValue *value1 = [[RLMGradientValue alloc] initWithValue:@(211)];
//            RLMGradientValue *value2 = [[RLMGradientValue alloc] initWithValue:@(211)];
//            RLMGradientValue *value3 = [[RLMGradientValue alloc] initWithValue:@(211)];
//            RLMGradientValue *value4 = [[RLMGradientValue alloc] initWithValue:@(211)];
//            RLMGradientValue *value5 = [[RLMGradientValue alloc] initWithValue:@(211)];
//            [self.gradient addObject:value];
//            [self.gradient addObject:value1];
//            [self.gradient addObject:value2];
//            [self.gradient addObject:value3];
//            [self.gradient addObject:value4];
//            [self.gradient addObject:value5];
            

            NSArray *gradientOneArray = [sender valueForKey:@"gradient"][0];
            NSArray *gradientTwoArray = [sender valueForKey:@"gradient"][1];

            for (NSNumber *i in gradientOneArray) {
                RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
                [self.gradient addObject:value];
            }
            for (NSNumber *i in gradientTwoArray) {
                RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
                [self.gradient addObject:value];
            }
        }
    }
    return self;
}

+ (NSArray*)lastTenChats {
    NSArray *chatsArray = (NSArray*)[RLMChat allObjects];
    NSArray *smallArray = [chatsArray subarrayWithRange:NSMakeRange(0, 10)];
    return smallArray;
}

+ (int)arrayObjectsCount {
    return (int)[[RLMChat allObjects] count];
}

+ (void)updateChat:(RLMChat*)newChat {
    RLMChat *chat = [[RLMChat alloc] init];
    chat.chatId = newChat.chatId;
    
    chat.message = chat.message;
    chat.chatKey = newChat.chatKey;
    chat.messageCount = newChat.messageCount;
    chat.name = newChat.name;
    chat.nickName = newChat.nickName;
    chat.avatar = newChat.avatar;
    chat.createdAt = newChat.createdAt;
    chat.lastMessage = newChat.lastMessage;
    chat.type = newChat.type;
    
    
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] addOrUpdateObject:chat];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (RLMChat*)findInteresFromArray:(NSArray*)array chatId:(NSNumber*)chatId {
    NSString *myPrimaryKey = [NSString stringWithFormat:@"%@",chatId];
    RLMChat *chat = [RLMChat objectForPrimaryKey: myPrimaryKey];
    return chat;
}

- (RLMChat*)getChatFromPrimaryKey:(NSNumber*)chatId {
    NSString *myPrimaryKey = [NSString stringWithFormat:@"%@",chatId];
    RLMChat *chat = [RLMChat objectForPrimaryKey: myPrimaryKey];
    return chat;
}

- (void)repleceFirstAndAddToLast:(RLMMessage*)message fromChat:(NSNumber*)chatId {
    NSString *myPrimaryKey = [NSString stringWithFormat:@"%@",chatId];
    RLMChat *chat = [RLMChat objectForPrimaryKey: myPrimaryKey];
//    RLMMessage *firstMessage = chat.message[0];
//    RLMMessage *lastMessage = chat.message[chat.message.count - 1];
    
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [chat.message removeObjectAtIndex:0];
//    [chat.message addObject:message];
//    [[RLMRealm defaultRealm] addOrUpdateObject:chat];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (NSMutableArray*)findFromFriendId:(NSNumber*)friendId {
    NSMutableArray *object = [NSMutableArray new];
    for (RLMChat *chat in [RLMChat allObjects]) {
        if ([chat.friendId isEqualToNumber:friendId]) {
            [object addObject:chat];
        }
    }
    return object;
}

- (BOOL)findMessage:(RLMMessage*)findMessage {
    BOOL messageIsFind = NO;
    for (RLMMessage *message in self.message) {
        if ([message.mId isEqualToNumber:findMessage.mId]) {
            messageIsFind = YES;
            break;
        }
    }
    return messageIsFind;
}

+ (void)saveFromServerResponse:(NSDictionary*)response {
    for (NSDictionary *dic in [response valueForKey:@"data"]) {
        RLMChat *chatResp = [[RLMChat alloc] initWithServerResponse:dic];
        RLMChat *findChat = [chatResp findInteresFromArray:(NSArray*)[RLMChat allObjects] chatId:chatResp.chatId];
        if (findChat == nil) {
            [[RLMRealm defaultRealm] beginWriteTransaction];
            [[RLMRealm defaultRealm] addObject:chatResp];
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
    }
}

- (RLMMessage*)findMessageFromChat:(NSNumber*)mId {
    RLMMessage *message = [[RLMMessage alloc] init];
    for (RLMMessage *findMessage in self.message) {
        if ([findMessage.mId isEqualToNumber:mId]) {
            message = findMessage;
            break;
        }
    }
    return message;
}

@end
