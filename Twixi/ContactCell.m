//
//  ContactCell.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ContactCell.h"
#import <UIKit/UIKit.h>

@implementation ContactCell


- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.avatar.layer.cornerRadius = self.avatar.bounds.size.width/2;
//    self.avatar.layer.cornerRadius = self.avatar.frame.size.width / 2;
//    self.avatar.clipsToBounds = YES;
    self.onlineView.layer.cornerRadius = self.onlineView.frame.size.width / 2;
    self.onlineView.clipsToBounds = YES;
    self.onlineView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.onlineView.backgroundColor = [UIColor greenColor];
    self.onlineView.layer.borderWidth = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

@end
