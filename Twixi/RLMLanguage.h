//
//  RLMLanguage.h
//  Twixi
//
//  Created by macOS on 18.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMLanguage : RLMObject

@property NSString *languageName;
@property NSString *languageCode;
@property NSNumber <RLMInt> *languageId;

- (id)initWithLanguageName:(NSString*)name andCode:(NSString*)code;
- (RLMLanguage*)findLanguageFromArray:(NSArray*)array languageCode:(NSString*)languageCode;

+ (void)saveFromServerResponse:(NSDictionary*)response;
+ (RLMLanguage*)english;

+ (RLMLanguage*)findMyLangFromArray:(NSArray*)array;

@end
