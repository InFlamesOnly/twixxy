//
//  ProfileViewController.m
//  Twixi
//
//  Created by macOS on 24.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "ProfileViewController.h"
#import "RLMUser.h"
#import "RLMStudy.h"
#import "RLMLanguage.h"
#import "AvatarView.h"
#import "SubscribeView.h"

@interface ProfileViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet AvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UIButton *premiumButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIView *levelViewOne;
@property (weak, nonatomic) IBOutlet UIView *levelViewTwo;
@property (weak, nonatomic) IBOutlet UILabel *levelOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *temp;
@property (weak, nonatomic) IBOutlet UILabel *words;
@property (weak, nonatomic) IBOutlet UILabel *languageStudy;
@property (weak, nonatomic) IBOutlet UIProgressView *progresView;

@property (strong, nonatomic) SubscribeView *subscribeView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateInfo];
    [self addTapToImageView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self updateInfo];
}

- (void)addTapToImageView {
    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.avatarView addGestureRecognizer:singleFingerTap];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}

- (void)updateInfo {
    [self updateUserInfo];
    [self updateLangs];
    [self updateStudy];
}

- (void)updateUserInfo {
    RLMUser *currentUser = [RLMUser currentUser];
    self.name.text = currentUser.name;
    self.nickName.text = [NSString stringWithFormat:@"@%@",currentUser.nickName];
    [self.avatarView setAvatar:currentUser.avatar orInitialsView:currentUser.name];
}

- (void)updateLangs {
    RLMUser *currentUser = [RLMUser currentUser];
    RLMLanguage *lang = [[RLMLanguage alloc] init];
    lang = [lang findLanguageFromArray:(NSArray*)[RLMLanguage allObjects] languageCode:currentUser.translateLangCode];
    [self updateLocalizedStringFromLang:lang];
}

- (void)updateLocalizedStringFromLang:(RLMLanguage*)lang {
    self.languageStudy.text = [NSString stringWithFormat:@"%@ : %@", NSLocalizedString(@"Language learning", nil) ,lang.languageName];
}

- (void)updateStudy {
    RLMStudy *currentStudy = [RLMStudy currentStudy];
    self.levelOneLabel.text = [NSString stringWithFormat:@"%@",currentStudy.level];
    NSNumber *number = currentStudy.level;
    int value = [number intValue];
    number = [NSNumber numberWithInt:value + 1];
    self.levelTwoLabel.text = [NSString stringWithFormat:@"%@",number];

    [self updateLocalizedStringFromStudy:currentStudy];
    
    [self progresViewFromStudy:currentStudy];
}

- (void)updateLocalizedStringFromStudy:(RLMStudy*)currentStudy {
    self.scoreLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", currentStudy.scoreCurrent, NSLocalizedString(@"of", nil), currentStudy.scoreMax, NSLocalizedString(@"points", nil)];
    self.words.text = [NSString stringWithFormat:@"%@ %@", currentStudy.scoreCurrent, NSLocalizedString(@"Words learned", nil)];
    self.temp.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Learning rate:", nil) ,currentStudy.studyRate];
}

- (void)progresViewFromStudy:(RLMStudy*)currentStudy {
    NSNumber *firstValue = currentStudy.scoreCurrent;
    int firstValueInt = [firstValue intValue];
    NSNumber *secondValue = currentStudy.scoreMax;
    int secondValueInt = [secondValue intValue];
    float progressValue = firstValueInt/secondValueInt;
    [self.progresView setProgress:progressValue animated:YES];
}

- (IBAction)showSubscribeScreen:(id)sender {
    self.subscribeView = [[SubscribeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [self.tabBarController.view addSubview:self.subscribeView];
    [self.subscribeView openWithTabBar:self.tabBarController.tabBar.frame.size.height];
}

@end
