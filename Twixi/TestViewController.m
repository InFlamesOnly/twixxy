//
//  TestViewController.m
//  Twixi
//
//  Created by macOS on 05.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "TestViewController.h"
#import "ChatViewController.h"
#import "PCAngularActivityIndicatorView.h"

@interface TestViewController ()

@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    [self.viewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
        vc.tabBarItem.title = nil;
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"TabBar"]) {
        UINavigationController *navController = [segue destinationViewController];
        ChatViewController *vc = (ChatViewController *)([navController viewControllers][0]);
        vc.chat = self.chat;
    }
}

//- (void)removeActivity {
//    [self.activity removeFromSuperview];
//    [self.activityView removeFromSuperview];
//}
//
//- (void)addActivityIndicator {
//    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    self.activityView.backgroundColor = [UIColor blackColor];
//    self.activityView.alpha = 0.5;
//    [self.view insertSubview:self.activityView atIndex:2];
//    self.activityView.center = self.view.center;
//    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
//
//    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
//    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
//    [self.activity startAnimating];
//    [self.view insertSubview:self.activity atIndex:2];
//}

@end
