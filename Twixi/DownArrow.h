//
//  DownArrow.h
//  Twixi
//
//  Created by macOS on 18.01.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DownArrowDelegate

- (void)tapToDownButton;

@end

@interface DownArrow : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *downButton;

@property (nonatomic,retain) id <DownArrowDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
