//
//  TestViewController.h
//  Twixi
//
//  Created by macOS on 05.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMChat.h"

@interface TestViewController : UITabBarController

@property (strong, nonatomic) RLMChat *chat;

//- (void)addActivityIndicator;
//- (void)removeActivity;

@end
