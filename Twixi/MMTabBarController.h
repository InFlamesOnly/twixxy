//
//  LMTabbedController.h
//  Management
//
//  Created by Olga Dalton on 16/04/14.
//

#import <UIKit/UIKit.h>

@interface MMTabBarController : UIViewController

@property (nonatomic, strong) UIViewController *currentViewController;
@property (nonatomic, weak) IBOutlet UIView *placeholderView;
@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *tabBarButtons;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *botConstraint;

- (void) setSelectedIndex: (int) index;

@end
    
