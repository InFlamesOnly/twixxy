//
//  LanguageViewController.h
//  Twixi
//
//  Created by macOS on 18.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMLanguage.h"

@protocol LanguageViewControllerDelegate

- (void)getLanguage:(RLMLanguage*)language;
- (void)getTrnLanguage:(RLMLanguage*)language;
@end

@interface LanguageViewController : UIViewController

@property BOOL isFirstTrLanguage;
@property BOOL isFirstMyLanguage;
@property BOOL isMineLanguage;
@property BOOL isTnsLanguage;

@property (nonatomic,retain) id <LanguageViewControllerDelegate> delegate;

@end
