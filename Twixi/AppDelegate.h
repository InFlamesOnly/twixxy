//
//  AppDelegate.h
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMChat.h"
#import "RLMFriend.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RLMChat *chat;
@property (strong, nonatomic) RLMFriend *respFriend;


@end

