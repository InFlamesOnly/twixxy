//
//  User.h
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIkit.h"

@interface User : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *seenAs;
@property (strong, nonatomic) NSString *avatarImage;

@property (strong, nonatomic) NSNumber *role;
@property (strong, nonatomic) NSString *timeSms;

+ (NSArray*)testUserBase;
+ (NSArray*)testUserBaseFromChat;

@end
