//
//  CardView.m
//  Twixi
//
//  Created by macOS on 06.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "CardView.h"
#import "UIView+ShadowToButton.m"

@implementation CardView

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {

    }
    return self;
}

-(void)drawRect:(CGRect)rect {
    [UIView addGradientToLoginButton:self.startButton];

}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)customize {
    self.wordView.layer.cornerRadius = self.wordView.frame.size.width / 2;
    self.wordView.clipsToBounds = YES;
    self.repeatView.layer.cornerRadius = self.repeatView.frame.size.width / 2;
    self.repeatView.clipsToBounds = YES;
    
    UIColor *firstColorWordView = [UIColor colorWithRed:50.0f/255.0f green:181.0f/255.0f blue:135.0f/255.0f alpha:1];
    UIColor *secondColorWordView = [UIColor colorWithRed:63.0f/255.0f green:203.0f/255.0f blue:136.0f/255.0f alpha:1];
    
    [UIView addGradientToViewWithColor:firstColorWordView secondColor:secondColorWordView fromView:self.wordView];
    
    UIColor *firstColorRepeatView = [UIColor colorWithRed:0.0f/255.0f green:198.0f/255.0f blue:251.0f/255.0f alpha:1];
    UIColor *secondColorRepeatView  = [UIColor colorWithRed:0.0f/255.0f green:91.0f/255.0f blue:234.0f/255.0f alpha:1];
    [UIView addGradientToViewWithColor:firstColorRepeatView secondColor:secondColorRepeatView fromView:self.repeatView];
    self.bcgVew.layer.cornerRadius = 15;
    self.bcgVew.layer.masksToBounds = YES;
    
    [self setTextFromCart];
}

- (void)setTextFromCart {
    [self.lvl setText:NSLocalizedString(@"Level 1", nil)];
    [self.words setText:NSLocalizedString(@"New words", nil)];
    [self.repeatsWords setText:NSLocalizedString(@"Repetitions", nil)];
    [self.todayPlan setText:NSLocalizedString(@"Plan for today:", nil)];
    [self.startButton setTitle:NSLocalizedString(@"Start now", nil) forState:UIControlStateNormal];
}

-(IBAction)start:(id)sender {
    [self.delegate tapToStart];
}

+ (NSArray*)testCardViewsfromView:(UIView*)view {
    CardView *rootView = [[[NSBundle mainBundle] loadNibNamed:@"CardView" owner:self options:nil] objectAtIndex:0];
    rootView.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [rootView customize];
    [rootView setTextFromCart];
    
    CardView *rootView1 = [[[NSBundle mainBundle] loadNibNamed:@"CardView" owner:self options:nil] objectAtIndex:0];
    rootView1.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [rootView1 customize];
    
    CardView *rootView2 = [[[NSBundle mainBundle] loadNibNamed:@"CardView" owner:self options:nil] objectAtIndex:0];
    rootView2.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    [rootView2 customize];
    
    return [[NSArray alloc] initWithObjects:rootView, rootView1, rootView2 ,nil];
}

@end
