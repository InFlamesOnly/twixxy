//
//  NicknameViewController.m
//  Twixi
//
//  Created by macOS on 24.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NicknameViewController.h"
#import "RequestManager.h"
#import "RLMUser.h"
#import "UIAlertController+Blocks.h"
#import "Reachability.h"

@interface NicknameViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nickNameTextField;

@property NSString *startNickname;

@end

@implementation NicknameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configStartView];
    [self removeGradient];
    [self createTapFromDissmiss];
}

- (void)createTapFromDissmiss {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)configStartView {
    self.startNickname = self.nickName;
    self.nickNameTextField.text = self.nickName;
    self.nickNameTextField.placeholder = NSLocalizedString(@"Nickname", nil);
}

- (IBAction)accept:(id)sender{
    [self.nickNameTextField resignFirstResponder];
    if ([self checkValidateNickname]) {
        [self checkUserNicknameFromServer];
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(textField == self.nickNameTextField) {
        return [self configNickNametextField:string];
    }
    return YES;
}

- (BOOL)configNickNametextField:(NSString*)string {
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890_"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (void)checkUserNicknameFromServer {
    if (![self.startNickname isEqualToString:self.nickNameTextField.text]) {
        [self checkInternetConnection];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)sendFromCheckUserNicknameFromServer {
    [[RequestManager sharedManager] checkUserNickName:self.nickNameTextField.text successBlock:^(NSDictionary *success) {
        if ([self parseResponse:success]) {
            [self sendNewNicknameFromServer];
        }
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)checkInternetConnection {
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Internet connection error", nil)];
    } else {
        [self startActivityFromTabBar];
        [self sendFromCheckUserNicknameFromServer];
    }
}


- (BOOL)parseResponse:(NSDictionary*)success {
    BOOL exists = [[success valueForKey:@"exists"] boolValue];
    if (exists == NO) {
        [self removeActivity];
        [self startActivityFromTabBar];
        return YES;
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The specified nickname already exists", nil)];
        return NO;
    }
    return NO;
}

- (void)sendNewNicknameFromServer {
    RLMUser *user = [RLMUser currentUser];
    [[RequestManager sharedManager] updateUserName:user.name lastName:user.lastName nickName:self.nickNameTextField.text avatar:@"" withSuccessBlock:^(NSDictionary *success) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        user.nickName = self.nickNameTextField.text;
        [[RLMRealm defaultRealm] commitWriteTransaction];
        [self removeActivity];
        [self.navigationController popViewControllerAnimated:YES];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (BOOL)checkValidateNickname {
    if (self.nickNameTextField.text.length < 5) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The minimum length of the nickname is 5 characters.", nil)];
        return NO;
    } else if (self.nickNameTextField.text.length > 32) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"The maximum length of the nickname is 32 characters.", nil)];
        return NO;
    } else {
        return YES;
    }
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

@end
