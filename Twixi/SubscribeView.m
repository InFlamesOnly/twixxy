//
//  SubscribeView.m
//  Twixi
//
//  Created by macOS on 05.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "SubscribeView.h"
#import "IAPServices.h"
#import "RLMSubscribe.h"
#import "RequestManager.h"
#import "User.h"


//Fix bug iphone 10 show subscribe
@implementation SubscribeView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (void)roundTopCorners {
    self.bcgView.layer.masksToBounds = YES;
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    
    self.bcgView.layer.mask = maskLayer;
}

- (void)createLogickPopLabel {
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popTapped)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.tappedLabelPoP addGestureRecognizer:tapGestureRecognizer];
    self.tappedLabelPoP.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsTapped)];
    tapGestureRecognizer2.numberOfTapsRequired = 1;
    [self.tappedLabelTermsOfUse addGestureRecognizer:tapGestureRecognizer2];
    self.tappedLabelTermsOfUse.userInteractionEnabled = YES;
}

- (void)popTapped {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twixymessenger.ucoz.net/twixy_Privacy_Policy.pdf"]];
}

- (void)termsTapped {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twixymessenger.ucoz.net/Terms_of_Use-2.html"]];
}

- (IBAction)changeSelectionState:(id)sender {
    if (self.isSelected == NO) {
        self.isSelected = YES;
        [self.selectedButton setImage:[UIImage imageNamed:@"TermChecked"] forState:UIControlStateNormal];
    } else if (self.isSelected == YES) {
        [self.selectedButton setImage:[UIImage imageNamed:@"TermUnchecked"] forState:UIControlStateNormal];
        self.isSelected = NO;
    }
}

- (void)customeinit {
    [[NSBundle mainBundle] loadNibNamed:@"SubscribeView" owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    self.scrollView.layer.masksToBounds = YES;
    self.scrollView.layer.cornerRadius = 13;
    self.bcgView.layer.masksToBounds = YES;
    self.bcgView.layer.cornerRadius = 13;
    [IAPServices sharedInstance].delegate = self;
    [self setText];
    self.isSelected = NO;
    [self createLogickPopLabel];
}

- (void)setText {
    [self.tittleLabel setText:NSLocalizedString(@"Buy Premium Subscription!", nil)];
    [self.subtittleLabel setText:NSLocalizedString(@"What gives a premium subscription", nil)];
    [self.firstLabel setText:NSLocalizedString(@"Unlimited real-time words translate from GoogleTranslate", nil)];
    [self.secondLabel setText:NSLocalizedString(@"Push notifications with new words to learn the language.", nil)];
    [self.thirdLabel setText:NSLocalizedString(@"Unlimited languages", nil)];
    [self.weakLabel setText:NSLocalizedString(@"1 Weak", nil)];
    [self.mounthLabel setText:NSLocalizedString(@"1 Month", nil)];
    [self.threemounthLabel setText:NSLocalizedString(@"3 Month", nil)];
}

- (void)open {
    CGRect screen = [[UIScreen mainScreen] bounds];
    
    self.heightConstraint.constant = screen.size.height + 20;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    }];
}

- (void)openWithTabBar:(CGFloat)tabBarHeight {
    CGRect screen = [[UIScreen mainScreen] bounds];
    self.heightConstraint.constant = screen.size.height - 20 + tabBarHeight;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    }];
}

- (void)openWithIphoneX:(CGFloat)tabBarHeight {
    CGRect screen = [[UIScreen mainScreen] bounds];
    self.heightConstraint.constant = screen.size.height - 10;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    }];
}

- (IBAction)close {
    self.heightConstraint.constant = 0.0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)buyFirst:(id)sender {
    if (self.isSelected == YES) {
        [[IAPServices sharedInstance] getProducts:@"DM.twiximassanger.com.Twixi.weaksubscribe"];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Need to accept the rules", nil)];
        
    }
}

- (IBAction)buySecond:(id)sender {
    if (self.isSelected == YES) {
        [[IAPServices sharedInstance] getProducts:@"DM.twiximassanger.com.Twixi.onemonth"];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Need to accept the rules", nil)];
    }
}

- (IBAction)buyThird:(id)sender {
    if (self.isSelected == YES) {
        [[IAPServices sharedInstance] getProducts:@"DM.twiximassanger.com.Twixi.threemonth"];
    } else {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Need to accept the rules", nil)];
    }
}

- (void) closeView {
    [self close];
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    UIViewController *currentTopVC = [self topViewController];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [currentTopVC presentViewController:alertController animated:YES completion:NULL];
}

@end
