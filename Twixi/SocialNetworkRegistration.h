//
//  SocialNetworkRegistration.h
//  Twixi
//
//  Created by macOS on 19.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialNetworkRegistration : UIViewController

@property NSString *name;
@property NSString *lastName;
@property NSString *email;
@property NSString *token;

- (void)addActivityIndicator;
- (void)removeActivity;
- (void) goToRegistration;

@end
