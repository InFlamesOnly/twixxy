//
//  RLMExamples.h
//  Twixi
//
//  Created by macOS on 13.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMExamples : RLMObject

@property NSNumber <RLMInt> *exampleId;
@property NSString *original;
@property NSString *translate;

- (id)initWithServerResponse:(NSDictionary*)response;

@end

RLM_ARRAY_TYPE(RLMExamples)
