//
//  ChatViewController.h
//  Twixi
//
//  Created by macOS on 16.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMFriend.h"
#import "RLMChat.h"

//TODO сделать показ стрелки и запрограмировать ее поведение

@interface ChatViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *messageArray;
@property (strong, nonatomic) IBOutlet UITableView *chatTableView;
@property (strong, nonatomic) RLMFriend *selectedFriend;
@property (nonatomic) NSInteger chatId;
@property (strong, nonatomic) RLMChat *chat;

@property BOOL needLoadMessage;

- (void)updateContentInsetForTableView:(UITableView *)tableView animated:(BOOL)animated ;

@end
