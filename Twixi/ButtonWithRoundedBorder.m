//
//  ButtonWithRoundedBorder.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ButtonWithRoundedBorder.h"

@implementation ButtonWithRoundedBorder

- (instancetype)init {
    self = [super init];
    if (self) {
        self.layer.cornerRadius = 3;
        self.clipsToBounds = YES;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.layer.cornerRadius = 3;
        self.clipsToBounds = YES;
    }
    return self;
}

@end
