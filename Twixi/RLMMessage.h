//
//  RLMMessage.h
//  Twixi
//
//  Created by macOS on 28.09.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMMessage : RLMObject

@property NSNumber <RLMInt> *role;
@property NSNumber <RLMInt> *mId;
@property NSString *message;
@property NSString *time;
@property BOOL isSend;

- (id)initWithRole:(NSNumber*)role message:(NSString*)message endTime:(NSString*)time;

@end

RLM_ARRAY_TYPE(RLMMessage)
