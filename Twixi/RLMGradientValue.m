//
//  RLMGradientValue.m
//  Twixi
//
//  Created by macOS on 09.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMGradientValue.h"

@implementation RLMGradientValue

- (id)initWithValue:(NSNumber*)value {
    self = [super init];
    if (self) {
        self.value = value;
    }
    return self;
}

@end


