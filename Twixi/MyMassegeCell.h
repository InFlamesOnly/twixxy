//
//  MyMassageCell.h
//  Twixi
//
//  Created by macOS on 16.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TCCopyableLabel.h>
#import "AvatarView.h"

@interface MyMassegeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UIView *messageView;
@property (weak, nonatomic) IBOutlet TCCopyableLabel *message;
@property (weak, nonatomic) IBOutlet UILabel *time;

@property (weak, nonatomic) IBOutlet UIImageView *acceptSendMessage;

@property BOOL isTranslated;
@property BOOL isSend;

@property (weak, nonatomic) IBOutlet UILabel *initials;

@end
