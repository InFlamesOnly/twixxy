//
//  AppDelegate.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

//+380988682571

// define macro
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#import "AppDelegate.h"
#import <IQKeyboardManager.h>
#import "RLMUser.h"
#import "RequestManager.h"
#import "UIViewController+CurrentViewController.h"
#import "ChatViewController.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "SocialNetworkRegistration.h"
#import "SignUp.h"
#import "Loginisation.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Stripe.h>
#import "RLMTranslationCart.h"
#import "RLMLanguage.h"
#import "ContactsViewController.h"
#import "TestViewController.h"
#import "NSDictionary+DictCategory.h"
#import <StoreKit/StoreKit.h>
#import "UIViewController+CurrentViewController.h"
#import "ChatsViewController.h"
//@import Stripe;
//fb858909467597546 - URL SCHEME  and baundle!

//TODO subscribe потестить или приняло банковскую хрень, запилить на экран подписок

@import Firebase;
@import UserNotifications;

@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate, GIDSignInDelegate>

@property (strong, nonatomic) RLMUser *user;


@end

@implementation AppDelegate {
    UIBackgroundTaskIdentifier bgTask;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FBSDKAppEvents activateApp];

    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"TabBar"]];
    [UITabBar appearance].layer.borderWidth = 0.0f;
    [UITabBar appearance].clipsToBounds = true;
    //TODO key
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:@"pk_test_3z5Oszifjo1FZ7uyjtX2w0TO"];
    [[STPPaymentConfiguration sharedConfiguration] setAppleMerchantIdentifier:@"merchant.com.Twixi.pay"];
    // do any other necessary launch configuration
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//     [[IQKeyboardManager sharedManager] setEnable:YES];
    [GIDSignIn sharedInstance].clientID = @"407312439986-qfc9p2oqj58mtme7hlgev6qvicj7jk52.apps.googleusercontent.com";
//    [GIDSignIn sharedInstance].delegate = self;
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }

    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [FIRApp configure];
    [self checkFirstScreen];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //[UIColor colorWithRed:199.0f/255.0f green:202.0f/255.0f blue:220.0f/255.0f alpha:1];
    
    [UITabBarItem.appearance setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor colorWithRed:199.0f/255.0f green:202.0f/255.0f blue:220.0f/255.0f alpha:1]}
                                           forState:UIControlStateNormal];
    
    return YES;
}


- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
//    NSString *userId = user.userID;                  // For client-side use only!
//    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    
    self.user.googleId = user.userID;
    

    UIViewController *vc = ((UINavigationController*)self.window.rootViewController).visibleViewController;
    if ([vc.class.description isEqualToString:@"SFAuthenticationViewController"]) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SocialNetworkRegistration *socVC = [storyboard instantiateViewControllerWithIdentifier:@"SocialNetworkRegistration"];
            socVC.name = givenName;
            socVC.lastName = familyName;
            socVC.email = email;
            socVC.token = user.userID;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:user.userID forKey:@"googleID"];
            [defaults synchronize];
            [socVC goToRegistration];

        return;
    }
    if ([vc isKindOfClass:[SocialNetworkRegistration class]]) {
        SocialNetworkRegistration *socVC = (SocialNetworkRegistration*)vc;
        socVC.name = givenName;
        socVC.lastName = familyName;
        socVC.email = email;
        socVC.token = user.userID;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:user.userID forKey:@"googleID"];
        [defaults synchronize];
        [socVC performSegueWithIdentifier:@"googleRegistration" sender:socVC];
    }
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *, id> *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
}

//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//       willPresentNotification:(UNNotification *)notification
//         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
//{
//    NSLog( @"Handle push from foreground" );
//    // custom code to handle push while app is in the foreground
//    NSLog(@"%@", notification.request.content.userInfo);
//}
//
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//didReceiveNotificationResponse:(UNNotificationResponse *)response
//         withCompletionHandler:(void (^)())completionHandler
//{
//    NSLog( @"Handle push from background or closed" );
//    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
//    NSLog(@"%@", response.notification.request.content.userInfo);
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
}

- (void)changeMessageFromPusher:(NSDictionary*)data {
    NSString *newMessageText = [[data valueForKey:@"data"] valueForKey:@"body"];
    NSNumber *messageId = [[data valueForKey:@"data"] valueForKey:@"id"];
    NSNumber *friendId = [[[[data valueForKey:@"data"] valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"id"];
    RLMChat *findChat = [[RLMChat alloc] init];
    NSArray *obj = [findChat findFromFriendId:friendId];
    if (obj.count != 0) {
        RLMChat *chat = obj[0];
        RLMMessage *message = [chat findMessageFromChat:messageId];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        message.message = newMessageText;
        message.time = message.time;
        message.mId = message.mId;
        message.role = message.role;
        message.isSend = message.isSend;
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
//    RLMMessage *messageRlm = [self findMessageFromMessageId:[[data valueForKey:@"data"] valueForKey:@"id"]];
//    if (messageRlm.mId != nil) {
//        [[RLMRealm defaultRealm] beginWriteTransaction];
//        messageRlm.message = newMessageText;
//        messageRlm.time = messageRlm.time;
//        messageRlm.mId = messageRlm.mId;
//        messageRlm.role = messageRlm.role;
//        messageRlm.isSend = messageRlm.isSend;
//        NSUInteger i = [self.messageArray indexOfObject:messageRlm];
//        [self.messageArray replaceObjectAtIndex:i withObject:messageRlm];
//
//        if (self.messageArray.count == i + 1) {
//            RLMMessage *newLastMessage = [self.messageArray lastObject];
//            self.chat.lastMessage = newLastMessage.message;
//        }
//
//        [[RLMRealm defaultRealm] commitWriteTransaction];
//    }
}

- (void)removeMessagePusher:(NSDictionary*)data {
    NSNumber *messageId = [[data valueForKey:@"data"] valueForKey:@"id"];
    NSNumber *friendId = [[[[data valueForKey:@"data"] valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"id"];
    RLMChat *findChat = [[RLMChat alloc] init];
    NSArray *obj = [findChat findFromFriendId:friendId];
    if (obj.count != 0) {
        RLMChat *chat = obj[0];
        RLMMessage *message = [chat findMessageFromChat:messageId];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        NSUInteger i = [chat.message indexOfObject:message];
        [chat.message removeObjectAtIndex:i];
        RLMMessage *newLastMessage = [chat.message lastObject];
        chat.lastMessage = newLastMessage.message;
        chat.sentAt = newLastMessage.time;
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
                                                       fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"MESSAGE!!!!!!!!!!!!!!!!!!!!!!!!!!");
    
    NSDictionary *info = [[NSDictionary alloc] init];
    info = userInfo;
    
    RLMChat *chatObject = [[RLMChat alloc] init];
    
    UIViewController *controller = [UIViewController currentViewController];
    
    if ([[info valueForKey:@"type"] isEqualToString:@"MESSAGE_EDIT"]) {
        NSError *jsonError;
        NSData *objectData = [[info valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        [self changeMessageFromPusher:json];
        NSLog(@"%@",json);
    }
    
    if ([[info valueForKey:@"type"] isEqualToString:@"MESSAGE_DELETED"]) {
        NSError *jsonError;
        NSData *objectData = [[info valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        NSLog(@"%@",json);
        [self removeMessagePusher:json];
    }
    
    if ([[info valueForKey:@"type"] isEqualToString:@"SUBSCRIPTION_EXPIRED"]) {
        RLMUser *currentUser = [RLMUser currentUser];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        currentUser.userBuySubscribe = @(0);
        currentUser.userEnableTranslate = @(0);
        [[RLMRealm defaultRealm] commitWriteTransaction];
    } else if ([[info valueForKey:@"type"] isEqualToString:@"DICTIONARY"]) {
        self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TestViewController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        self.window.rootViewController = tabBar;
        [self.window makeKeyAndVisible];
        [tabBar setSelectedIndex:2];
    } else if ([[info valueForKey:@"type"] isEqualToString:@"MESSAGE_NEW"]) {
        if ([info valueForKey:@"data"]) {
            
            NSError *jsonError;
            NSData *objectData = [[info valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            NSNumber *friendId = [[[[json valueForKey:@"data"] valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"id"];
            RLMChat *findChat = [[RLMChat alloc] init];
            NSArray *obj = [findChat findFromFriendId:friendId];
            
            if (![controller isKindOfClass:[ChatViewController class]]) {
                if (obj.count != 0) {
                    RLMChat *chat = obj[0];
                    chatObject = chat;
                    NSDictionary *dict = [json valueForKey:@"data"];
                    
                    RLMMessage *message = [[RLMMessage alloc] init];
                    message.message = [dict valueForKey:@"body"];
                    NSString *result = [self convert:[dict valueForKey:@"created_at"] fromNeedDateFormat:@"HH:mm:ss"];
                    message.role = @(2);
                    message.time = result;
                    message.mId = [dict valueForKey:@"id"];
                    message.isSend = YES;
                    
                    BOOL isFind = [chatObject findMessage:message];
                    
                    if (!isFind) {
                        if (chat.message.count == 0) {
                            [[RLMRealm defaultRealm] beginWriteTransaction];
                            chat.lastMessage = message.message;
                            chat.sentAt = [dict valueForKey:@"created_at"];
                            [[RLMRealm defaultRealm] commitWriteTransaction];
                            NSLog(@"%@",obj);
//                            return;
                        } else {
                            if (chat.message.count >= 30) {
                            [chat repleceFirstAndAddToLast:message fromChat:chat.chatId];
                            [[RLMRealm defaultRealm] beginWriteTransaction];
                            [chat.message addObject:message];
                            [[RLMRealm defaultRealm] commitWriteTransaction];
                            NSLog(@"SAVE MESSAGE");
                        } else {
                            [[RLMRealm defaultRealm] beginWriteTransaction];
                            [chat.message addObject:message];
                            [[RLMRealm defaultRealm] commitWriteTransaction];
                            NSLog(@"SAVE MESSAGE");
                        }
                        }
                    }
                } else {
                    NSDictionary *dict = [json valueForKey:@"data"];
                    RLMChat *chat = [[RLMChat alloc] initWithServerResponseFromPush:[dict valueForKey:@"conversation"]];

                    chatObject = chat;
                    
                    [[RLMRealm defaultRealm] beginWriteTransaction];
                    [[RLMRealm defaultRealm] addObject:chat];
                    [[RLMRealm defaultRealm] commitWriteTransaction];
                    
                    RLMMessage *message = [[RLMMessage alloc] init];
                    message.message = [dict valueForKey:@"body"];
                    NSString *result = [self convert:[dict valueForKey:@"created_at"] fromNeedDateFormat:@"HH:mm:ss"];
                    message.role = @(2);
                    message.time = result;
                    message.mId = [dict valueForKey:@"id"];
                    message.isSend = YES;

                    [[RLMRealm defaultRealm] beginWriteTransaction];
                    chat.lastMessage = message.message;
                    chat.sentAt = [dict valueForKey:@"created_at"];
                    [[RLMRealm defaultRealm] commitWriteTransaction];
                }
            }
        }
        
        if (application.applicationState == UIApplicationStateActive) {
            if ([controller isKindOfClass:[ChatsViewController class]]) {
                ChatsViewController *chats = (ChatsViewController*)controller;
                [chats viewDidAppear:YES];
            }
        } else if (application.applicationState == UIApplicationStateBackground) {
            if ([info valueForKey:@"data"]) {
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TestViewController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
            self.window.rootViewController = tabBar;
                [self.window makeKeyAndVisible];
                [tabBar setSelectedIndex:1];
                UINavigationController *nav = [tabBar.viewControllers objectAtIndex:1];
                ChatsViewController *chats = nav.viewControllers.firstObject;
                NSError *jsonError;
                NSData *objectData = [[info valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&jsonError];
                NSNumber *friendId = [[[[json valueForKey:@"data"] valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"id"];
                RLMChat *findChat = [[RLMChat alloc] init];
                NSArray *obj = [findChat findFromFriendId:friendId];
                RLMChat *chat = obj[0];
                chats.selectedChat = chat;
                RLMFriend *friend = [[RLMFriend alloc] init];
                RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:chatObject.friendId];
                chats.selectedFriend = findFriend;
//                [chats performSegueWithIdentifier:@"cc" sender:self];
//                [chats viewDidLoad];
            }
        } else if (application.applicationState == UIApplicationStateInactive) {
            if ([info valueForKey:@"data"]) {
                self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                TestViewController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
                self.window.rootViewController = tabBar;
                [self.window makeKeyAndVisible];
                [tabBar setSelectedIndex:1];
                UINavigationController *nav = [tabBar.viewControllers objectAtIndex:1];
                ChatsViewController *chats = nav.viewControllers.firstObject;
                NSError *jsonError;
                NSData *objectData = [[info valueForKey:@"data"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&jsonError];
                NSNumber *friendId = [[[[json valueForKey:@"data"] valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"id"];
                RLMChat *findChat = [[RLMChat alloc] init];
                NSArray *obj = [findChat findFromFriendId:friendId];
                RLMChat *chat = obj[0];
//                chats.selectedChat = chat;
                chats.selectedChat = chat;
                RLMFriend *friend = [[RLMFriend alloc] init];
                RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:chatObject.friendId];
                chats.selectedFriend = findFriend;
                [chats performSegueWithIdentifier:@"cc" sender:self];
//                [chats viewDidLoad];
            }
        }
    } else if ([info valueForKey:@"type"] == nil) {
        NSError *jsonError;
        NSData *objectData = [[info valueForKey:@"friend"] dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        RLMFriend *friend = [[RLMFriend alloc] initWithServerResponse:[json valueForKey:@"data"]];
        RLMFriend *findFriend = [friend findFriendFromArray:(NSArray*)[RLMFriend allObjects] chatId:friend.friendId];
        if (findFriend == nil) {
            [[RLMRealm defaultRealm] beginWriteTransaction];
            [[RLMRealm defaultRealm] addObject:friend];
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
        
        if (application.applicationState == UIApplicationStateActive) {
            if ([controller isKindOfClass:[ContactsViewController class]]) {
                ContactsViewController *contacts = (ContactsViewController*)controller;
                [contacts viewDidAppear:YES];
            }
        } else if (application.applicationState == UIApplicationStateBackground) {
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TestViewController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
            self.window.rootViewController = tabBar;
            [self.window makeKeyAndVisible];
            [tabBar setSelectedIndex:0];
            UINavigationController *nav = [tabBar.viewControllers objectAtIndex:0];
            ContactsViewController *contacts = nav.viewControllers.firstObject;
            contacts.selectedFriend = friend;
//            [contacts performSegueWithIdentifier:@"cc" sender:self];
        } else if (application.applicationState == UIApplicationStateInactive) {
            self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            TestViewController *tabBar = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
            self.window.rootViewController = tabBar;
            [self.window makeKeyAndVisible];
            [tabBar setSelectedIndex:0];
            UINavigationController *nav = [tabBar.viewControllers objectAtIndex:0];
            ContactsViewController *contacts = nav.viewControllers.firstObject;
            contacts.selectedFriend = friend;
//            [contacts performSegueWithIdentifier:@"cc" sender:self];
        }
        
    }
}

- (void)createChatFromServer:(NSDictionary*)success fromChat:(RLMChat*)chat {
    RLMChat *saveChat = [self parseChatWithDict:success];
    chat = saveChat;
    self.chat = chat;
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] addObject:saveChat];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (NSString*)convert:(NSString*)date fromNeedDateFormat:(NSString*)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [NSDate new];
    if (date == nil) {
        dateFromString = [NSDate date];
    } else {
        dateFromString = [dateFormatter dateFromString:date];
    }
    
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger sec = [tz secondsFromGMTForDate: dateFromString];
    dateFromString = [NSDate dateWithTimeInterval: sec sinceDate: dateFromString];
    
    if ([format isEqualToString:@"EEEE, MMM d, yyyy"]) {
        [dateFormatter setDateFormat:@"EEEE, MMM d, yyyy"];
    } else if ([format isEqualToString:@"HH:mm:ss"]) {
        [dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    
    return [dateFormatter stringFromDate:dateFromString];
}

- (RLMChat*)parseChatWithDict:(NSDictionary*)success {
    success = [success dictionaryRemovingNSNullValues];
    RLMChat *chat = [[RLMChat alloc] init];
    chat.chatKey = [NSString stringWithFormat:@"%@",[[success valueForKey:@"data"] valueForKey:@"id"]];
    chat.chatId = [[success valueForKey:@"data"] valueForKey:@"id"];
    NSDictionary *membFriend = [[[success valueForKey:@"data"] valueForKey:@"last_message"] valueForKey:@"data"];
    NSDictionary *sender = [[membFriend valueForKey:@"sender"] valueForKey:@"data"];
    chat.name = [NSString stringWithFormat:@"%@ %@",[sender valueForKey:@"first_name"],[sender valueForKey:@"last_name"]];
    chat.avatar = [sender valueForKey:@"avatar"];
    chat.nickName = [sender valueForKey:@"nickname"];
    chat.createdAt = [sender valueForKey:@"created_at"];
    chat.updatedAt = [sender valueForKey:@"updated_at"];
    chat.sentAt = [sender valueForKey:@"last_login"];
    
    if ([[sender valueForKey:@"avatar"] isEqualToString:@""]) {
        NSArray *gradientOneArray = [sender valueForKey:@"gradient"][0];
        NSArray *gradientTwoArray = [sender valueForKey:@"gradient"][1];
        
        for (NSNumber *i in gradientOneArray) {
            RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
            [chat.gradient addObject:value];
        }
        for (NSNumber *i in gradientTwoArray) {
            RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
            [chat.gradient addObject:value];
        }
    }
    
    
    return chat;
}


- (void)getChatHistoryFromChatId:(NSNumber*)chatId fromResponse:(NSDictionary*)json {
    RLMChat *chat = [RLMChat objectForPrimaryKey: [NSString stringWithFormat:@"%@",chatId]];
    if (chat == nil) {
        NSDictionary *resp = [[json valueForKey:@"data"] valueForKey:@"conversation"];
        RLMUser *user = [RLMUser currentUser];
        RLMChat *newChat = [self parseChatWithDict:resp];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [user.chat addObject:newChat];
        [[RLMRealm defaultRealm] commitWriteTransaction];
        chat = newChat;
    }
    [[RequestManager sharedManager] getChatsHistoryFromChatId:chatId withPage:@(1) pageOffset:@(30) withSuccessBlock:^(NSDictionary *success) {
        NSArray *messagesArray = [success valueForKey:@"data"];
        messagesArray = [[messagesArray reverseObjectEnumerator] allObjects];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [chat.message removeAllObjects];
        [[RLMRealm defaultRealm] commitWriteTransaction];
        for (NSDictionary *dict in messagesArray) {
            RLMMessage *message = [[RLMMessage alloc] init];
            if ([[[[dict valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"phone"] isEqualToString:self.user.telephone]) {
                message.role = @(1);
                message.time = [dict valueForKey:@"created_at"];
                message.message = [dict valueForKey:@"body"];
                message.mId = [dict valueForKey:@"id"];
                message.isSend = YES;
                [[RLMRealm defaultRealm] beginWriteTransaction];
                [chat.message addObject:message];
                [[RLMRealm defaultRealm] commitWriteTransaction];
            } else {
                message.role = @(2);
                message.time = [dict valueForKey:@"created_at"];
                message.message = [dict valueForKey:@"body"];
                message.mId = [dict valueForKey:@"id"];
                message.isSend = YES;
                [[RLMRealm defaultRealm] beginWriteTransaction];
                [chat.message addObject:message];
                [[RLMRealm defaultRealm] commitWriteTransaction];
            }
        }
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TestViewController *test  = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        test.chat = chat;
        self.chat = chat;
        [self.window.rootViewController performSegueWithIdentifier:@"TabBar" sender:nil];

    } onFalureBlock:^(NSInteger statusCode) {

        
    }];
}

- (void)userNotificationCenter:(UNUserNotificationCenter* )center willPresentNotification:(UNNotification* )notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    NSLog(@"");
    completionHandler(UNNotificationPresentationOptionAlert);
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    // For iOS 10 display notification (sent via APNS)
    [UNUserNotificationCenter currentNotificationCenter].delegate = self;
    // For iOS 10 data message (sent via FCM)
    [FIRMessaging messaging].delegate = self;
    
    [application registerForRemoteNotifications];
    
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:0];
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:2];
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:1];

}

// With "FirebaseAppDelegateProxyEnabled": NO

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    
    if (![self.user.token isEqualToString:@""]) {
        [[RequestManager sharedManager] updateUserFCMToken:fcmToken withSuccessBlock:^(NSDictionary *success) {
            
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    }
   
//    [[RequestManager sharedManager] registerNotificationWithFCMToken:fcmToken successBlock:^(NSDictionary *success) {
//
//    } onFalureBlock:^(NSInteger statusCode) {
//
//    }];
    
    NSLog(@"FCM registration token: %@", fcmToken);
    
    // TODO: If necessary send token to application server.
}

- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    [self connectToFcm];
    
    // TODO: If necessary send token to application server.
}

- (void)connectToFcm {
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] shouldEstablishDirectChannel];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM. FCM token - %@", [[FIRInstanceID instanceID] token] );
        }
    }];
}

- (BOOL)checkUserLogin {
    if ([RLMUser allObjects].count == 0) {
        return NO;
    } else {
        self.user = [RLMUser currentUser];
        if ([self.user.isRegister isEqualToNumber:@(1)]) {
            
            NSString *fcmToken = [FIRMessaging messaging].FCMToken;
            NSLog(@"FCM registration token: %@", fcmToken);
            
            if (fcmToken) {
                [[RequestManager sharedManager] updateUserFCMToken:fcmToken withSuccessBlock:^(NSDictionary *success) {
                    
                } onFalureBlock:^(NSInteger statusCode) {
                    
                }];
            }
            return YES;
        }
        else {
            return NO;
        }
    }
}

- (void)checkFirstScreen {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if ([self checkUserLogin]) {
        UITabBarController *rootViewController;
        //TabBar
        //MMTabBarController
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"TabBar"];
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
        [rootViewController setSelectedIndex:1];
    } else {
        UIViewController *rootViewController;
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"RootNavigationController"];
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
        // Clean up any unfinished task business by marking where you
        // stopped or ending the task outright.
        [application endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    // Start the long-running task and return immediately.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if ([RLMUser allObjects].count != 0) {
            [[RequestManager sharedManager] setDisconnectFromServerWithSuccessBlock:^(NSDictionary *success) {
                [application endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;
                
            } onFalureBlock:^(NSInteger statusCode) {
                
            }];
        }
    });
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    UIViewController *controller = [UIViewController currentViewController];
    if ([controller isKindOfClass:[ChatsViewController class]]) {
        ChatsViewController *chats = (ChatsViewController*)controller;
        [chats viewDidAppear:YES];
    }
    
    if ([controller isKindOfClass:[ContactsViewController class]]) {
        ContactsViewController *contacts = (ContactsViewController*)controller;
        [contacts viewDidAppear:YES];
    }
}


- (void)applicationWillTerminate:(UIApplication *)application {
        if ([RLMUser allObjects].count != 0) {
            [[RequestManager sharedManager] setDisconnectFromServerWithSuccessBlock:^(NSDictionary *success) {
                [application endBackgroundTask:bgTask];
                bgTask = UIBackgroundTaskInvalid;

            } onFalureBlock:^(NSInteger statusCode) {
                
            }];
        }
}

@end
