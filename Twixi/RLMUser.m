//
//  RLMUser.m
//  Twixi
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RLMUser.h"
#import "RLMStudy.h"
#import "NSDictionary+DictCategory.h"

@implementation RLMUser

+ (RLMUser *)currentUser {
    return [[self allObjects] objectAtIndex:0];
}

+ (void)updateUser:(RLMUser*)newUser {
    RLMUser *user = [RLMUser currentUser];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    user.userId = newUser.userId;
//    [RLMUser createOrUpdateInRealm:realm withValue:updateUser];
    user.telephone = newUser.telephone;
    user.nickName = newUser.nickName;
//    user.token = newUser.token;
    user.online = newUser.online;
    user.lastOnline = newUser.lastOnline;
    user.avatar = newUser.avatar;
    user.chat = newUser.chat;
    user.googleId = newUser.googleId;
    user.facebookId = newUser.facebookId;
    user.deviceToken = newUser.deviceToken;
    user.name = newUser.name;
    user.lastName = newUser.lastName;
    user.myLangCode = newUser.myLangCode;
    user.translateLangCode = newUser.translateLangCode;
    user.notification = newUser.notification;
    user.userTemp = newUser.userTemp;
    user.userBuySubscribe = newUser.userBuySubscribe;
    user.userEnableTranslate = newUser.userEnableTranslate;
    [realm commitWriteTransaction];
}

+ (void)updateAvatar:(NSString*)path {
    RLMUser *user = [RLMUser currentUser];
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    user.avatar = path;
    [realm commitWriteTransaction];
}

//+ (void)updateSubscribe:(NSString*)type {
//    RLMUser *user = [RLMUser currentUser];
//    RLMRealm *realm = [RLMRealm defaultRealm];
//    [realm beginWriteTransaction];
//    user.subType = type;
//    [realm commitWriteTransaction];
//}

+ (void)saveUser:(RLMUser *)user {
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:user];
    [realm commitWriteTransaction];
}

+ (void)saveFromServerResponse:(NSDictionary*)response {
    RLMUser *user = [[RLMUser alloc] init];
    user.token = [response valueForKey:@"token"];
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] addObject:user];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)updateFromServerResponse:(NSDictionary*)response {
    RLMUser *user = [[RLMUser alloc] init];
    RLMStudy *study = [[RLMStudy alloc] initWithServerResponse:[response valueForKey:@"data"]];
    NSDictionary *tempDict = [response dictionaryRemovingNSNullValues];
    user.userId = [[response valueForKey:@"data"] valueForKey:@"id"];
    user.name = [[tempDict valueForKey:@"data"] valueForKey:@"first_name"];
    user.lastName = [[tempDict valueForKey:@"data"] valueForKey:@"last_name"];
    user.nickName = [[tempDict valueForKey:@"data"] valueForKey:@"nickname"];
    user.avatar = [[tempDict valueForKey:@"data"] valueForKey:@"avatar"];
    user.telephone = [[tempDict valueForKey:@"data"] valueForKey:@"phone"];
    user.deviceToken = [[tempDict valueForKey:@"data"] valueForKey:@"device_token"];
    user.myLangCode = [[[[[[response valueForKey:@"data"] valueForKey:@"schema"] valueForKey:@"data"] valueForKey:@"targetLang"] valueForKey:@"data"] valueForKey:@"code"];
    user.translateLangCode = [[[[[[response valueForKey:@"data"] valueForKey:@"schema"] valueForKey:@"data"] valueForKey:@"sourceLang"] valueForKey:@"data"] valueForKey:@"code"];
    user.notification = @(1);
    NSString *temp = [[tempDict valueForKey:@"data"] valueForKey:@"study_rate"];
    NSNumber *hasPrime = [[tempDict valueForKey:@"data"] valueForKey:@"has_prime"];
    if ([hasPrime intValue] == 1) {
        user.userBuySubscribe = @(1);
        user.userEnableTranslate = @(1);
    } else {
        user.userBuySubscribe = @(0);
        user.userEnableTranslate = @(0);
    }
    
    if ([temp isEqualToString:@"low"]) {
        user.userTemp = @(0);
    } else if ([temp isEqualToString:@"medium"]) {
        user.userTemp = @(1);
    } else if ([temp isEqualToString:@"high"]) {
        user.userTemp = @(2);
    }
    
    [RLMUser updateUser:user];
    [RLMStudy updateCurrentStudy:study];
    NSLog(@"%@",response);
}

+ (void)updateFromRegistrationResponse:(NSDictionary*)response {
    NSDictionary *tempDict = [response dictionaryRemovingNSNullValues];
    RLMUser *user = [[RLMUser alloc] init];
    user.name = [[tempDict valueForKey:@"data"] valueForKey:@"first_name"];
    user.lastName = [[tempDict valueForKey:@"data"] valueForKey:@"last_name"];
    user.nickName = [[tempDict valueForKey:@"data"] valueForKey:@"nickname"];
    user.avatar = [[tempDict valueForKey:@"data"] valueForKey:@"avatar"];
    user.telephone = [[tempDict valueForKey:@"data"] valueForKey:@"phone"];
    user.deviceToken = [[tempDict valueForKey:@"data"] valueForKey:@"device_token"];
    [RLMUser updateUser:user];
}

+ (void)userIsRegisted {
    RLMUser *user = [RLMUser currentUser];
    [[RLMRealm defaultRealm] beginWriteTransaction];
    user.isRegister = @(1);
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)userIsNotRegisted {
    RLMUser *user = [RLMUser currentUser];
    [[RLMRealm defaultRealm] beginWriteTransaction];
    user.isRegister = @(0);
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)saveMyLangCode:(NSString*)myLang {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [RLMUser currentUser].myLangCode = myLang;
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)saveTrnLangCode:(NSString*)trnCode {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [RLMUser currentUser].translateLangCode = trnCode;
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

+ (void)updateName:(NSString*)name andLastname:(NSString*)lastName {
    RLMUser *currentUser = [RLMUser currentUser];
    [[RLMRealm defaultRealm] beginWriteTransaction];
    currentUser.name = name;
    currentUser.lastName = lastName;
    [[RLMRealm defaultRealm] commitWriteTransaction];
}



@end
