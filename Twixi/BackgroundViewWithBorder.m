//
//  BackgroundViewWithBorder.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "BackgroundViewWithBorder.h"

@implementation BackgroundViewWithBorder

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addBorder];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self addBorder];
    }
    return self;
}

- (void)addBorder {
    self.layer.borderWidth = 1;
//    self.layer.borderColor = [UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:0.5].CGColor;
    CGFloat opacity = 0.5f;
    self.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:opacity].CGColor;
    self.layer.cornerRadius = 3;
    self.clipsToBounds = YES;
}

- (void)centerLineView {
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height/2, self.frame.size.width, 1)];
    CGFloat opacity = 0.5f;
    lineView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:opacity];
    [self addSubview:lineView];
}

@end
