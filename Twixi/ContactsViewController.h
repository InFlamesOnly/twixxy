//
//  ContactsViewController.h
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RLMChat.h"
#import "RLMFriend.h"

@interface ContactsViewController : UIViewController

@property (strong, nonatomic) RLMFriend *selectedFriend;
@property (strong, nonatomic) RLMChat *selectedChat;

@end
