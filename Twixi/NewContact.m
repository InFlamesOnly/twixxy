//
//  NewContact.m
//  Twixi
//
//  Created by macOS on 27.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "NewContact.h"
#import "CustomePlaseholderTextField.h"
#import "RLMFriend.h"
#import "RequestManager.h"
#import "TelephoneNumberValidator.h"
#import "NSString+PhoneNumber.h"

@interface NewContact ()

@property (weak, nonatomic) IBOutlet CustomePlaseholderTextField *nameTextField;
@property (weak, nonatomic) IBOutlet CustomePlaseholderTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet CustomePlaseholderTextField *telephoneTextField;


@end

@implementation NewContact

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.nameTextField setPlaceholderText:@"First Name"];
    [self.lastNameTextField setPlaceholderText:@"Last Name"];
    [self.telephoneTextField setPlaceholderText:@"Telephone Number"];

}

-(void)viewDidAppear:(BOOL)animated {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)findUserAndAdd:(id)sender {
    NSString *phoneNumber = [NSString removeAllCharacteFromValidatePhoneNumber:self.telephoneTextField.text];
//    if (phoneNumber.length == 12) {
//        [[RequestManager sharedManager] addUserFromTelephoneNumber:phoneNumber succesBlock:^(NSDictionary *success) {
//            if ([[success valueForKey:@"code"] integerValue] == 0) {
//                RLMFriend *friend = [[RLMFriend alloc] initWithServerResponse:[success valueForKey:@"data"]];
//                RLMRealm *realm = [RLMRealm defaultRealm];
//                [realm beginWriteTransaction];
//                [realm addObject:friend];
//                [realm commitWriteTransaction];
//                [self.telephoneTextField resignFirstResponder];
//                [self showAllertViewWithTittleWithAction:@"" andMessage:[success valueForKey:@"success"]];
//            } else {
//                [self showAllertViewWithTittleWithAction:@"Error" andMessage:[success valueForKey:@"error"]];
//            }
//            
//        } onFalureBlock:^(NSInteger statusCode) {
//            
//        }];
//    } else {
//        [self showAllertViewWithTittle:@"Ошибка" andMessage:@"Укажите валидный номер телефона!"];
//    }
}

- (IBAction)back:(id)sender {
    NSLog(@"DISMISS");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.telephoneTextField) {
        if (textField.text.length == 0) {
            textField.text = @"+38 ";
        }
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.telephoneTextField) {
        if (range.location < 19) {
            return [TelephoneNumberValidator formattedTextField:textField shouldChangeCharactersInRange:range replacementString:string];
        } else {
            return NO;
        }
    }
    else {
        return YES;
    }
}

- (void)showAllertViewWithTittleWithAction:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadTable" object:self];
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}

@end
