//
//  ViewWIthShadow.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ViewWIthShadow.h"

@implementation ViewWIthShadow

- (instancetype)init {
    self = [super init];
    if (self) {
        [self roundeWithWhiteLayer];
        [self addShadowToView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self roundeWithWhiteLayer];
        [self addShadowToView];
    }
    return self;
}

-(void)roundeWithWhiteLayer {
    self.layer.cornerRadius = self.bounds.size.width/2;
    self.layer.borderWidth = 4.0;
    self.layer.borderColor=[[UIColor whiteColor] CGColor];
}

-(void)addShadowToView {
    self.layer.shadowColor = [UIColor colorWithRed:209/255 green:220/255 blue:228/255 alpha:0.2].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 1);
    self.layer.shadowOpacity = 1;
    self.layer.shadowRadius = 4;
}

@end
