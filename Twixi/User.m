//
//  User.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initWithName:(NSString*)name seenAs:(NSString*)seenAs andAvatar:(UIImage*)avatar {
    self = [super init];
    if (self) {
        self.name = name;
        self.seenAs = seenAs;
//        self.avatar = avatar;
    }
    return self;
}

- (id)initWithName:(NSString*)name seenAs:(NSString*)seenAs avatar:(UIImage*)avatar andRole:(NSNumber*) role timeSms:(NSString*)timeSms{
    self = [super init];
    if (self) {
        self.name = name;
        self.seenAs = seenAs;
//        self.avatar = avatar;
        self.role = role;
        self.timeSms = timeSms;
    }
    return self;
}

+ (NSArray*)testUserBase {
    NSMutableArray *array = [NSMutableArray new];
    User *user1 = [[User alloc] initWithName:@"User Name 1" seenAs:@"seen 1 hour ago" andAvatar:[UIImage imageNamed:@"user1"]];
    User *user2 = [[User alloc] initWithName:@"User Name 2" seenAs:@"seen 2 hour ago" andAvatar:[UIImage imageNamed:@"user2"]];
    User *user3 = [[User alloc] initWithName:@"User Name 3" seenAs:@"seen 3 hour ago" andAvatar:[UIImage imageNamed:@"user3"]];
    User *user4 = [[User alloc] initWithName:@"User Name 4" seenAs:@"seen 4 hour ago" andAvatar:[UIImage imageNamed:@"user4"]];
    User *user5 = [[User alloc] initWithName:@"User Name 5" seenAs:@"seen 5 hour ago" andAvatar:[UIImage imageNamed:@"user5"]];
    User *user6 = [[User alloc] initWithName:@"User Name 6" seenAs:@"seen 6 hour ago" andAvatar:[UIImage imageNamed:@"user6"]];
    User *user7 = [[User alloc] initWithName:@"User Name 7" seenAs:@"seen 7 hour ago" andAvatar:[UIImage imageNamed:@"user7"]];
    User *user8 = [[User alloc] initWithName:@"User Name 8" seenAs:@"seen 8 hour ago" andAvatar:[UIImage imageNamed:@"user8"]];
    User *user9 = [[User alloc] initWithName:@"User Name 9" seenAs:@"seen 9 hour ago" andAvatar:[UIImage imageNamed:@"user9"]];
    User *user10 = [[User alloc] initWithName:@"User Name 10" seenAs:@"seen 10 hour ago" andAvatar:[UIImage imageNamed:@"user10"]];
    
    [array addObject:user1];
    [array addObject:user2];
    [array addObject:user3];
    [array addObject:user4];
    [array addObject:user5];
    [array addObject:user6];
    [array addObject:user7];
    [array addObject:user8];
    [array addObject:user9];
    [array addObject:user10];
    
    return array;
}

+ (NSArray*)testUserBaseFromChat {
    NSMutableArray *array = [NSMutableArray new];
    User *user1 = [[User alloc] initWithName:@"Natasha" seenAs:@"I was seriously about ready to smash my  WORKS!" avatar:[UIImage imageNamed:@"user1"] andRole:[NSNumber numberWithInt:1] timeSms:@"11:05"];
    User *user2 = [[User alloc] initWithName:@"Maryana" seenAs:@"I was seriously about ready to smash my monitor because I could not gift adaptation of code that ACTUALLY WORKS!" avatar:[UIImage imageNamed:@"user2"] andRole:[NSNumber numberWithInt:2] timeSms:@"11:06"];
    User *user3 = [[User alloc] initWithName:@"Natasha" seenAs:@"I was seriously about " avatar:[UIImage imageNamed:@"user1"] andRole:[NSNumber numberWithInt:1] timeSms:@"11:07"];
    User *user4 = [[User alloc] initWithName:@"Maryana" seenAs:@"I was seriously aboutI was seriously about" avatar:[UIImage imageNamed:@"user2"] andRole:[NSNumber numberWithInt:2] timeSms:@"11:08"];
    User *user5 = [[User alloc] initWithName:@"Natasha" seenAs:@"I was seriously about" avatar:[UIImage imageNamed:@"user1"] andRole:[NSNumber numberWithInt:1] timeSms:@"11:09"];
    User *user6 = [[User alloc] initWithName:@"Maryana" seenAs:@"I was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutIsly aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriou was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously aboutaboutI was seriously about" avatar:[UIImage imageNamed:@"user2"] andRole:[NSNumber numberWithInt:2] timeSms:@"11:10"];
    User *user7 = [[User alloc] initWithName:@"Natasha" seenAs:@"I was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutI was seriously about" avatar:[UIImage imageNamed:@"user1"] andRole:[NSNumber numberWithInt:1] timeSms:@"11:11"];
    User *user8 = [[User alloc] initWithName:@"Maryana" seenAs:@"I was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutI was seriously about" avatar:[UIImage imageNamed:@"user2"] andRole:[NSNumber numberWithInt:2] timeSms:@"11:12"];
    User *user9 = [[User alloc] initWithName:@"Natasha" seenAs:@"I was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutI was seriously aboutI was seriously about" avatar:[UIImage imageNamed:@"user1"] andRole:[NSNumber numberWithInt:1] timeSms:@"11:50"];
    
    [array addObject:user1];
    [array addObject:user2];
    [array addObject:user3];
    [array addObject:user4];
    [array addObject:user5];
    [array addObject:user6];
    [array addObject:user7];
    [array addObject:user8];
    [array addObject:user9];
    
    return array;
}


@end
