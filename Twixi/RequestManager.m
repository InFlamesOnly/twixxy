//
//  RequestManager.m
//  Twixi
//
//  Created by macOS on 26.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "RequestManager.h"
#import <AFNetworking.h>
#import "NSDictionary+DictCategory.h"
#import "Contact.h"

#if !defined(StringOrEmpty)
#define StringOrEmpty(A)  ({ __typeof__(A) __a = (A); __a ? __a : @""; })
#endif


@import Firebase;

//http://chat.servey.design/user/social-login
static NSString *languagesList = @"http://chat.servey.design/translation/languages";
static NSString *getTranslations = @"http://chat.servey.design/translation/translate";
static NSString *restorePassword = @"http://chat.servey.design/user/forgot";
static NSString *connectUser = @"http://chat.servey.design/user/connect";
static NSString *disconnectUser = @"http://chat.servey.design/user/disconnect";
static NSString *socialLogin = @"http://chat.servey.design/user/social-login";
static NSString *searchUsersArray = @"http://chat.servey.design/user/contact-search?name=";
static NSString *notificationRegister = @"http://chat.servey.design/user/register_id";
static NSString *reqistration = @"http://chat.servey.design/user/register";
static NSString *autorisation = @"http://chat.servey.design/user/login";
static NSString *delete = @"http://chat.servey.design/user/delete";
static NSString *userList = @"http://chat.servey.design/user/list";
static NSString *addUser = @"http://chat.servey.design/user/contact-add";
static NSString *removeUser = @"http://chat.servey.design/user/contact-delete";
static NSString *changeAvatar = @"http://chat.servey.design/user/save-avatar";
static NSString *auth = @"http://chat.servey.design/user/auth";
static NSString *createChat = @"http://chat.servey.design/chat/create";
static NSString *removeChat = @"http://chat.servey.design/chat/delete";
static NSString *chatList = @"http://chat.servey.design/chat/list";
static NSString *sendMessage = @"http://chat.servey.design/message/send";
//static NSString *subscribe = @"http://chat.servey.design/subscriptions/subscribe";
//static NSString *subscribelist = @"http://chat.servey.design/subscriptions/list";

//subscriptions/list


static NSString *chatHistory = @"http://chat.servey.design/message/get";//TODO http://chat.servey.design/message/get/[type:init/new/old]/[chat_id]
//http://chat.servey.design/user/register_id






//NEW API SERVER
//http://1252003.servey.web.hosting-test.net/api
//static NSString *API = @"http://twizzy.servey.design/api/v1/";
static NSString *API = @"http://1252003.servey.web.hosting-test.net/api/v1/";
static NSString *newAuth = @"auth/request";
static NSString *newLogin = @"auth/login";
static NSString *newGetUserInfo = @"users";
static NSString *newContacts = @"contacts";
static NSString *newSearch = @"users/search";
static NSString *newChats = @"chats";
static NSString *newUploadImages = @"images";
static NSString *newSubscribe = @"users/subscriptions";
static NSString *newCancelSubcribe = @"users/subscriptions/cancel";
static NSString *newSubList = @"users/subscriptions";
static NSString *newLanguageList = @"languages";
static NSString *syncContact = @"contacts/book";
static NSString *disconnect = @"auth/disconnect";
static NSString *trainingSchemas = @"training-schemas";
static NSString *subscribe = @"users/subscriptions/subscribe";
static NSString *nicknameCheck = @"common/nickname/exists";
static NSString *dictionaryWords = @"dictionary/words";
static NSString *dictionaryLevels = @"dictionary/levels";
static NSString *newRemove = @"users";
static NSString *newTimezone = @"users/timezone";
static NSString *newBlacklist = @"blacklist/user";

///blacklist/user

static NSString *newSubscribeList = @"subscriptions";



///auth/login


@implementation RequestManager

+ (RequestManager*)sharedManager {
    static RequestManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [RequestManager new];
    });
    return manager;
}

- (AFHTTPSessionManager*)sessionManager {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    return manager;
}

- (AFHTTPSessionManager*)sessionManagerWithAccessTokken {
    RLMUser *user = [RLMUser currentUser];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    return manager;
}

- (void)registerNotificationWithFCMToken:(NSString*)fcmToken
                            successBlock:(void(^)(NSDictionary*success)) success
                           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"fcm_id" : fcmToken};
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:notificationRegister parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)subscribeWithStripeToken:(NSString*)stripeToken
                        stripeId:(NSString*)stripeId
                    successBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"credit_card_token" : stripeToken,
                                 @"subscription_id" : stripeId,
                                 };
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    NSString *requset = [NSString stringWithFormat:@"%@%@",API,subscribe];
    [manager POST:requset parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)loginWithFacebook:(NSString*)faceBookToken
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    if (faceBookToken == nil) {
        faceBookToken = @"123";
    }
    
    NSDictionary *parameters = @{@"facebook_id" : faceBookToken};
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:socialLogin parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)loginWithGoogle:(NSString*)googleToken
           successBlock:(void(^)(NSDictionary*success)) success
          onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"google_id" : googleToken};
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:socialLogin parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)restorePassword:(NSString*)email
           successBlock:(void(^)(NSDictionary*success)) success
          onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"email" : email};
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:restorePassword parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)getLanguageListWithSuccessBlock:(void(^)(NSDictionary*success)) success
                           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    
    [manager GET:languagesList parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

//- (void)getSubscribeListWithSuccessBlock:(void(^)(NSDictionary*success)) success
//           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
//    
//    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
//    
//    [manager GET:subscribelist parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        if (success) {
//            success(json);
//        }
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"%@", error);
//    }];
//}

- (void)getUsersFromName:(NSString*)name
            successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *request = [NSString stringWithFormat:@"%@%@",searchUsersArray,name];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)getTranslationFromSourseLang:(NSString*)sourceLng
                          targetLang:(NSString*)targetLng
                          phraseText:(NSString*)text
                        successBlock:(void(^)(NSDictionary*success)) success
                       onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *request = [NSString stringWithFormat:@"%@?source=%@&target=%@&phrase=%@",getTranslations,sourceLng,targetLng,text];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    NSString *encoded = [request stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [manager GET:encoded parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)getChatHistoryWithId:(NSNumber*)chatId
                     andType:(NSString*)type
                 successBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"message_id" : @(100000)};
    
    NSString *request = [NSString stringWithFormat:@"%@/new/%@",chatHistory,chatId];
    
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)reqisterUserWithLogin:(NSString*)login
                  googleToken:(NSString*)googleToken
                facebookToken:(NSString*)facebookToken
                        email:(NSString*)email
                         name:(NSString*)name
                     lastName:(NSString*)lastName
                     password:(NSString*)password
                 successBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *fullUserName = [NSString stringWithFormat:@"%@ %@",name, lastName];
    
    NSDictionary *parameters = [[NSDictionary alloc] init];
    
    if (googleToken != nil) {
        parameters = @{@"login" : login,
                       @"email" : email,
                       @"google_id" : googleToken,
                       @"name" : fullUserName,
                       @"password" : password};
    } else if (facebookToken != nil) {
        parameters = @{@"login" : login,
                       @"email" : email,
                       @"facebook_id" : facebookToken,
                       @"name" : fullUserName,
                       @"password" : password};
    } else {
        parameters = @{@"login" : login,
                       @"email" : email,
                       @"name" : fullUserName,
                       @"password" : password};
    }

    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:reqistration parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
                success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)checkUserNickName:(NSString*)nickname
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *encoded = [nickname stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *request = [NSString stringWithFormat:@"%@%@?nickname=%@",API,nicknameCheck,encoded];
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    //
    //    [manager POST:autorisation parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    //
    //
    //    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    //        NSLog(@"%@", error);
    //    }];
}

- (void)autorisationUserWithEmail:(NSString*)email
                                   password:(NSString*)password
                               successBlock:(void(^)(NSDictionary*success)) success
                              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"email" : email,
                                 @"password" : password};
    
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:autorisation parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
//    
//    [manager POST:autorisation parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"%@", error);
//    }];
}

- (void)getInfoUserSuccessBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:auth parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (error) {
            fail(error.code);
        }
        NSLog(@"%@", error);
    }];

    
}

- (void)removeUserFromId:(NSInteger)userId
            successBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail {

    NSDictionary *parameters = @{@"id" : @(userId)};
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:removeUser parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
}

- (void)getFriendsList:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    int timestamp = [[NSDate date] timeIntervalSince1970];
    
    NSDictionary *parameters = @{@"last_update" : @(timestamp)};
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:userList parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

- (void)addUserFromuserId:(NSNumber*)userId
                       succesBlock:(void(^)(NSDictionary*success)) success
                     onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"id" : userId};
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];

    [manager POST:addUser parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

- (void)creatPublicOrPrivateChat:(NSString*)privatoOrPublic
                      withUserId:(NSInteger)userId
                    orArrayUsers:(NSNumber*)arrayUsers
                     succesBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = [NSDictionary new];
    
    if ([privatoOrPublic isEqualToString:@"private"]) {
        NSArray *array = [NSArray arrayWithObjects:@(userId), nil];
        parameters = @{@"type" : @"private",
                       @"users" : array};
    } else {
        parameters = @{@"type" : @"public",
                       @"users" : arrayUsers};
    }
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:createChat parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

- (void)removeUserSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:delete parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

- (void)removePublicOrPrivateChatWithId:(NSInteger)chatId
                            succesBlock:(void(^)(NSDictionary*success)) success
                          onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"chat_id" : @(chatId)};
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:removeChat parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

- (void)getChatListFromUsersuccesBlock:(void(^)(NSDictionary*success)) success
                         onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:chatList parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

//TODO
- (void)sendMessage:(NSInteger)chatId
            message:(NSString*)message
        succesBlock:(void(^)(NSDictionary*success)) success
      onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSNumber *chatNumberId = [NSNumber numberWithInteger:chatId];
    NSDictionary *parameters = @{@"chat_id" : chatNumberId,
                                 @"message" : message,
                                 @"type" : @"text",
                                 @"my_id" : refreshedToken};
    
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:sendMessage parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        
        if (success) {
            success(json);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
    }];
    
}

- (void)sendServerToConnectUser {
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:connectUser parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}
- (void)sendServerToDissconectUsersuccesBlock:(void(^)(NSDictionary*success)) success
                                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    RLMUser *user = [RLMUser currentUser];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"accessToken"];
    
    [manager POST:disconnectUser parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

- (void)postAvatarFromBundlePath:(NSString*)path
                     succesBlock:(void(^)(NSDictionary*success)) success
                   onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    RLMUser *user = [RLMUser currentUser];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:changeAvatar parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:path] name:@"image" fileName:@"image.png" mimeType:@"image/png" error:nil];
        
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [request setValue:user.token forHTTPHeaderField:@"accessToken"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                          if (success) {
                              success(json);
                          }
                      }
                  }];
    
    [uploadTask resume];
    
}

//---------------------------------------------------------- NEW API METHODS

- (void)authUserWithPhone:(NSString*)phone
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *stringWithoutPlus = [phone
                                     stringByReplacingOccurrencesOfString:@"+" withString:@""];
    NSDictionary *parameters = @{@"phone" : stringWithoutPlus};
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newAuth];
    AFHTTPSessionManager *manager = [self sessionManager];
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 202) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getContactListSuccessBlock:(void(^)(NSDictionary*success)) success
                     onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newContacts];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getDictListSuccessBlock:(void(^)(NSDictionary*success)) success
                    onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@dictionary/level/%@/words",API, dictionaryWords];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getDictListWithLevel:(NSString*)level
                successBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@dictionary/level/%@/words",API,level];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getLevels:(void(^)(NSDictionary*success)) success
    onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, dictionaryLevels];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getSubscribeList:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newSubscribeList];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)buySubscribe:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newSubscribeList];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)addContactFromId:(NSNumber*)userId
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{@"user_id" : userId};
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newContacts];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)deleteContactFromId:(NSNumber*)userId
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{@"user_id" : userId};
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newContacts];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager DELETE:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 204) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getUserSuccessBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newGetUserInfo];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getUserSearch:(NSString*)string
     withSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *encoded = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *request = [NSString stringWithFormat:@"%@%@?query=%@",API, newSearch,encoded];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)updateUserName:(NSString*)name
              lastName:(NSString*)lastName
              nickName:(NSString*)nickName
                avatar:(NSString*)avatar
      withSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    RLMUser *currentUser = [RLMUser currentUser];
//    name = [name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    lastName = [lastName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    nickName = [nickName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSDictionary *parameters = @{@"first_name" : name,
                                 @"last_name" : lastName,
                                 @"nickname" : nickName,
                                 @"avatar" : avatar
                                 };
    if ([avatar isEqualToString:@""]) {
        parameters = @{@"first_name" : name,
                                     @"last_name" : lastName,
                                     @"nickname" : nickName
                                     };
    }
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newGetUserInfo];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        if (statusCode == 500) {
           [self showErrorFromErrorCode:statusCode withTittle:@"Никнейм должен иметь только латинские символы"];
        } else {
            [self showErrorFromErrorCode:statusCode withTittle:message];
        }
        
        fail(statusCode);
        NSLog(@"%@",error);
        
    }];
}

- (void)updateStadyRate:(int)rate
      withSuccessBlock:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    RLMUser *currentUser = [RLMUser currentUser];
    
    NSString *stringRate = @"";
    if (rate == 0) {
        stringRate = @"low";
    } else if (rate == 1) {
        stringRate = @"medium";
    } else if (rate == 2) {
        stringRate = @"high";
    }
    
    NSDictionary *parameters = @{@"study_rate" : stringRate};

    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newGetUserInfo];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        fail(statusCode);
        NSLog(@"%@",error);
        
    }];
}

- (void)updateUserFCMToken:(NSString*)token
      withSuccessBlock:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{
                                 @"device_token" : token,
                                 @"device_platform" : @"IOS"
                                 };
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newGetUserInfo];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
//        NSInteger statusCode = [response statusCode];
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
//        if (statusCode == 200) {
//            success(json);
//        }
//        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
//        NSInteger statusCode = [response statusCode];
//        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
//        fail(statusCode);
//        NSLog(@"%@",error);
    }];
}

- (void)sendTimezoneWithSuccessBlock:(void(^)(NSDictionary*success)) success
       onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSDictionary *parameters = @{@"timezone" : [currentTimeZone abbreviationForDate:[NSDate date]]};
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newTimezone];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

    }];
}

- (void)addUserToBleckList:(NSNumber*)userId
          withSuccessBlcok:(void(^)(NSDictionary*success)) success
             onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"user_id" : userId};
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newBlacklist];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        fail(error.code);
    }];
}

- (void)removeUserToBleckList:(NSNumber*)userId
             withSuccessBlcok:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"user_id" : userId};
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newBlacklist];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager DELETE:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        fail(error.code);
    }];
}

- (void)updateCurrentLanguage:(NSString*)langCode
          withSuccessBlock:(void(^)(NSDictionary*success)) success
             onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{
                                 @"lang" : langCode
                                 };
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newGetUserInfo];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
        
    }];
}

- (void)updateSettingsLanguage:(NSString*)langCode
               andNotification:(NSNumber*)notification
              withSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *language = @{@"code" : @"langCode"};
    NSDictionary *parameters = @{
                                 @"notifications" : notification,
                                 @"language" : language
                                 };
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@users/settings",API];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
        
    }];
}

- (void)loginUserWithCode:(NSString*)code
             successBlock:(void(^)(NSDictionary*success)) success
            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"code" : code};
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newLogin];
    AFHTTPSessionManager *manager = [self sessionManager];
    
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
//            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getChatsWithPage:(NSNumber*)page
              pageOffset:(NSNumber*)pageOffset
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {

    NSString *request = [NSString stringWithFormat:@"%@%@?page=%@&limit=%@",API, newChats,page,pageOffset];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}
//////////////=============
- (void)removeMessageFromChat:(NSNumber*)chatId
                    messageId:(NSNumber*)messageId
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    ///chats/{id}/messages/{message}
    NSString *request = [NSString stringWithFormat:@"%@chats/%@/messages/%@",API,chatId, messageId];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager DELETE:request parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 204) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)changeMessageFromChat:(NSNumber*)chatId
                    messageId:(NSNumber*)messageId
                  fromMessage:(NSString*)message
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"message" : message};
    ///chats/{id}/messages/{message}
    NSString *request = [NSString stringWithFormat:@"%@chats/%@/messages/%@",API,chatId, messageId];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getLanguageSchemeWithSuccessBlock:(void(^)(NSDictionary*success)) success
                            onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API,trainingSchemas];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)removeChat:(NSNumber*)chatId
  withSuccessBlock:(void(^)(NSDictionary*success)) success
     onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@chats/%@",API,chatId];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager DELETE:request parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 204) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getChatsHistoryFromChatId:(NSNumber*)chatId
                         withPage:(NSNumber*)page
                     pageOffset:(NSNumber*)pageOffset
               withSuccessBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail {

    NSString *request = [NSString stringWithFormat:@"%@chats/%@/messages?limit=%@&page=%@",API, chatId,pageOffset,page];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)sendMessage:(NSString*)message
         fromChatId:(NSNumber*)chatId
   withSuccessBlock:(void(^)(NSDictionary*success)) success
      onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"message" : message};
    
    NSString *request = [NSString stringWithFormat:@"%@chats/%@/messages",API, chatId];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)createChatsWithUserId:(NSNumber*)userId
        withSuccessBlock:(void(^)(NSDictionary*success)) success
           onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{@"user_id" : userId};
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newChats];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}
- (void)removeChatsWithChatId:(NSNumber*)chatId
             withSuccessBlock:(void(^)(NSDictionary*success)) success
                onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@/%@",API, newChats,chatId];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager DELETE:request parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}
- (void)markChatsWithChatId:(NSNumber*)chatId
           withSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@/%@",API, newChats,chatId];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)subscribeWithCartToken:(NSString*)cartTokken
                andSubscribeId:(NSString*)subscribeId
           withSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"credit_card_token" : cartTokken,
                                 @"subscription_id" : subscribeId
                                 };
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newSubscribe];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)subscribeWithSubscribeId:(NSString*)subscribeId
              withSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSDictionary *parameters = @{@"subscription_id" : subscribeId};
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newSubscribe];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager POST:request parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)getSubscribeListWithSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newSubList];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}
- (void)getNewLanguageListWithSuccessBlock:(void(^)(NSDictionary*success)) success
                          onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newLanguageList];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)translateText:(NSString*)text
       withSourceLang:(NSString*)soutceLang
           toParsLang:(NSString*)parseLang
     withSuccessBlock:(void(^)(NSDictionary*success)) success
        onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *encoded = [text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *request = [NSString stringWithFormat:@"%@translation/translate?source=%@&target=%@&phrase=%@",API, soutceLang, parseLang, encoded];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager GET:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
//        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
    
}

- (void)sendContactListFromSync:(NSArray*)contactList
              withSuccessBlock:(void(^)(NSDictionary*success)) success
                 onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, syncContact];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];

    [manager POST:request parameters:contactList progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 202) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)deleteUserwWthSuccessBlock:(void(^)(NSDictionary*success)) success
              onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newRemove];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager DELETE:request parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 204) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (void)updateScheme:(NSNumber*)scheme
      withSuccessBlock:(void(^)(NSDictionary*success)) success
         onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSDictionary *parameters = @{@"training_schema_id" : scheme
                                 };
    parameters = [parameters dictionaryRemovingNSNullValues];
    NSString *request = [NSString stringWithFormat:@"%@%@",API, newGetUserInfo];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    [manager PATCH:request parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 200) {
            success(json);
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
        
    }];
}

- (void)setDisconnectFromServerWithSuccessBlock:(void(^)(NSDictionary*success)) success
                  onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *request = [NSString stringWithFormat:@"%@%@",API, disconnect];
    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    
    [manager POST:request parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
        if (statusCode == 204) {
            success(json);
            //            [self showAllertViewWithTittle:@"Succes" andMessage:@"Zaloginilsya"];
        }
        NSLog(@"%@",json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *) [task response];
        NSInteger statusCode = [response statusCode];
        NSString *message = [error localizedDescription];
        [self showErrorFromErrorCode:statusCode withTittle:message];
        fail(statusCode);
        NSLog(@"%@",error);
    }];
}

- (NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array
{
    id objectInstance;

    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
    for (objectInstance in array)
        [mutableDictionary addEntriesFromDictionary:objectInstance];

    return (NSDictionary *)mutableDictionary;
}

- (void)uploadImageFromBundlePath:(NSString*)path
                 withSuccessBlock:(void(^)(NSDictionary*success)) ok
                    onFalureBlock:(void(^)(NSInteger statusCode)) fail {
    NSString *paths = [NSString stringWithFormat:@"%@%@",API, newUploadImages];
//    AFHTTPSessionManager *manager = [self sessionManagerWithAccessTokken];
    
    RLMUser *user = [RLMUser currentUser];
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:paths parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:path] name:@"image" fileName:@"image.png" mimeType:@"image/png" error:nil];
        
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [request setValue:[NSString stringWithFormat:@"JWTAuthToken example - Bearer %@",user.token] forHTTPHeaderField:@"Authorization"];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //[progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                          if (ok) {
                              NSString *photoPath = [[json valueForKey:@"data"] valueForKey:@"url"];
//                              [self updateUserName:@"" lastName:@"" nickName:@"" avatar:[NSString stringWithFormat:@"http://twizzy.servey.design/%@",photoPath] withSuccessBlock:^(NSDictionary *success) {
//                                  ok(json);
//                              } onFalureBlock:^(NSInteger statusCode) {
//
//                              }];
                              [self updateUserName:user.name lastName:user.lastName nickName:user.nickName avatar:photoPath withSuccessBlock:^(NSDictionary *success) {
                                  ok(json);
                              } onFalureBlock:^(NSInteger statusCode) {
                                  
                              }];
                          }
                      }
                  }];
    
    [uploadTask resume];
}


//Mark as read chats

//- (NSString*)showErrorMessageFromStatusCode:(int)statusCode {
//    switch (statusCode) {
//        case 202:
//            return @"Confirmation code sent";
//            break;
//        case 202:
//            return @"Такого юзера нет"
//            break;
//        case 202:
//            return @"Такого юзера нет"
//            break;
//        default:
//            break;
//    }
//}

- (void)showErrorFromErrorCode:(NSInteger)statusCode withTittle:(NSString*)tittle {
    [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:tittle];
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    UIViewController *currentTopVC = [self currentTopViewController];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [currentTopVC presentViewController:alertController animated:YES completion:NULL];
}

- (UIViewController *)currentTopViewController
{
    UIViewController *topVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    while (topVC.presentedViewController)
    {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

@end
