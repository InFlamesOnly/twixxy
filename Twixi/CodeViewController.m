//
//  CodeViewController.m
//  Twixi
//
//  Created by macOS on 02.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "CodeViewController.h"
#import "UIAlertController+Blocks.h"
#import "RequestManager.h"
#import "RLMUser.h"
#import "RLMLanguage.h"
#import "RLMStudy.h"
#import "RLMScheme.h"
#import "RLMFriend.h"
#import "RLMChat.h"
#import "RLMSubscribe.h"
#import "MyContacts.h"


@import Firebase;

@interface CodeViewController () 

@property (weak, nonatomic) IBOutlet UILabel *telephoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;

@property (strong, nonatomic) NSMutableArray *contactsArray;

@end

@implementation CodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contactsArray = [[NSMutableArray alloc] init];
    self.telephoneLabel.text = self.telephone;
}



- (void)viewDidAppear:(BOOL)animated {
    [self contactScan];
}

- (void)enter {
    RLMUser *user = [RLMUser currentUser];
    if ([user.isRegister isEqualToNumber:@(1)]) {
        [self performSegueWithIdentifier:@"loginIsAccept" sender:self];
    } else {
        [self performSegueWithIdentifier:@"goToNewProfile" sender:self];
    }
    [self removeActivity];
}

- (void)contactScan {
    [[MyContacts sharedInstance] contactScanAndGetAllusers];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)next:(id)sender {
    [self startActivity];
    [self.codeTextField resignFirstResponder];
    if ([self checkValidateCode]) {
        [self loginUserWithCode:self.codeTextField.text successBlock:^(NSDictionary *success) {
            [self checkRegistration:success];
            [self getLangListSuccessBlock:^(NSDictionary *success) {
                [self getLangSchemeList];
            }];
            [self getPricesFromServer];
            [self getContactListSuccessBlock:^(NSDictionary *success) {
                [self getChatsListSuccessBlock:^(NSDictionary *success) {
                    [self getUserSuccessBlock:^(NSDictionary *success) {
                        [self updateFcmToken];
                        [self sendTimezone];
                        [self enter];
                    }];
                }];
            }];
        }];
    }
}

- (void)checkRegistration:(NSDictionary*)response {
    BOOL isRegiseter = [[response valueForKey:@"c-registered"] boolValue];
    if (isRegiseter) {
        [RLMUser userIsRegisted];
    } else {
        [RLMUser userIsNotRegisted];
    }
}

- (BOOL)checkValidateCode {
    if (self.codeTextField.text.length != 5) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Enter the correct code", nil)];
        [self removeActivity];
        return NO;
    } else {
        return YES;
    }
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

#pragma mark - requests
- (void)getLangListSuccessBlock:(void(^)(NSDictionary*success))successBlock {
    [[RequestManager sharedManager] getNewLanguageListWithSuccessBlock:^(NSDictionary *success) {
        [RLMLanguage saveFromServerResponse:success];
        successBlock(success);
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get language list"];
    }];
}

- (void)getLangSchemeList {
    [[RequestManager sharedManager] getLanguageSchemeWithSuccessBlock:^(NSDictionary *success) {
        [RLMScheme saveFromServerResponse:success];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get scheme list"];
    }];
}

- (void)loginUserWithCode:(NSString*)code successBlock:(void(^)(NSDictionary*success))successBlock {
    [[RequestManager sharedManager] loginUserWithCode:self.codeTextField.text successBlock:^(NSDictionary *success) {
        [RLMUser saveFromServerResponse:success];
        successBlock(success);
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error user login"];
    }];
    
}

- (void)getContactListSuccessBlock:(void(^)(NSDictionary*success))successBlock {
    [[RequestManager sharedManager] getContactListSuccessBlock:^(NSDictionary *success) {
        [RLMFriend saveFromServerResponse:success];
        successBlock(success);
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get contact list"];
    }];
}

- (void)getChatsListSuccessBlock:(void(^)(NSDictionary*success))successBlock {
    [[RequestManager sharedManager] getChatsWithPage:@(1) pageOffset:@(10000) withSuccessBlock:^(NSDictionary *success) {
        [RLMChat saveFromServerResponse:success];
        successBlock(success);
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get chat list"];
    }];
}

- (void)getUserSuccessBlock:(void(^)(NSDictionary*success))successBlock {
    [[RequestManager sharedManager] getUserSuccessBlock:^(NSDictionary *success) {
        [RLMUser updateFromServerResponse:success];
        successBlock(success);
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get user info"];
    }];
}

- (void)updateFcmToken {
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    if (fcmToken) {
        [[RequestManager sharedManager] updateUserFCMToken:fcmToken withSuccessBlock:nil onFalureBlock:nil];
    }
}

- (void)getPricesFromServer {
    [[RequestManager sharedManager] getSubscribeList:^(NSDictionary *success) {
        [RLMSubscribe saveFromServerResponse:success];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get subscribes"];
    }];
//    [[RequestManager sharedManager] getSubscribeListWithSuccessBlock:^(NSDictionary *success) {
//        [RLMSubscribe saveFromServerResponse:success];
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self removeActivity];
//        [self showAllertViewWithTittle:@"Error" andMessage:@"Error get subscribes"];
//    }];
}

- (IBAction)getCodeAgain:(id)sender {
    [self startActivity];
    [self.codeTextField resignFirstResponder];
    [self showAllertViewWithTittle:NSLocalizedString(@"Success", nil) andMessage:NSLocalizedString(@"Сode re-sent to your phone number", nil)];
    [[RequestManager sharedManager] authUserWithPhone:self.telephoneLabel.text successBlock:^(NSDictionary *success) {
        [self removeActivity];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

//
- (void)sendContactListFromSync {
    [[RequestManager sharedManager] sendContactListFromSync:self.contactsArray withSuccessBlock:^(NSDictionary *success) {
        
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (void)sendTimezone {
    [[RequestManager sharedManager] sendTimezoneWithSuccessBlock:nil onFalureBlock:nil];
}

@end
