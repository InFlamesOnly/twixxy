//
//  RLMStudy.m
//  Twixi
//
//  Created by macOS on 07.06.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "RLMStudy.h"

@implementation RLMStudy

- (id)initWithServerResponse:(NSDictionary*)response {
    self = [super init];
    if (self) {
        self.studyRate = [response valueForKey:@"study_rate"];
        self.level = [response valueForKey:@"level"];
        self.learnedWords = [response valueForKey:@"learned_words"];
        self.scoreMax = [[response valueForKey:@"score"] valueForKey:@"max"];
        self.scoreCurrent = [[response valueForKey:@"score"] valueForKey:@"current"];
    }
    return self;
}

+ (RLMStudy*)currentStudy {
    return [RLMStudy allObjects][0];
}

+ (void)updateCurrentStudy:(RLMStudy*)study {
    if ([RLMStudy allObjects].count == 1) {
        RLMStudy *currentStudy = [RLMStudy currentStudy];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        currentStudy = study;
        [[RLMRealm defaultRealm] commitWriteTransaction];
    } else {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [[RLMRealm defaultRealm] addObject:study];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

@end
