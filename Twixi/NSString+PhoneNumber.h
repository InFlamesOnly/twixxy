//
//  NSString+PhoneNumber.h
//  Pigulka
//
//  Created by macOS on 07.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PhoneNumber)

+ (NSString *) removeAllCharacteFromValidatePhoneNumber:(NSString *)phoneNumber;

@end
