//
//  GradientWithTapDissmissViewController.m
//  Twixi
//
//  Created by macOS on 06.12.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "GradientWithTapDissmissViewController.h"


@interface GradientWithTapDissmissViewController () <UIGestureRecognizerDelegate>

@end


@implementation GradientWithTapDissmissViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTapFromDissmiss];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)createTapFromDissmiss {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

@end
