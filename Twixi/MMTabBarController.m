//
//  LMTabbedController.m
//  Management
//
//  Created by Olga Dalton on 16/04/14.
//

#import "MMTabBarController.h"
#import "SwitcherView.h"
#import "ChatViewController.h"
#import "UIView+ShadowToButton.h"

@interface MMTabBarController ()

@property (weak, nonatomic) IBOutlet SwitcherView *switcher;
@property (weak, nonatomic) IBOutlet UIView *tabBarView;

@property (strong, nonatomic) ChatViewController *chatViewController;

@end

@implementation MMTabBarController
@synthesize currentViewController;
@synthesize placeholderView;
@synthesize tabBarButtons;

- (void) setSelectedIndex: (int) index
{
    if ([tabBarButtons count] <= index) return;
    
    NSArray *availableIdentifiers = @[@"FirstVcIdentifier",
                                      @"SecondVcIdentifier",
                                      @"ThirdVcIdentifier",
                                      @"FourViewController"];
    
    [self performSegueWithIdentifier: availableIdentifiers[index]
                              sender: tabBarButtons[index]];
}

- (void)hideTabBar:(NSNotification *)note {
    [self.tabBarView setHidden:YES];
    self.botConstraint.constant = self.botConstraint.constant - 49;
}

- (void)showTabBar:(NSNotification *)note {
    [self.tabBarView setHidden:NO];
    self.botConstraint.constant = self.botConstraint.constant + 49;
}

- (void)showSetting:(NSNotification *)note {
    [self performSegueWithIdentifier:@"ThirdVcIdentifier" sender:self];
}

-(void)viewDidLayoutSubviews {
//    self.tabBarView.layer.shadowRadius  = 2.0f;
//    self.tabBarView.layer.shadowColor   = [UIColor blackColor].CGColor;
//    self.tabBarView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
//    self.tabBarView.layer.shadowOpacity = 0.5f;
//    self.tabBarView.layer.masksToBounds = NO;
//    
//    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -0.7f, 0);
//    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.tabBarView.bounds, shadowInsets)];
//    self.tabBarView.layer.shadowPath    = shadowPath.CGPath;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self setSelectedImage];
    
    if([tabBarButtons count]) {
        
        [self performSegueWithIdentifier: @"FirstVcIdentifier"
                                  sender: tabBarButtons[0]];
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hideTabBar:)
                                                       name:@"hideTabBar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showTabBar:)
                                                 name:@"showTabBar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showSetting:)
                                                 name:@"showSetting" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {

    
}

- (void)viewDidAppear:(BOOL)animated {
//    UIButton *button = tabBarButtons[0];
//    CGPoint center = [button center];
//    [self.switcher initSwitcherWithX:center.x - 35/2 width:35 height:self.switcher.frame.size.height];
}

- (void)setSelectedImage {
    [tabBarButtons[0] setImage:[UIImage imageNamed:@"userActive"] forState:UIControlStateSelected];
    [tabBarButtons[1] setImage:[UIImage imageNamed:@"chatActive"] forState:UIControlStateSelected];
    [tabBarButtons[2] setImage:[UIImage imageNamed:@"settingActive"] forState:UIControlStateSelected];
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender {
    
    NSArray *availableIdentifiers = @[@"FirstVcIdentifier",
                                      @"SecondVcIdentifier",
                                      @"ThirdVcIdentifier",
                                      @"FourViewController"];
    
    if([availableIdentifiers containsObject: segue.identifier])
    {
        for (UIButton *btn in tabBarButtons)
        {
            if(sender != nil && ![btn isEqual: sender]) {
                [btn setSelected: NO];
            } else if(sender != nil) {
                [btn setSelected: YES];
                if (btn == tabBarButtons[0]) {
//                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                             bundle: nil];
//                    
//                    ChatViewController *controller = (ChatViewController*)[mainStoryboard
//                                                                           instantiateViewControllerWithIdentifier:
//                                                                           @"ChatViewController"];
//                    controller.delegate = self;
                }
//                [self.switcher moveToButton:btn withSpeed:0.3];
                
            }
        } 
    }
}

@end
