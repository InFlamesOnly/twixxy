//
//  Contact.h
//  Twixi
//
//  Created by macOS on 23.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *phone;

@end
