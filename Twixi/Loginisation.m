//
//  ViewController.m
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "Loginisation.h"
#import "WhiteTextFieldPlaseholder.h"
#import "ButtonWithRoundedBorder.h"
#import "BackgroundViewWithBorder.h"
#import "RequestManager.h"
#import "TelephoneNumberValidator.h"
#import "MMTabBarController.h"
#import "NSString+PhoneNumber.h"
#import <IQKeyboardManager.h>
#import "PCAngularActivityIndicatorView.h"
#import "UIView+ShadowToButton.h"
#import "NSString+EmailValidation.h"
#import <GoogleSignIn/GoogleSignIn.h>

@interface Loginisation ()<GIDSignInUIDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *loginWithFaceBookButton;
@property (weak, nonatomic) IBOutlet UITextField *backgrounTextFieldsView;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *googleButton;

@property (weak, nonatomic) IBOutlet UIButton *loginisationButton;

@property(strong, nonatomic) GIDSignInButton *signInButton;

@property (weak, nonatomic) IBOutlet UIView *gradiantView;
@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@property BOOL securityEntry;

@end

@implementation Loginisation

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.signInButton = [[GIDSignInButton alloc] initWithFrame:CGRectMake(30, 30, 100, 100)];
//    [self.view addSubview:self.signInButton];
//    [self.signInButton setHidden:YES];
//    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [self.loginButton setEnabled:NO];
    self.securityEntry = YES;
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [UIView addGreyGradientToButton:self.loginButton];
    [self addGradientToView];
}


-(void)viewDidLayoutSubviews {
    [UIView addShadowToButton:self.facebookButton];
    [UIView addShadowToButton:self.googleButton];
    [UIView addShadowToButton:self.loginButton];
    [UIView addGreyGradientToButton:self.loginButton];
    CALayer *layer = [self.loginButton.layer.sublayers firstObject];
    [layer removeFromSuperlayer];
//    [UIView addGradientToLoginButton:self.loginButton];
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [self authFromGoogle];
//    [self removeActivity];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//
    NSString *email = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;

    if (textField == self.emailTextField) {
       email = [self.emailTextField.text stringByReplacingCharactersInRange:range
                                                          withString:string];
    } else if (textField == self.passwordTextField) {
        password = [self.passwordTextField.text stringByReplacingCharactersInRange:range
                                                                  withString:string];
    }
    
    BOOL emailIsValid = [email isValidEmail];
    
    if (password.length == 0 || emailIsValid == NO) {
        CALayer *layer = [self.loginButton.layer.sublayers firstObject];
        if ([layer.name isEqualToString:@"gradient"] || [layer.name isEqualToString:@"grey"]) {
            NSLog(@"remove gradient");
            [layer removeFromSuperlayer];
            [UIView addGreyGradientToButton:self.loginButton];
            [self.loginButton setEnabled:NO];
        }
        
    } else {
        CALayer *layer = [self.loginButton.layer.sublayers firstObject];
        if ([layer.name isEqualToString:@"grey"]) {
            NSLog(@"remove grey");
            [layer removeFromSuperlayer];
            [self.loginButton setEnabled:YES];
            [UIView addGradientToLoginButton:self.loginButton];
        }
    }
    return YES;
}

//-(void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:YES];
//
//}

- (void)addGradientToView {
    UIColor *firstColor = [UIColor colorWithRed:92.0f/255.0f green:107.0f/255.0f blue:192.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:48.0f/255.0f green:63.0f/255.0f blue:159.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    [self.view.layer insertSublayer:gradient atIndex:0];
}

- (void)addActivityIndicator {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    [self.view addSubview:self.activityView];
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}

- (void)removeActivity {
    [self.activity removeFromSuperview];
    [self.activityView removeFromSuperview];
}

-(void)dismissKeyboard {
    [self.emailTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
//    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (IBAction)signGoogle:(id)sender {
    [self addActivityIndicator];
    [self authFromGoogle];
//    [self.signInButton sendActionsForControlEvents:UIControlEventTouchUpInside];

}

- (void)authFromGoogle {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *googleId = [defaults valueForKey:@"googleID"];
////    RLMUser *currentUser = [RLMUser currentUser];
//    if (googleId == nil) {
//        [self showAllertViewWithTittle:@"Error" andMessage:@"Not google"];
//        [self removeActivity];
//        return;
//    }
//    [[RequestManager sharedManager] loginWithGoogle:googleId successBlock:^(NSDictionary *success) {
//        if ([[success valueForKey:@"code"] integerValue] == 0) {
//            RLMUser *user = [[RLMUser alloc] init];
//            user.token = [success valueForKey:@"token"];
//            NSString *token = [success valueForKey:@"token"];
//            [RLMUser saveUser:user];
//            [[RequestManager sharedManager] getInfoUserSuccessBlock:^(NSDictionary *success) {
//                if ([[success valueForKey:@"code"] integerValue]== 0) {
//                    RLMUser *user = [[RLMUser alloc] init];
//                    user.translateLangCode = @"en";
//                    user.token = token;
//                    user.nickName = [success valueForKey:@"login"];
//                    user.online = [success valueForKey:@"online"];
//                    user.name = [success valueForKey:@"name"];
//                    user.lastOnline = [success valueForKey:@"last_online"];
//                    user.avatar = [NSString stringWithFormat:@"http://chat.servey.design/%@",[success valueForKey:@"avatar"]];
//                    [RLMUser updateUser:user];
//                    
//                    [self removeActivity];
//                    [self performSegueWithIdentifier:@"loginIsAccept" sender:self];
//                } else {
//                    [self removeActivity];
//                    [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//                }
//            } onFalureBlock:^(NSInteger statusCode) {
//                [self removeActivity];
//                [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//            }];
//        } else {
//            [self removeActivity];
////            [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//        }
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self removeActivity];
//        
//    }];
}

- (IBAction)signUpAction:(id)sender {

}
- (IBAction)signInAction:(id)sender {
    if (self.emailTextField.text.length == 0 && self.passwordTextField.text.length == 0) {
        [self showAllertViewWithTittle:@"Ошибка" andMessage:@"Заполните все поля!"];
    } else {
        [self autorisationUser];
    }
    
}
- (IBAction)disableSecuretyEntry:(id)sender {
    self.passwordTextField.secureTextEntry = !self.passwordTextField.secureTextEntry;
}

- (void) autorisationUser {
//    [self addActivityIndicator];
//    [[RLMRealm defaultRealm] beginWriteTransaction];
//    [[RLMRealm defaultRealm] deleteAllObjects];
//    [[RLMRealm defaultRealm] commitWriteTransaction];
//
//    [[RequestManager sharedManager] autorisationUserWithEmail:self.emailTextField.text password:self.passwordTextField.text successBlock:^(NSDictionary *success) {
//
//        if ([[success valueForKey:@"code"] integerValue] == 0) {
//            RLMUser *user = [[RLMUser alloc] init];
//            user.token = [success valueForKey:@"token"];
//            NSString *token = [success valueForKey:@"token"];
//            [RLMUser saveUser:user];
//            [[RequestManager sharedManager] getInfoUserSuccessBlock:^(NSDictionary *success) {
//                if ([[success valueForKey:@"code"] integerValue]== 0) {
//                    RLMUser *user = [[RLMUser alloc] init];
//                    user.token = token;
//                    user.translateLangCode = @"en";
////                    user.telephone = [NSString stringWithFormat:@"+%@",[success valueForKey:@"telephone"] ];
//                    user.online = [success valueForKey:@"online"];
//                    user.name = [success valueForKey:@"name"];
//                    user.nickName = [success valueForKey:@"login"];
//                    user.lastOnline = [success valueForKey:@"last_online"];
//                    user.avatar = [NSString stringWithFormat:@"http://chat.servey.design/%@",[success valueForKey:@"avatar"]];
//                    [RLMUser updateUser:user];
//                    [self performSegueWithIdentifier:@"loginIsAccept" sender:self];
////                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
////                    MMTabBarController *vc = [storyboard instantiateViewControllerWithIdentifier:@"MMTabBarController"];
////                    [self removeActivity];
////                    [self.navigationController pushViewController:vc animated:YES];
//                } else {
//                    [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//                }
//            } onFalureBlock:^(NSInteger statusCode) {
//                [self removeActivity];
//            }];
//
//        } else {
//            [self removeActivity];
//            [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//        }
//
//
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self removeActivity];
//    }];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)loginWithFacebookAction:(id)sender {
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    NSString *fb = [userDefaults objectForKey:@"facebookId"];
//    [[RequestManager sharedManager] loginWithFacebook:fb successBlock:^(NSDictionary *success) {
//        if ([[success valueForKey:@"code"] integerValue] == 0) {
//            RLMUser *user = [[RLMUser alloc] init];
//            user.token = [success valueForKey:@"token"];
//            NSString *token = [success valueForKey:@"token"];
//            [RLMUser saveUser:user];
//            [[RequestManager sharedManager] getInfoUserSuccessBlock:^(NSDictionary *success) {
//                if ([[success valueForKey:@"code"] integerValue]== 0) {
//                    RLMUser *user = [[RLMUser alloc] init];
//                    user.translateLangCode = @"en";
//                    user.token = token;
//                    user.online = [success valueForKey:@"online"];
//                    user.name = [success valueForKey:@"name"];
//                    user.nickName = [success valueForKey:@"login"];
//                    user.lastOnline = [success valueForKey:@"last_online"];
//                    user.avatar = [NSString stringWithFormat:@"http://chat.servey.design/%@",[success valueForKey:@"avatar"]];
//                    [RLMUser updateUser:user];
//
//                    [self removeActivity];
//                    [self performSegueWithIdentifier:@"loginIsAccept" sender:self];
//                } else {
//                    [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//                }
//            } onFalureBlock:^(NSInteger statusCode) {
//                [self removeActivity];
//            }];
//        } else {
//            [self removeActivity];
//            [self showAllertViewWithTittle:@"Error" andMessage:[success valueForKey:@"error"]];
//        }
//    } onFalureBlock:^(NSInteger statusCode) {
//        [self removeActivity];
//    }];

}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}


@end
