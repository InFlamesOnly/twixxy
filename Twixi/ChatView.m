//
//  ChatView.m
//  Twixi
//
//  Created by macOS on 27.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ChatView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation ChatView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self addBorder];
        
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        [self addBorder];
        
    }
    return self;
}

- (void)addBorder {
    //D1DDE4
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderWidth = 1;
    //    self.layer.borderColor = [UIColor colorWithRed:255.0f green:255.0f blue:255.0f alpha:0.5].CGColor;
//    CGFloat opacity = 0.8f;
    self.layer.borderColor = UIColorFromRGB(0xD1DDE4).CGColor;
    self.layer.cornerRadius = 3;
    self.clipsToBounds = YES;
}

@end
