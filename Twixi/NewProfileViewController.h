//
//  NewProfileViewController.h
//  Twixi
//
//  Created by macOS on 02.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientWithTapDissmissViewController.h"

@interface NewProfileViewController : GradientWithTapDissmissViewController

@end
