//
//  RLMUser.h
//  Twixi
//
//  Created by macOS on 27.07.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <Realm/Realm.h>
#import "RLMChat.h"

@interface RLMUser : RLMObject

@property NSNumber <RLMInt> *userId;
@property NSString *facebookId;
@property NSString *googleId;
@property NSString *name;
@property NSString *lastName;
@property NSString *nickName;
@property NSString *deviceToken;
@property NSString *token;
@property NSString *telephone;
@property NSString *online;
@property NSString *lastOnline;
@property NSString *avatar;
@property NSString *myLangCode;
@property NSString *subType;
@property NSString *translateLangCode;
@property NSNumber <RLMInt> *notification;
@property NSNumber <RLMInt> *userTemp;

@property NSNumber <RLMInt> *isRegister;

@property NSNumber <RLMInt> *userBuySubscribe;
@property NSNumber <RLMInt> *userEnableTranslate;

@property (nonatomic, strong, readwrite) RLMArray<RLMChat *>< RLMChat> *chat;

+ (RLMUser *)currentUser;
+ (void)updateUser:(RLMUser*)newUser;
+ (void)saveUser:(RLMUser *)user;
+ (void)updateAvatar:(NSString*)path;

+ (void)saveFromServerResponse:(NSDictionary*)response;
+ (void)updateFromServerResponse:(NSDictionary*)response;
+ (void)updateFromRegistrationResponse:(NSDictionary*)response;

+ (void)saveMyLangCode:(NSString*)myLang;
+ (void)saveTrnLangCode:(NSString*)trnCode;

+ (void)updateName:(NSString*)name andLastname:(NSString*)lastName;

+ (void)userIsRegisted;
+ (void)userIsNotRegisted;

@end
