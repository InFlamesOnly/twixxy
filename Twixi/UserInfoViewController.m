//
//  UserInfoViewController.m
//  Twixi
//
//  Created by macOS on 26.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "UserInfoViewController.h"
#import "UserInfoCell.h"
#import "NSString+TelegramNSDateString.h"
#import "RequestManager.h"
#import "UIAlertController+Blocks.h"

@interface UserInfoViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *navigationViewLabel;
@property (weak, nonatomic) IBOutlet UITableView *infoTableView;

@end

@implementation UserInfoViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.infoTableView reloadData];
    self.navigationViewLabel.text = NSLocalizedString(@"Information", nil);
}

- (void)muteButtonPressed {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UserInfoCell *cell = [self.infoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Enable", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        cell.value.text = NSLocalizedString(@"Default (Enabled)", nil);
         [self.infoTableView reloadData];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Mute for 2 days", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        cell.value.text = NSLocalizedString(@"Mute for 2 days", nil);
         [self.infoTableView reloadData];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Mute for 8 hours", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        cell.value.text = NSLocalizedString(@"Mute for 8 hours", nil);
         [self.infoTableView reloadData];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Mute for 1 hour", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        cell.value.text = NSLocalizedString(@"Mute for 1 hour", nil);
         [self.infoTableView reloadData];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Disable", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        cell.value.text = @"123";
         [self.infoTableView reloadData];
    }]];
    
    actionSheet.view.tintColor = [UIColor colorWithRed:50.0f/255.0f green:67.0f/255.0f blue:179.0f/255.0f alpha:1];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 5) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if (indexPath.row == 7) {
        [self muteButtonPressed];
    }
    if (indexPath.row == 8) {
        UserInfoCell *cell = [self.infoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0]];
        if ([self.selectedFriend.userIsBlocked isEqualToNumber:@(0)]) {
            [self blockUser];
            cell.value.text =  NSLocalizedString(@"Unblock user", nil);
        } else {
            cell.value.text =  NSLocalizedString(@"Block user", nil);
            [self removeUser];
        }
        [self.infoTableView reloadData];
    }
}

- (void)blockUser {
    [[RequestManager sharedManager] addUserToBleckList:self.selectedFriend.friendId withSuccessBlcok:^(NSDictionary *success) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        self.selectedFriend.userIsBlocked = @(1);
        [[RLMRealm defaultRealm] commitWriteTransaction];
        NSString *text = [NSString stringWithFormat:@"%@ %@",self.selectedFriend.name,NSLocalizedString(@"unlocked", nil)];
        [UIAlertController showAlertInViewController:self withTitle:@"" message:text cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (void)removeUser {
    [[RequestManager sharedManager] removeUserToBleckList:self.selectedFriend.friendId withSuccessBlcok:^(NSDictionary *success) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        self.selectedFriend.userIsBlocked = @(0);
        [[RLMRealm defaultRealm] commitWriteTransaction];
        NSString *text = [NSString stringWithFormat:@"%@ %@",self.selectedFriend.name,NSLocalizedString(@"is blocked", nil)];
        [UIAlertController showAlertInViewController:self withTitle:@"" message:text cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 4) {
        return 21;
    }
    else if (indexPath.row == 1) {
        return 104;
    }
    else if (indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 8) {
        return 41;
    }
    
    else if (indexPath.row == 7 || indexPath.row == 6 ) {
        return 0;
    }
    else
        return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserInfoCell *cell = [[UserInfoCell alloc] init];
    if (indexPath.row == 0) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell0" forIndexPath:indexPath];
    }
    if (indexPath.row == 1) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell1"];
        [cell.avatar setAvatar:self.selectedFriend.avatar orInitialsView:self.selectedFriend.name gradientColorsArray:(NSArray*)self.selectedFriend.gradient];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",self.selectedFriend.name, self.selectedFriend.lastName];
        NSString *time = [NSString convertResponseToCellsTimeString:self.selectedFriend.lastOnline];
        cell.lastSeen.text = time;
    }
    if (indexPath.row == 2) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell2"];
        cell.value.text = [NSString stringWithFormat:@"@%@",self.selectedFriend.nickName];
        cell.nameLabel.text = NSLocalizedString(@"Nickname", nil);//Никнейм
    }
    if (indexPath.row == 3) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell3"];
        cell.value.text = [NSString stringWithFormat:@"+%@",self.selectedFriend.phone];
        cell.nameLabel.text = NSLocalizedString(@"Phone number", nil);//Номер телефона
    }
    if (indexPath.row == 4) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell4"];
        
    }
    if (indexPath.row == 5) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell5"];
        cell.nameLabel.text = NSLocalizedString(@"Send Message", nil);//Отправить сообщение
        
    }
    if (indexPath.row == 6) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell6"];
        cell.nameLabel.text = NSLocalizedString(@"Share Contact", nil);//Отправить контакт
    }
    if (indexPath.row == 7) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell7"];
        cell.value.text = NSLocalizedString(@"Default (Enabled)", nil);
        cell.nameLabel.text = NSLocalizedString(@"Notifications", nil);//Увидомления
    }
    if (indexPath.row == 8) {
        cell = [self.infoTableView dequeueReusableCellWithIdentifier:@"UserInfoCell8"];
        if ([self.selectedFriend.userIsBlocked isEqualToNumber:@(0)]) {
            cell.value.text = NSLocalizedString(@"Block user", nil);
        } else {
            cell.value.text =  NSLocalizedString(@"Unblock user", nil);
        }
        cell.value.text = NSLocalizedString(@"Block user", nil);//Заблокировать пользователя
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}



@end
