//
//  NSString+EmailValidation.h
//  Twixi
//
//  Created by macOS on 03.03.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EmailValidation)

- (BOOL)isValidEmail;

@end
