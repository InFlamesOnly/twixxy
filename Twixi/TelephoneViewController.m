//
//  TelephoneViewController.m
//  Twixi
//
//  Created by macOS on 02.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "TelephoneViewController.h"
#import "RequestManager.h"
#import "CodeViewController.h"
#import "TelephoneCodeViewController.h"
#import "Code.h"
#import "CountryListDataSource.h"
#import "MyContacts.h"
#import "UIAlertController+Blocks.h"
#import "PoPView.h"
#import "IOSDeviceName.h"

@interface TelephoneViewController () <UITextFieldDelegate ,TelephoneCodeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *codeNameButton;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *countryCode;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) NSMutableArray *contactsArray;

@property (strong, nonatomic) PoPView *popView;

@end

@implementation TelephoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self contactScan];
    [self getCurrentTelephoneCode];
}

- (void)contactScan {
    [[MyContacts sharedInstance] contactScan];
}

- (IBAction)showPop:(id)sender {
    IOSDeviceName *name = [IOSDeviceName new];
    if ([name.deviceName isEqualToString:@"iPhone X"] || [name.deviceName isEqualToString:@"Simulator"]) {
        self.popView = [[PoPView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.view addSubview:self.popView];
        [self.popView openWithIphoneX:self.tabBarController.tabBar.frame.size.height];
    } else {
        self.popView = [[PoPView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.view addSubview:self.popView];
        [self.popView open];
    }
}

- (void)getCurrentTelephoneCode {
    CountryListDataSource *dataSource = [[CountryListDataSource alloc] init];
    NSDictionary *dic = [dataSource getCurrentLocationCode];
    
    if (dic != nil) {
        Code *code = [[Code alloc] initWithName:[dic valueForKey:@"name"] image:[dic valueForKey:@"image"] code:[dic valueForKey:@"dial_code"]];
        [self.codeNameButton setTitle:code.name forState:UIControlStateNormal];
        self.countryCode.text = code.code;
        [self.flagImageView setImage:[UIImage imageNamed:code.image]];
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)nextScreen:(id)sender {
    [self.phoneTextField resignFirstResponder];
    [self startActivity];
    if ([self checkValidatePhone]) {
        [self authUser];
    }
}

- (BOOL)checkValidatePhone {
    if (self.phoneTextField.text.length < 5) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Enter a valid phone number", nil)];
        [self removeActivity];
        return NO;
    } else {
        return YES;
    }
}

- (void)authUser {
    [[RequestManager sharedManager] authUserWithPhone:[NSString stringWithFormat:@"%@%@",self.countryCode.text,self.phoneTextField.text] successBlock:^(NSDictionary *success) {
        [self performSegueWithIdentifier:@"codeScreen" sender:self];
        [self removeActivity];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"codeScreen"]) {
        CodeViewController *vc = [segue destinationViewController];
        vc.telephone = [NSString stringWithFormat:@"%@%@",self.countryCode.text,self.phoneTextField.text];
    }
    if ([segue.identifier isEqualToString:@"codesVC"]) {
        TelephoneCodeViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

#pragma mark - TelephoneCodeViewControllerDelegate
- (void)getCode:(Code*)code {
    [self.codeNameButton setTitle:code.name forState:UIControlStateNormal];
    self.countryCode.text = code.code;
    [self.flagImageView setImage:[UIImage imageNamed:code.image]];
}

@end
