//
//  TermOfService.m
//  Twixi
//
//  Created by macOS on 22.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import "TermOfService.h"
#import "TOFCell.h"

@interface TermOfService ()

@property (weak, nonatomic) IBOutlet UILabel *navigationLabel;

@end

@implementation TermOfService

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationLabel.text = NSLocalizedString(@"Terms of Service", nil);
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
