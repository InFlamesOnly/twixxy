//
//  DictionaryViewController.m
//  Twixi
//
//  Created by macOS on 05.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "DictionaryViewController.h"
#import "CardView.h"
#import "PagedFlowView.h"


@interface DictionaryViewController () <PagedFlowViewDelegate, PagedFlowViewDataSource, CardViewDelegate>

@property (weak, nonatomic) IBOutlet PagedFlowView *pagedView;
@property(nonatomic, strong) NSArray *contentArray;

@end


@implementation DictionaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentArray = [CardView testCardViewsfromView:self.pagedView];
    [self configurePaged];
}

- (void)configurePaged {
    self.pagedView.delegate = self;
    self.pagedView.dataSource = self;
    self.pagedView.minimumPageAlpha = 1;
    self.pagedView.minimumPageScale = 1;
    self.pagedView.minimumPageScale = 0.97;
    self.pagedView.orientation = PagedFlowViewOrientationVertical;
}

- (void)tapToStart {
    [self performSegueWithIdentifier:@"showDetail" sender:self];
}

#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView;{
    return CGSizeMake(self.pagedView.frame.size.width - 32, self.pagedView.frame.size.height - 42);
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    NSLog(@"Scrolled to page # %ld", (long)index);
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    NSLog(@"Tapped on page # %ld", (long)index);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Datasource
//返回显示View的个数
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [self.contentArray count];
}

//返回给某列使用的View
- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    return [self configureCardView:flowView fromIndex:index];
}

- (CardView*)configureCardView:(PagedFlowView *)flowView fromIndex:(NSInteger)index {
    CardView *cardView = (CardView *)[flowView dequeueReusableCell];
    cardView = self.contentArray[index];
    cardView.layer.cornerRadius = 15;
    cardView.layer.masksToBounds = YES;
    cardView.delegate = self;
    return cardView;
}

- (IBAction)pageControlValueDidChange:(id)sender {
    UIPageControl *pageControl = sender;
    [self.pagedView scrollToPage:pageControl.currentPage];
}

@end
