//
//  SwitcherView.h
//  Twixi
//
//  Created by macOS on 15.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitcherView : UIView

@property (strong, nonatomic) UIView *switcher;

- (void)moveToButton:(UIButton*)button withSpeed:(double)speed;
- (void)initSwitcherWithX:(CGFloat)x width:(CGFloat)width height:(CGFloat)height;

@end
