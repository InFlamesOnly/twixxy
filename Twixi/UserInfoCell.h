//
//  UserInfoCell.h
//  Twixi
//
//  Created by macOS on 26.10.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarView.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastSeen;
@property (weak, nonatomic) IBOutlet AvatarView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *value;

@end

NS_ASSUME_NONNULL_END
