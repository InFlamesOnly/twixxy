//
//  UIViewController+CurrentViewController.h
//  Twixi
//
//  Created by macOS on 03.10.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (CurrentViewController)

+ (UIViewController*) currentViewController;

@end
