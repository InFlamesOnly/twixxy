//
//  SocialNetworkRegistration.m
//  Twixi
//
//  Created by macOS on 19.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "SocialNetworkRegistration.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "PCAngularActivityIndicatorView.h"
#import "SignUp.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "RLMUser.h"
#import "UIView+ShadowToButton.h"


//FBSDKLoginButtonDelegate
@interface SocialNetworkRegistration () <GIDSignInUIDelegate, FBSDKLoginButtonDelegate, GIDSignInDelegate>

@property(strong, nonatomic) GIDSignInButton *signInButton;

@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@property(weak, nonatomic) IBOutlet UIButton *facebookButton;
@property(weak, nonatomic) IBOutlet UIButton *gmailButton;
@property(weak, nonatomic) IBOutlet UIButton *registrationButton;

@property (strong, nonatomic) FBSDKLoginButton *loginButton;

//@property (nonatomic, strong) IBOutlet GIDSignInButton *signInButton;
//@property (nonatomic, strong) UITextView *output;
//@property (nonatomic, strong) GTLRGmailService *service;

@end

@implementation SocialNetworkRegistration

- (void)viewDidLoad {
    [super viewDidLoad];
    self.signInButton = [[GIDSignInButton alloc] initWithFrame:CGRectMake(30, 30, 100, 100)];
    [self.view addSubview:self.signInButton];
    [GIDSignIn sharedInstance].delegate = self;
    [self.signInButton setHidden:YES];
    [GIDSignIn sharedInstance].uiDelegate = self;
    [self facebookInit];
    [UIView addGreyGradientToButton:self.loginButton];
}

-(void)viewDidLayoutSubviews {
    [self addShadowToButton:self.facebookButton];
    [self addShadowToButton:self.gmailButton];
    [self addShadowToButton:self.registrationButton];
    [self addGradientToView];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:NO];

}

- (void)addShadowToButton:(UIButton*)button {
    button.layer.shadowRadius  = 2.0f;
    button.layer.shadowColor   = [UIColor blackColor].CGColor;
    button.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    button.layer.shadowOpacity = 0.5f;
    button.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -0.7f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(button.bounds, shadowInsets)];
    button.layer.shadowPath    = shadowPath.CGPath;
}

- (void)addGradientToView {
    UIColor *firstColor = [UIColor colorWithRed:254.0f/255.0f green:96.0f/255.0f blue:118.0f/255.0f alpha:1];
    UIColor *secondColor = [UIColor colorWithRed:255.0f/255.0f green:153.0f/255.0f blue:68.0f/255.0f alpha:1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = self.registrationButton.bounds;
    gradient.cornerRadius = 3;
    gradient.masksToBounds = YES;
    gradient.colors = @[(id)firstColor.CGColor, (id)secondColor.CGColor];
    
    [self.registrationButton.layer insertSublayer:gradient atIndex:0];
}

//#pragma mark -
//#pragma mark - Facebook delegate methods
- (void)facebookInit {
    if ([FBSDKAccessToken currentAccessToken]) {
        FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
        NSLog(@"%@",token);
    }
    self.loginButton = [[FBSDKLoginButton alloc] init];
    self.loginButton.readPermissions = @[@"public_profile", @"email"];
    self.loginButton.delegate = self;
    self.loginButton.hidden = YES;
}
- (void)loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
              error:(NSError *)error {
    if ([FBSDKAccessToken currentAccessToken]) {
        [self fetchProfile];
    }
}

- (void)fetchProfile {
    [self addActivityIndicator];
    NSNumberFormatter *numberFormater = [[NSNumberFormatter alloc] init];
    numberFormater.numberStyle = NSNumberFormatterDecimalStyle;
//    NSNumber *myNumber = [numberFormater numberFromString:[FBSDKAccessToken currentAccessToken].userID];
//    NSString *token = [FBSDKAccessToken currentAccessToken].tokenString;
    //  user_photos,email,user_birthday,user_likes

    //  NSDictionary *parameters = @{@"fields" : @"first_name, last_name, email"};
    NSDictionary *parameters = @{@"fields" : @"email, id, first_name, last_name"};
    FBSDKGraphRequest *fbRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters];
    [fbRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        [self removeActivity];
        self.email = [result valueForKey:@"email"];
        self.token = [result valueForKey:@"id"];
        self.name = [result valueForKey:@"first_name"];
        self.lastName = [result valueForKey:@"last_name"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:self.token forKey:@"facebookId"];
        [defaults synchronize];
//        RLMUser *user = [RLMUser currentUser];
//        RLMRealm *realm = [RLMRealm defaultRealm];
//        [realm beginWriteTransaction];
//        user.facebookId = self.token;
//        [realm commitWriteTransaction];
        [self performSegueWithIdentifier:@"facebookRegistration" sender:self];
    }];
}

- (void)getUserFromServer {

}


- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {

}

- (void)addActivityIndicator {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    [self.view addSubview:self.activityView];
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}

- (void)removeActivity {
    [self.activity removeFromSuperview];
    [self.activityView removeFromSuperview];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    
    self.name = givenName;
    self.lastName = familyName;
    self.email = email;
    self.token = user.userID;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:user.userID forKey:@"googleID"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"googleRegistration" sender:self];
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [self removeActivity];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)facebook:(id)sender {
    [self.loginButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (void) goToRegistration {
    [self performSegueWithIdentifier:@"googleRegistration" sender:self];
}

-(IBAction)gmail:(id)sender {
    [self addActivityIndicator];
    [self.signInButton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"googleRegistration"]){
        SignUp *vc = segue.destinationViewController;
        vc.name = self.name;
        vc.lastName = self.lastName;
        vc.token = self.token;
        vc.email = self.email;
    } else if ([segue.identifier isEqualToString:@"facebookRegistration"]) {
        SignUp *vc = segue.destinationViewController;
        vc.name = self.name;
        vc.lastName = self.lastName;
        vc.token = self.token;
        vc.email = self.email;
    }
    
    //facebookRegistration
}

@end
