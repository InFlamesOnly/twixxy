//
//  RLMStudy.h
//  Twixi
//
//  Created by macOS on 07.06.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Realm/Realm.h>

@interface RLMStudy : RLMObject

@property NSString *studyRate;
@property NSNumber <RLMInt> *level;

@property NSNumber <RLMInt> *learnedWords;
@property NSNumber <RLMInt> *scoreMax;
@property NSNumber <RLMInt> *scoreCurrent;

+ (RLMStudy*)currentStudy;
+ (void)updateCurrentStudy:(RLMStudy*)study;
- (id)initWithServerResponse:(NSDictionary*)response;


@end
