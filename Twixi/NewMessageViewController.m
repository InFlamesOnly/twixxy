//
//  NewMessageViewController.m
//  Twixi
//
//  Created by macOS on 22.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NewMessageViewController.h"
#import "RLMFriend.h"
#import "RequestManager.h"
#import "ChatViewController.h"
#import "ContactCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImageView+AvatarImageView.h"
#import "UIView+ShadowToButton.m"
#import "NSDictionary+DictCategory.h"
#import "Reachability.h"

@interface NewMessageViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UITableView *contactTableView;
@property (strong, nonatomic) NSArray *contactArray;
@property (strong, nonatomic) RLMFriend *selectedFriend;
@property (nonatomic) NSInteger chatId;
@property (strong, nonatomic) RLMChat *selectedChat;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;

@end

@implementation NewMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contactArray = (NSArray*)[RLMFriend allObjects];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    self.tittleLabel.text = NSLocalizedString(@"New message", nil);
}

- (IBAction)back:(id)sender {
    //    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewDidLayoutSubviews {
    [UIView addGradientToView:self.navigationView];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //    RLMFriend *friend = [self.usersArray objectAtIndex:[[self.contactsTableView indexPathForSelectedRow]row]];
    if ([segue.identifier isEqualToString:@"cc"]) {
        ChatViewController *vc = segue.destinationViewController;
//        UINavigationController *navController = [segue destinationViewController];
//        ChatViewController *vc = (ChatViewController *)([navController viewControllers][0]);
        vc.chat = self.selectedChat;
        vc.selectedFriend = self.selectedFriend;
        vc.chatId = self.chatId;
        
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RLMFriend *friend = [self.contactArray objectAtIndex:indexPath.row];
    self.selectedFriend = friend;
    RLMChat *chat = [[RLMChat alloc] init];
    NSArray *chats = [chat findFromFriendId:friend.friendId];
    if (chats.count != 0) {
        RLMChat *findChat = chats[0];
        self.selectedChat = findChat;
        [self performSegueWithIdentifier:@"cc" sender:self];
        self.contactTableView.userInteractionEnabled = YES;
    } else {
        [self performSegueWithIdentifier:@"cc" sender:self];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.contactArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactCell *cell = (ContactCell*)[self.contactTableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    RLMFriend *friend = [[RLMFriend alloc] init];
    friend = [self.contactArray objectAtIndex:indexPath.row];
    
    if ([friend.online intValue] == 1) {
        cell.onlineView.backgroundColor = [UIColor colorWithRed:80.0f/255.0f green:181.0f/255.0f blue:171.0f/255.0f alpha:1];
    } else {
        cell.onlineView.backgroundColor = [UIColor redColor];
    }
    cell.name.text = [NSString stringWithFormat:@"%@ %@",friend.name,friend.lastName];
    cell.seenAn.text = [NSString stringWithFormat:@"@%@",friend.nickName];
    
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", friend.name , friend.lastName];
    
    [cell.avatarView setAvatar:friend.avatar orInitialsView:fullName gradientColorsArray:(NSArray*)friend.gradient];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

@end
