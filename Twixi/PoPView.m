//
//  PoPView.m
//  Twixi
//
//  Created by macOS on 15.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

#import "PoPView.h"

@implementation PoPView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customeinit];
    }
    return self;
}

- (void)roundTopCorners {
    self.bcgView.layer.masksToBounds = YES;
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){10.0, 10.}].CGPath;
    
    self.bcgView.layer.mask = maskLayer;
}

- (void)customeinit {
    [[NSBundle mainBundle] loadNibNamed:@"PoPView" owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    self.scrollView.layer.masksToBounds = YES;
    self.scrollView.layer.cornerRadius = 13;
    self.bcgView.layer.masksToBounds = YES;
    self.bcgView.layer.cornerRadius = 13;
}

- (void)open {
    CGRect screen = [[UIScreen mainScreen] bounds];
    
    self.heightConstraint.constant = screen.size.height + 20;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    }];
}

- (void)openWithTabBar:(CGFloat)tabBarHeight {
    CGRect screen = [[UIScreen mainScreen] bounds];
    self.heightConstraint.constant = screen.size.height - 20 + tabBarHeight;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    }];
}

- (void)openWithIphoneX:(CGFloat)tabBarHeight {
    CGRect screen = [[UIScreen mainScreen] bounds];
    self.heightConstraint.constant = screen.size.height - 10;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    }];
}

- (IBAction)close {
    self.heightConstraint.constant = 0.0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.contentView layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void) closeView {
    [self close];
}


@end
