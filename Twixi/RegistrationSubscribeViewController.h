//
//  RegistrationSubscribeViewController.h
//  Twixi
//
//  Created by macOS on 07.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GradientViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegistrationSubscribeViewController : GradientViewController

@end

NS_ASSUME_NONNULL_END
