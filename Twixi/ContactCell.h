//
//  ContactCell.h
//  Twixi
//
//  Created by macOS on 14.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AvatarView.h"

@interface ContactCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UIImageView *plusImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topNameConstr;
//@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIView *onlineView;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *seenAn;
//@property (weak, nonatomic) IBOutlet UILabel *initials;

@end
