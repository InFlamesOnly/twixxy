//
//  MessageActionSheet.m
//  Twixi
//
//  Created by macOS on 16.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "MessageActionSheet.h"

@implementation MessageActionSheet

- (instancetype)init
{
    self = [super init];
    if (self) {
        //        [self customize];
        //
        //        [UIView addGradientToLoginButton:self.startButton];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        //        [self customize];
        //        [UIView addGradientToLoginButton:self.startButton];
    }
    return self;
}

-(void)drawRect:(CGRect)rect {
    
    
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (void)hideChange {
    [self.changeText setHidden:YES];
    [self.changeImage setHidden:YES];
    self.allViewConstraint.constant = 132;
    self.changeConstraint.constant = 0;
    
    [self.changeText setHidden:NO];
    [self.changeImage setHidden:NO];
    [self.changeButton setHidden:NO];
    
    [self.removeText setHidden:NO];
    [self.removeImage setHidden:NO];
    [self.removeButton setHidden:NO];
}

- (void)showChange {
    [self.changeText setHidden:NO];
    [self.changeImage setHidden:NO];
    self.allViewConstraint.constant = 193;
    self.changeConstr.constant = 44;
    self.removeConstr.constant = 44;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
}

- (void)showChangeFromFriend {
    [self.changeText setHidden:NO];
    [self.changeImage setHidden:NO];
    self.allViewConstraint.constant = 104;
    self.changeConstr.constant = 0;
    self.removeConstr.constant = 0;
    
    [self.changeText setHidden:YES];
    [self.changeImage setHidden:YES];
    [self.changeButton setHidden:YES];
    
    [self.removeText setHidden:YES];
    [self.removeImage setHidden:YES];
    [self.removeButton setHidden:YES];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self addGestureRecognizer:singleFingerTap];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    [self.delegate close];
}

-(IBAction)close:(id)sender {
    [self.delegate close];
}

-(IBAction)copyWord:(id)sender {
    [self.delegate copy];
}

-(IBAction)translateWord:(id)sender {
    [self.delegate translate];
}

-(IBAction)removeWord:(id)sender {
    [self.delegate remove];
}

-(IBAction)changeWord:(id)sender {
    [self.delegate change];
}

@end
