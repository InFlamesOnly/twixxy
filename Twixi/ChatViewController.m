//
//  ChatViewController.m
//  Twixi
//
//  Created by macOS on 16.05.17.
//  Copyright © 2017 macOS. All rights reserved.
//

#import "ChatViewController.h"
#import "MyMassegeCell.h"
#import "OtherMessageCell.h"
#import "User.h"
#import "MMTabBarController.h"
#import <IQKeyboardManager.h>
#import "THChatInput.h"
#import <UIImageView+AFNetworking.h>
#import "RLMUser.h"
#import "RequestManager.h"
#import "RLMMessage.h"
#import "UIView+ShadowToButton.m"
#import "AppDelegate.h"
#import "MessageActionSheet.h"
#import <Pusher/Pusher.h>
#import "NSString+TelegramNSDateString.h"
#import <AVFoundation/AVFoundation.h>
#import <SVPullToRefresh.h>
#import <GrowingTextViewHandler.h>
#import "Reachability.h"
#import "NSDictionary+DictCategory.h"
#import "PCAngularActivityIndicatorView.h"
#import "AvatarView.h"
#import "UserInfoViewController.h"
#import "SubscribeView.h"
#import "UIAlertController+Blocks.h"
#import "DownArrow.h"
#import "RLMSubscribe.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ChatViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, MessageActionSheetDelegate, UIGestureRecognizerDelegate, PTPusherDelegate, TCCopyableLabelDelegate, GrowingTextViewHandlerDelegate, DownArrowDelegate>


@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastOnline;

@property (weak, nonatomic) IBOutlet AvatarView *avatarFromNavigation;
@property (weak, nonatomic) IBOutlet AvatarView *avatarView;

@property (weak, nonatomic) IBOutlet DownArrow *downArrowButton;

//@property (weak, nonatomic) IBOutlet UILabel *initials;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateToAdd;
@property (weak, nonatomic) IBOutlet UIView *oneMessageView;

@property (weak, nonatomic) IBOutlet UITextView *chatTextView;
@property (weak, nonatomic) IBOutlet UITextView *translateTextView;

@property (weak, nonatomic) IBOutlet UIView *chatView;
@property (weak, nonatomic) IBOutlet UIView *translateView;
@property (weak, nonatomic) IBOutlet UIView *translateBcgView;

@property (strong, nonatomic) RLMUser *user;
@property (strong, nonatomic) UIBezierPath *cellPath;
@property (nonatomic) double keyboardHeight;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) NSIndexPath *translateIndexPathRow;
@property (strong, nonatomic) NSIndexPath *sendMessageIndexPath;

@property (strong, nonatomic) NSTimer *translateTimer;
@property (strong, nonatomic) UIBezierPath *circlePath;

@property (strong, nonatomic) PTPusher *client;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *translateBotConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatBotConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chatFullViewConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *translateViewConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *translateFullViewConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *downArrowConstraint;

@property (strong, nonatomic) MessageActionSheet *actionSheet;

@property (strong, nonatomic) GrowingTextViewHandler *chatTextHendler;
@property (strong, nonatomic) GrowingTextViewHandler *translateTextHendler;
@property (strong, nonatomic) GrowingTextViewHandler *chatViewHendler;

@property (strong, nonatomic) CAShapeLayer *circleLayer;

@property (strong, nonatomic) UIView *activityView;
@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@property (weak, nonatomic) IBOutlet UIButton *translateButton;

@property (strong, nonatomic) SubscribeView *subscribeView;

@property (nonatomic) CGFloat lastContentOffset;

@property NSString *messageText;
@property NSString *messageCopyText;

@property BOOL chatIsFind;

@property BOOL buttonIsHide;

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@property int countSpace;

@property BOOL changeWorld;
@property BOOL beginTranslate;
@property BOOL firstShow;
@property int paging;

@end


@implementation ChatViewController

//TODO выдвигающийся текст филди, downButton
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configNavigation];
    [self configureStartScreen];
    [self addPulToRequestObject];
    [self createBlinkSound];
    [self setAvatarRoundCorners];
    [self configureTranslateButton];
    [self configureStartLoadMessage];
    [self createChangeHandler];
    [self createTranslateHendler];
    [self configChatView];
    [self configTranslateView];
    [self configAvatars];
    [self configTapToHideKeyboard];
    [self configTapToAvatar];
    [self startDownButton];
    [self updateSubscribe];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

- (void)updateSubscribe {
    if ([self.user.subType  isEqual: @"DM.twiximassanger.com.Twixi.weaksubscribe"]) {
        RLMSubscribe *subscribe = [RLMSubscribe allObjects][0];
        [[RequestManager sharedManager] subscribeWithSubscribeId:subscribe.value withSuccessBlock:^(NSDictionary *success) {
            
        } onFalureBlock:^(NSInteger statusCode) {

        }];
    }
    
    if ([self.user.subType  isEqual: @"DM.twiximassanger.com.Twixi.onemonth"]) {
        RLMSubscribe *subscribe = [RLMSubscribe allObjects][1];
        [[RequestManager sharedManager] subscribeWithSubscribeId:subscribe.value withSuccessBlock:^(NSDictionary *success) {
            
        } onFalureBlock:^(NSInteger statusCode) {

        }];
    }
    
    if ([self.user.subType  isEqual: @"DM.twiximassanger.com.Twixi.threemonth"]) {
        RLMSubscribe *subscribe = [RLMSubscribe allObjects][2];
        [[RequestManager sharedManager] subscribeWithSubscribeId:subscribe.value withSuccessBlock:^(NSDictionary *success) {
            
        } onFalureBlock:^(NSInteger statusCode) {

        }];
    }
}

- (void)startDownButton {
    self.downArrowButton.delegate = self;
    self.buttonIsHide = YES;
    self.downArrowConstraint.constant = -128;
    [self.view layoutIfNeeded];
}

#pragma mark - UIScrollViewDelegate

- (void)tapToDownButton {
    [self scrollAtLastMessagePosition];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        [self checkLastMessage];
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
        [self checkLastMessage];
    }
    self.lastContentOffset = scrollView.contentOffset.y;
}

- (void)checkLastMessage {
    if (self.messageArray.count != 0) {
        NSIndexPath *myIndexPath = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
        NSArray *visibleRows = [self.chatTableView indexPathsForVisibleRows];
        NSIndexPath *lastRow = [visibleRows lastObject];
        
        if (lastRow.row == myIndexPath.row) {
            if (self.buttonIsHide == NO) {
                [self hideScrollToLastMessageButton];
                self.buttonIsHide = YES;
            }
            NSLog(@"row is last");
        } else {
            if (self.buttonIsHide == YES) {
                [self showScrollToLastMessageButton];
                self.buttonIsHide = NO;
            }
            NSLog(@"last row is hidden");
        }
    }
}


- (void)showScrollToLastMessageButton {
    self.downArrowConstraint.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)hideScrollToLastMessageButton {
    self.downArrowConstraint.constant = -128;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
//    [self.client disconnect];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)configNavigation {
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)configureStartScreen {
    self.messageArray = [NSMutableArray new];
    self.user = [RLMUser currentUser];
    self.changeWorld = NO;
    self.nameLabel.text = self.selectedFriend.name;
    self.nickNameLabel.text = [NSString stringWithFormat:@"@%@",self.selectedFriend.nickName];
    self.dateToAdd.text = [NSString stringWithFormat:@"%@ %@ %@ %@",NSLocalizedString(@"you added", nil),self.selectedFriend.name,NSLocalizedString(@"from", nil),[self convert:self.selectedFriend.addDate fromNeedDateFormat:@"EEEE, MMM d, yyyy"]];
    [self.oneMessageView setHidden:NO];
    [self.chatTableView setHidden:YES];
    NSString *time = [NSString convertResponseToCellsTimeString:self.selectedFriend.lastOnline];
    self.lastOnline.text = time;
    self.firstShow = YES;
    [self addShafowToView:self.translateBcgView];
    self.translateTextView.editable = NO;
    self.paging = 1;
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[IQKeyboardManager sharedManager] setEnable:NO];
    self.tittleLabel.text = self.selectedFriend.name;
    self.translateButton.selected = [self.user.userEnableTranslate intValue];
}

- (void)addPulToRequestObject {
    __weak ChatViewController *weakSelf = self;
    [self.sendButton setEnabled:NO];
    [self.chatTableView addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
        [weakSelf.chatTableView.pullToRefreshView stopAnimating];
    }];
}

- (void)createBlinkSound {
    NSString *path = [NSString stringWithFormat:@"%@/Massage.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
}

- (void)setAvatarRoundCorners {
    self.avatarView.layer.cornerRadius = self.avatarView.frame.size.width / 2;
    self.avatarView.clipsToBounds = YES;
}

- (void)configureTranslateButton {
    [self.translateButton setImage:[UIImage imageNamed:@"TranslateOn"] forState:UIControlStateSelected];
    [self.translateButton setImage:[UIImage imageNamed:@"TranslateOff"] forState:UIControlStateNormal];
}

- (void)createChangeHandler {
    self.chatTextHendler = [[GrowingTextViewHandler alloc]initWithTextView:self.chatTextView withHeightConstraint:self.chatViewConstraint];
    [self.chatTextHendler updateMinimumNumberOfLines:0 andMaximumNumberOfLine:4];
    self.chatTextHendler.chatTextFieldName = @"chatTextView";
    self.chatTextHendler.delegate = self;
}

- (void)createTranslateHendler {
    self.translateTextHendler = [[GrowingTextViewHandler alloc]initWithTextView:self.translateTextView withHeightConstraint:self.translateViewConstraint];
    [self.translateTextHendler updateMinimumNumberOfLines:0 andMaximumNumberOfLine:4];
    self.beginTranslate = NO;
    self.translateTextHendler.chatTextFieldName = @"translateTextField";
    self.translateTextHendler.delegate = self;
}

- (void)configureStartLoadMessage {
    RLMChat *findChat = [[RLMChat alloc] init];
    NSArray *obj = [findChat findFromFriendId:self.selectedFriend.friendId];
    if (obj.count == 1) {
        self.chat = obj[0];
        self.chatIsFind = YES;
        if (self.chat.message.count == 0) {
            [self addActivityIndicator];
            [self createChatWithMessagess];
        } else {
            [self loacalMessage];
        }
    } else {
        self.chatIsFind = NO;
    }
}

- (void)createChatWithMessagess {
    [[RequestManager sharedManager] createChatsWithUserId:self.selectedFriend.friendId withSuccessBlock:^(NSDictionary *success) {
        RLMChat *chat = [self parseChatWithDict:success];
        chat = [chat findInteresFromArray:(NSArray*)[RLMChat allObjects] chatId:chat.chatId];
        if (chat == nil) {
            [self createChatFromServer:success fromChat:chat];
        } else {
            [self findChatAndLoadMessage:chat];
        }
        [self pusherCalbackObject];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)findChatAndLoadMessage:(RLMChat*)chat {
    self.chat = chat;
    for (RLMMessage *message in self.chat.message) {
        [self.messageArray addObject:message];
    }
    if (self.messageArray.count == 0) {
        [self.activity removeFromSuperview];
        [self.activityView removeFromSuperview];
        [self addLastMessagess];
        [self.oneMessageView setHidden:NO];
        [self.chatTableView setHidden:YES];
    } else {
        [self.chatTableView setHidden:NO];
        [self.oneMessageView setHidden:YES];
        [self reloadFromEmptyMessages];
    }
}

- (void)createChatFromServer:(NSDictionary*)success fromChat:(RLMChat*)chat {
    RLMChat *saveChat = [self parseChatWithDict:success];
    chat = saveChat;
    self.chat = chat;
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] addObject:saveChat];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (void)loacalMessage {
    for (RLMMessage *message in self.chat.message) {
        [self.messageArray addObject:message];
    }
    [self.chatTableView setHidden:NO];
    [self.oneMessageView setHidden:YES];
    [self reloadFromEmptyMessages];
    [self removeActivity];
}

- (void)configChatView {
    self.chatView.layer.cornerRadius = 15;
    self.chatView.clipsToBounds = YES;
    self.chatView.layer.borderWidth = 1;
    self.chatView.layer.borderColor = [UIColor colorWithRed:206.0f/255.0f green:215.0f/255.0f blue:229.0f/255.0f alpha:1.0].CGColor;
}

- (void)configTranslateView {
    self.translateView.layer.cornerRadius = 15;
    self.translateView.layer.borderColor = [UIColor colorWithRed:227.0f/255.0f green:234.0f/255.0f blue:244.0f/255.0f alpha:1.0].CGColor;
    self.translateView.layer.borderWidth = 2;
    self.translateView.clipsToBounds = YES;
}

- (void)configAvatars {
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.selectedFriend.name , self.selectedFriend.lastName];
    [self.avatarView setAvatar:self.selectedFriend.avatar orInitialsView:fullName gradientColorsArray:(NSArray*)self.selectedFriend.gradient];
    [self.avatarFromNavigation setAvatar:self.selectedFriend.avatar orInitialsView:fullName gradientColorsArray:(NSArray*)self.selectedFriend.gradient];
}

- (void)configTapToHideKeyboard {
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tableTapped:)];
    [self.chatTableView addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [tap setCancelsTouchesInView:NO];
    [self.oneMessageView addGestureRecognizer:tap];
}

- (void)tableTapped:(UITapGestureRecognizer *)tap {
    CGPoint location = [tap locationInView:self.chatTableView];
    NSIndexPath *path = [self.chatTableView indexPathForRowAtPoint:location];
    
    if (path) {
        [self dismissKeyboard];
    }
}

- (void)configTapToAvatar {
    UITapGestureRecognizer *tapToAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(showProfile)];
    [self.avatarFromNavigation addGestureRecognizer:tapToAvatar];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    [self dismissKeyboard];
    return YES;
}

- (IBAction)offOnTranslate:(id)sender {
    [self.view endEditing:YES];
    [self enableOrDisableTranslate];
    [self enableOrDisableTranslateButton];
}

- (void)enableOrDisableTranslate {
    if ([self.user.userBuySubscribe isEqualToNumber:@(1)] ) {
        self.translateButton.selected = !self.translateButton.selected;
        [[RLMRealm defaultRealm] beginWriteTransaction];
        self.user.userEnableTranslate = @(self.translateButton.selected);
        [[RLMRealm defaultRealm] commitWriteTransaction];
    } else {
        self.subscribeView = [[SubscribeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.tabBarController.view addSubview:self.subscribeView];
        [self.subscribeView open];
    }
}

- (void)showSubscribeView {
        self.subscribeView = [[SubscribeView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        [self.tabBarController.view addSubview:self.subscribeView];
        [self.subscribeView open];
        [self.view endEditing:YES];
}

- (void)enableOrDisableTranslateButton {
    if (self.translateButton.selected) {
        self.translateTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(translateTimerEnd:) userInfo:nil repeats:NO];
    } else {
        [self stopTimer];
        self.translateFullViewConstraint.constant = 40;
        self.beginTranslate = NO;
        [self.view layoutIfNeeded];
    }
}

- (void)addActivityIndicator {
    self.activityView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityView.backgroundColor = [UIColor blackColor];
    self.activityView.alpha = 0.5;
    self.activityView.center = self.view.center;
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.activityView convertPoint:self.activityView.center fromView:self.activityView.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activityView];
    [self.view addSubview:self.activity];
}

- (void)showProfile {
    [self performSegueWithIdentifier:@"showUserInfo" sender:self];
    NSLog(@"AvatarTap");
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showUserInfo"]) {
        UserInfoViewController *vc = segue.destinationViewController;
        vc.selectedFriend = self.selectedFriend;
    }
}

- (void)addLastMessagess {
    [self addActivityIndicator];
    [self getChatHistory];
}

- (void)getChatHistory {
    [[RequestManager sharedManager] getChatsHistoryFromChatId:self.chat.chatId withPage:@(1) pageOffset:@(30) withSuccessBlock:^(NSDictionary *success) {
        NSArray *messagesArray = [success valueForKey:@"data"];
        NSMutableArray *mArray = [NSMutableArray new];
        messagesArray = [[messagesArray reverseObjectEnumerator] allObjects];
        [self removeAllMessagesFromChat];
        for (NSDictionary *dict in messagesArray) {
            NSString *result = [self convert:[dict valueForKey:@"created_at"] fromNeedDateFormat:@"HH:mm:ss"];
            if ([[[[dict valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"phone"] isEqualToString:self.user.telephone]) {
                RLMMessage *my = [self myMessage:dict fromCreatedAt:result];
                [mArray addObject:my];
            } else {
                RLMMessage *friend = [self friendMessage:dict fromCreatedAt:result];
                [mArray addObject:friend];
            }
        }
        [self removeActivity];
        self.messageArray = mArray;
        [self.chatTableView reloadData];
        if (self.messageArray.count != 0) {
            [self.chatTableView setHidden:NO];
            [self.oneMessageView setHidden:YES];
        } else {
            [self.chatTableView setHidden:YES];
            [self.oneMessageView setHidden:NO];
        }
        
        [self reloadFromEmptyMessages];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)removeActivity {
    [self.activity removeFromSuperview];
    [self.activityView removeFromSuperview];
}

- (RLMMessage*)myMessage:(NSDictionary*)dict fromCreatedAt:(NSString*)result {
    RLMMessage *message = [[RLMMessage alloc] init];
    message.role = @(1);
    message.time = result;
    message.message = [dict valueForKey:@"body"];
    message.mId = [dict valueForKey:@"id"];
    message.isSend = YES;
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [self.chat.message addObject:message];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    return message;
}

- (RLMMessage*)friendMessage:(NSDictionary*)dict fromCreatedAt:(NSString*)result {
    RLMMessage *message = [[RLMMessage alloc] init];
    message.role = @(2);
    message.time = result;
    message.message = [dict valueForKey:@"body"];
    message.mId = [dict valueForKey:@"id"];
    message.isSend = YES;
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [self.chat.message addObject:message];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    return message;
}

- (void)removeAllMessagesFromChat {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [self.chat.message removeAllObjects];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}


- (void)reloadFromEmptyMessages {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.messageArray.count > 0) {
            [self.oneMessageView setHidden:YES];
            [self.chatTableView setHidden:NO];
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
            [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    });
}

- (void)scrollAtLastMessagePosition {
    NSIndexPath * indexPath = [NSIndexPath indexPathForRow:self.messageArray.count - 1 inSection:0];
    [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (RLMChat*)parseChatWithDict:(NSDictionary*)success {
    success = [success dictionaryRemovingNSNullValues];
    RLMChat *chat = [[RLMChat alloc] init];
    chat.chatKey = [NSString stringWithFormat:@"%@",[[success valueForKey:@"data"] valueForKey:@"id"]];
    chat.chatId = [[success valueForKey:@"data"] valueForKey:@"id"];
    NSDictionary *membFriend = [[[success valueForKey:@"data"] valueForKey:@"users"] valueForKey:@"data"][0];
    chat.name = [NSString stringWithFormat:@"%@ %@",[membFriend valueForKey:@"first_name"],[membFriend valueForKey:@"last_name"]];
    chat.nickName = [membFriend valueForKey:@"nickname"];
    chat.createdAt = [membFriend valueForKey:@"created_at"];
    chat.updatedAt = [membFriend valueForKey:@"updated_at"];
    chat.sentAt = [membFriend valueForKey:@"last_login"];
    chat.avatar = [membFriend valueForKey:@"avatar"];
    chat.friendId = [membFriend valueForKey:@"id"];
    
    if ([[membFriend valueForKey:@"avatar"] isEqualToString:@""]) {
        NSArray *gradientOneArray = [membFriend valueForKey:@"gradient"][0];
        NSArray *gradientTwoArray = [membFriend valueForKey:@"gradient"][1];
        
        for (NSNumber *i in gradientOneArray) {
            RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
            [chat.gradient addObject:value];
        }
        for (NSNumber *i in gradientTwoArray) {
            RLMGradientValue *value = [[RLMGradientValue alloc] initWithValue:i];
            [chat.gradient addObject:value];
        }
    }
    return chat;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

- (void)onlineOffline {
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:[NSString stringWithFormat:@"contacts-status-channel.%@",self.user.userId]];
    
    [channel bindToEventNamed:@"Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" handleWithBlock:^(PTPusherEvent *channelEvent) {
        
        NSDictionary *data = [channelEvent.data valueForKey:@"data"];
        NSNumber *online = [[data valueForKey:@"data"] valueForKey:@"online"];
        NSString *lastLogin = [[data valueForKey:@"data"] valueForKey:@"last_login"];
        
        if ([online isEqualToNumber:@(1)]) {
            self.lastOnline.text = @"Online";
        } else {
            NSString *time = [NSString convertResponseToCellsTimeString:lastLogin];
            self.lastOnline.text = time;
        }
    }];
}

- (void)changeMessage {
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:[NSString stringWithFormat:@"mc-chat-conversation-message-update.%@",self.user.userId]];

    [channel bindToEventNamed:@"Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" handleWithBlock:^(PTPusherEvent *channelEvent) {
        [self changeMessageFromPusher:channelEvent.data];
        NSLog(@"channelEvent");
    }];
}

- (void)removeMessage {
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:[NSString stringWithFormat:@"mc-chat-conversation-message-delete.%@",self.user.userId]];
    
    [channel bindToEventNamed:@"Illuminate\\Notifications\\Events\\MessageDeleteNotification" handleWithBlock:^(PTPusherEvent *channelEvent) {
        NSLog(@"channelEvent");
        [self removeMessagePusher:channelEvent.data];
    }];
}

- (void)changeMessageFriend {
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:[NSString stringWithFormat:@"mc-chat-conversation-message-update.%@",self.selectedFriend.friendId]];
    
    [channel bindToEventNamed:@"Illuminate\\Notifications\\Events\\BroadcastNotificationCreated" handleWithBlock:^(PTPusherEvent *channelEvent) {
        [self changeMessageFromPusher:channelEvent.data];
        NSLog(@"channelEvent");
    }];
}

- (void)removeMessageFriend {
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:[NSString stringWithFormat:@"mc-chat-conversation-message-delete.%@",self.selectedFriend.friendId]];
    
    [channel bindToEventNamed:@"Illuminate\\Notifications\\Events\\MessageDeleteNotification" handleWithBlock:^(PTPusherEvent *channelEvent) {
        [self removeMessagePusher:channelEvent.data];
        NSLog(@"channelEvent");
    }];
}

- (void)removeMessagePusher:(NSDictionary*)data {
    RLMMessage *messageRlm = [self findMessageFromMessageId:[[data valueForKey:@"data"] valueForKey:@"id"]];
    if (messageRlm.mId != nil) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        NSUInteger i = [self.messageArray indexOfObject:messageRlm];
        [self.messageArray removeObjectAtIndex:i];
        [self.chatTableView reloadData];
        [self.chat.message removeObjectAtIndex:i];
        [[RLMRealm defaultRealm] commitWriteTransaction];
        RLMMessage *newLastMessage = [self.messageArray lastObject];
        self.chat.lastMessage = newLastMessage.message;
        self.chat.sentAt = newLastMessage.time;
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}
//[channelEvent.data objectForKey:@"message"];

- (void)pusherCalbackObject {
    self.client = [PTPusher pusherWithKey:@"5a9ae17e447d7c736b9a" delegate:self encrypted:YES cluster:@"eu"];
    
    PTPusherChannel *channel = [self.client subscribeToChannelNamed:[NSString stringWithFormat:@"mc-chat-conversation.%@",self.chat.chatId]];
    
    [channel bindToEventNamed:@"Musonza\\Chat\\Messages\\MessageWasSent" handleWithBlock:^(PTPusherEvent *channelEvent) {
        // channelEvent.data is a NSDictianary of the JSON object received
        NSString *message = [channelEvent.data objectForKey:@"message"];
        if ([message valueForKey:@"user_id"] == self.user.userId) {
            [self getMyMessageFromPusher:message];
        } else {
            [self getFriendMessageFromPusher:message];
        }
    }];
    [self pusherConfigure];
    [self.client connect];
}

- (void)updateFromLastMessage:(RLMMessage*)message addMessageDate:(NSString*)messageDate {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    self.chat.lastMessage = message.message;
    self.chat.sentAt = [messageDate valueForKey:@"created_at"];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (void)scrollAtLastMessage {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:(self.messageArray.count - 1) inSection:0];
    [self.chatTableView beginUpdates];
    [self.chatTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    [self.chatTableView endUpdates];
    [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

- (void)getFriendMessageFromPusher:(NSString*)message {
    RLMMessage *respMessage = [[RLMMessage alloc] init];
    respMessage.message = [message valueForKey:@"body"];
    NSString *result = [self convert:[message valueForKey:@"created_at"] fromNeedDateFormat:@"HH:mm:ss"];
    respMessage.role = @(2);
    respMessage.time = result;
    respMessage.mId = [message valueForKey:@"id"];
    
    if (self.chat.message.count >= 30) {
        [self.chat repleceFirstAndAddToLast:respMessage fromChat:self.chat.chatId];
        [self.messageArray addObject:respMessage];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [self.chat.message addObject:respMessage];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    } else {
        [self.messageArray addObject:respMessage];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [self.chat.message addObject:respMessage];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
    [self updateFromLastMessage:respMessage addMessageDate:message];
    [self scrollAtLastMessage];
}

- (void)getMyMessageFromPusher:(NSString*)message {
    RLMMessage *messageRlm = [self findMessageFromMessageId:[message valueForKey:@"id"]];
    if (messageRlm.mId == nil) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        messageRlm = [self.chat.message lastObject];
        messageRlm.mId = [message valueForKey:@"id"];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
    [[RLMRealm defaultRealm] beginWriteTransaction];
    messageRlm.isSend = YES;
    [[RLMRealm defaultRealm] commitWriteTransaction];
    
    [self messsage:messageRlm];
    
    [[RLMRealm defaultRealm] beginWriteTransaction];
    self.chat.lastMessage = messageRlm.message;
    self.chat.sentAt = [message valueForKey:@"created_at"];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

- (void)pusherConfigure {
    [self onlineOffline];
    [self changeMessage];
    [self removeMessage];
    [self removeMessageFriend];
    [self changeMessageFriend];
}

-(void)viewDidLayoutSubviews {
    [self.navigationController.navigationBar setHidden:YES];
    [UIView addGradientToView:self.navigationView];
}

- (RLMMessage*)findMessageFromMessageId:(NSNumber*)messageId {
    RLMMessage *message = [[RLMMessage alloc] init];
    for (RLMMessage *mess in self.messageArray) {
        if ([mess.mId isEqualToNumber:messageId]) {
            message = mess;
        }
    }
    return message;
}


- (void)messsage:(RLMMessage*)messsage {
    for (int i = 0; i < self.messageArray.count; i++) {
        RLMMessage *iMessage = self.messageArray[i];
        if (iMessage.mId == messsage.mId) {
            [self.messageArray replaceObjectAtIndex:i withObject:messsage];
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:i inSection:0];
            NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            [self.chatTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}


-(void)resizeViewRemove:(NSString*)hendlerName withHeight:(int)height {
    self.translateFullViewConstraint.constant = self.translateFullViewConstraint.constant - 13;
}

- (void)resizeViewAdd:(NSString*)hendlerName withHeight:(int)height {
    if ([hendlerName isEqualToString:@"chatTextView"]) {
        if ([self.user.userBuySubscribe isEqualToNumber:@(1)]) {
            self.translateFullViewConstraint.constant = 5 + 5 + 5 + self.translateViewConstraint.constant + height;
        }
        [self updateContentViewFromStartValue:20];

    } else {
        [self updateContentViewFromStartValue:20];
        self.translateFullViewConstraint.constant = 5 + 5 + 5 + self.chatViewConstraint.constant + height;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    [self.chatTextHendler resizeTextViewWithAnimation:YES];
    [self.translateTextHendler resizeTextViewWithAnimation:YES];
}

- (void)updateContentViewFromStartValue:(int)value {
    [self.view layoutIfNeeded];
    UIEdgeInsets insets = UIEdgeInsetsMake(self.chatTableView.contentInset.top, 0, 0, 0);
    
    if (self.beginTranslate == YES) {
        self.chatTableView.contentInset = insets;
        self.chatTableView.scrollIndicatorInsets = insets;
        self.chatTableView.contentOffset = CGPointMake(self.chatTableView.contentOffset.x, self.chatTableView.contentOffset.y);
    } else {
        self.chatTableView.contentInset = insets;
        self.chatTableView.scrollIndicatorInsets = insets;
        self.chatTableView.contentOffset = CGPointMake(self.chatTableView.contentOffset.x, self.chatTableView.contentOffset.y + value);
    }
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [self keyboardHide:notification];
    [self updateKeyboardShow];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [self keyboardShow:notification];
    [self updateKeyboardShow];
}

- (void)keyboardShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.keyboardHeight = (double)(keyboardSize.height);
    self.chatBotConstraint.constant = self.keyboardHeight;
    
    self.chatTableView.contentOffset = CGPointMake(self.chatTableView.contentOffset.x, self.chatTableView.contentOffset.y + keyboardSize.height);
}

- (void)keyboardHide:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.keyboardHeight = (double)(keyboardSize.height);
    self.chatBotConstraint.constant = 0;
}

- (void)updateKeyboardShow {
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (void)getChatListHistoryWithPaging:(NSNumber*)page {
    [[RequestManager sharedManager] getChatsHistoryFromChatId:self.chat.chatId withPage:page pageOffset:@(30) withSuccessBlock:^(NSDictionary *success) {
        NSArray *messagesArray = [success valueForKey:@"data"];
        if (messagesArray.count != 0) {
            NSMutableArray *messages = [self parseArrayMessages:messagesArray];
            [self convertArrayMessagess:messages];
        }
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (NSMutableArray*)parseArrayMessages:(NSArray*)array {
    array = [[array reverseObjectEnumerator] allObjects];
    NSMutableArray *messageArrary = [NSMutableArray new];
    for (NSDictionary *dict in array) {
        NSString *result = [self convert:[dict valueForKey:@"created_at"] fromNeedDateFormat:@"HH:mm:ss"];
        RLMMessage *message = [[RLMMessage alloc] init];
        if ([[[[dict valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"phone"] isEqualToString:self.user.telephone]) {
            message.role = @(1);
        } else {
            message.role = @(2);
        }
        message.time = result;
        message.message = [dict valueForKey:@"body"];
        message.mId = [dict valueForKey:@"id"];
        message.isSend = YES;
        [messageArrary addObject:message];
    }
    return messageArrary;
}

- (void)convertArrayMessagess:(NSArray*)messageArrary {
    NSMutableArray *array = (NSMutableArray*)self.messageArray;
    messageArrary = (NSMutableArray*)[[messageArrary reverseObjectEnumerator] allObjects];
    for (RLMMessage *mes in messageArrary) {
        [array insertObject:mes atIndex:0];
        [self.chatTableView beginUpdates];
        
        [self.chatTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [self.chatTableView endUpdates];
    }
    self.messageArray = array;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self pusherCalbackObject];

}

-(void)dismissKeyboard {
    [self.translateTextView resignFirstResponder];
    [self.chatTextView resignFirstResponder];
}

#pragma mark - TextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *searchStr = [textView.text stringByReplacingCharactersInRange:range
                                                                        withString:text];
    BOOL newLine = [text isEqualToString:@"\n"];
    if(newLine) {
        if (self.chatTextView.text.length == 0) {
            return NO;
        }
    }
    
    [self checkEnableButtonsFromString:searchStr];
    
    [self stopTimer];
    [self checkCherecterCount:[searchStr length]];

    return YES;
}

- (void)checkEnableButtonsFromString:(NSString*)text {
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[text stringByTrimmingCharactersInSet: set] length] == 0) {
        [self.sendButton setEnabled:NO];
    } else {
        [self.sendButton setEnabled:YES];
    }
}

- (void)checkCherecterCount:(NSUInteger)characterCount {
    if (characterCount > 0) {
        if (self.translateButton.selected == YES) {
            self.translateTimer = [NSTimer scheduledTimerWithTimeInterval:0.8 target:self selector:@selector(translateTimerEnd:) userInfo:nil repeats:NO];
        }
    } else {
        self.translateFullViewConstraint.constant = 40;
        self.beginTranslate = NO;
        [self.view layoutIfNeeded];
    }
}

-(void)translateTimerEnd:(NSTimer *)time {
    [self replaceWord:self.chatTextView.text fromTextView:self.translateTextView];
}

- (void)replaceWord:(NSString*)word fromTextView:(UITextView*)textView {
   __block NSString *translateText = [NSString new];
    [[RequestManager sharedManager] translateText:word withSourceLang:self.user.myLangCode toParsLang:self.user.translateLangCode withSuccessBlock:^(NSDictionary *success) {
            translateText = [success valueForKey:@"data"];
            [textView setText:translateText];
            [self checkBeginTranslate];
            [self.translateTextHendler resizeTextViewWithAnimation:YES];
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
}

- (void)checkBeginTranslate {
    if (self.beginTranslate == NO) {
        [self.view layoutIfNeeded];
        self.translateFullViewConstraint.constant = 80;
        [self updateContentViewFromStartValue:40];
//        [self updateContentViewFromStart];
        self.beginTranslate = YES;
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.countSpace = 0;
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}

- (IBAction)attacmentAction:(id)sender {
    NSLog(@"attacmentAction");
}

- (IBAction)translate:(id)sender {
    NSString *message = self.chatTextView.text;
    NSString *translate = self.translateTextView.text;
    self.chatTextView.text = translate;
    self.translateTextView.text = message;
}

- (IBAction)back:(id)sender {
    [self.chatTextView resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)sendMessage:(id)sender {
    [self hideScrollToLastMessageButton];
    [self configSendMessage];
    [self hideScrollToLastMessageButton];
}

- (void)removeEnters {
    NSString *trimmedString = [self.chatTextView.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.messageText = trimmedString;
}

- (void)checkInternetConnectionFromSendMessage {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus] == NotReachable) {
        [self showAllertViewWithTittle:NSLocalizedString(@"Error", nil) andMessage:NSLocalizedString(@"Internet connection error", nil)];
        [self.sendButton setEnabled:YES];
        return;
    } else {
        if (self.chatIsFind == NO) {
            [self createChats];
        } else {
            [self createAndSendMessageFromSever];
        }
    }
}

- (void)createAndSendMessageFromSever {
    [self sendMessage];
    [self sendMessageFromServer];
}

- (void)createChats {
    [self addActivityIndicator];
    [[RequestManager sharedManager] createChatsWithUserId:self.selectedFriend.friendId withSuccessBlock:^(NSDictionary *success) {
        RLMChat *chat = [self parseChatWithDict:success];
        chat = [chat findInteresFromArray:(NSArray*)[RLMChat allObjects] chatId:chat.chatId];
        if (chat == nil) {
            [self createChatFromServer:success fromChat:chat];
        }
        [self getChatHistoryWithOldChats];
        self.chatIsFind = YES;
        [self pusherCalbackObject];
        [self removeActivity];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)getChatHistoryWithOldChats {
    [[RequestManager sharedManager] getChatsHistoryFromChatId:self.chat.chatId withPage:@(1) pageOffset:@(30) withSuccessBlock:^(NSDictionary *success) {
        NSArray *messagesArray = [success valueForKey:@"data"];
        NSMutableArray *mArray = [NSMutableArray new];
        messagesArray = [[messagesArray reverseObjectEnumerator] allObjects];
        [self removeAllMessagesFromChat];
        for (NSDictionary *dict in messagesArray) {
            NSString *result = [self convert:[dict valueForKey:@"created_at"] fromNeedDateFormat:@"HH:mm:ss"];
            if ([[[[dict valueForKey:@"sender"] valueForKey:@"data"] valueForKey:@"phone"] isEqualToString:self.user.telephone]) {
                RLMMessage *my = [self myMessage:dict fromCreatedAt:result];
                [mArray addObject:my];
            } else {
                RLMMessage *friend = [self friendMessage:dict fromCreatedAt:result];
                [mArray addObject:friend];
            }
        }
        [self removeActivity];
        self.messageArray = mArray;
        [self.chatTableView reloadData];
        if (self.messageArray.count != 0) {
            [self.chatTableView setHidden:NO];
            [self.oneMessageView setHidden:YES];
        } else {
            [self.chatTableView setHidden:YES];
            [self.oneMessageView setHidden:NO];
        }
        
        [self reloadFromEmptyMessages];
        self.chatIsFind = YES;
        [self createAndSendMessageFromSever];
    } onFalureBlock:^(NSInteger statusCode) {
        [self removeActivity];
    }];
}

- (void)sendMessageFromServer {
    [[RequestManager sharedManager] sendMessage:self.messageText fromChatId:self.chat.chatId withSuccessBlock:^(NSDictionary *success) {
        
    } onFalureBlock:^(NSInteger statusCode) {
        [self.sendButton setEnabled:YES];
    }];
}

- (void)chengeMessageFromButtonTap {
    UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
    self.translateFullViewConstraint.constant = 40;
    [self stopTimer];
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        MyMassegeCell *myCell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
        RLMMessage *message = self.messageArray[self.translateIndexPathRow.row];
        if ([self checkIsChange:message.message]) {
            [[RequestManager sharedManager] changeMessageFromChat:self.chat.chatId messageId:message.mId fromMessage:message.message withSuccessBlock:^(NSDictionary *success) {
                [[RLMRealm defaultRealm] beginWriteTransaction];
                myCell.message.text = self.chatTextView.text;
                message.message = self.chatTextView.text;
                [self.messageArray replaceObjectAtIndex:self.translateIndexPathRow.row withObject:message];
                self.changeWorld = NO;
                if (self.messageArray.count == self.translateIndexPathRow.row + 1) {
                    RLMMessage *newLastMessage = [self.messageArray lastObject];
                    self.chat.lastMessage = newLastMessage.message;
                }
                [[RLMRealm defaultRealm] commitWriteTransaction];
                [self clearMessagessView];
                [self.sendButton setEnabled:NO];
                [self.chatTableView beginUpdates];
                [self.chatTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:self.translateIndexPathRow.row inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
                [self.chatTableView endUpdates];
                [self dismissKeyboard];
            } onFalureBlock:^(NSInteger statusCode) {
                
            }];
        } else {
            NSLog(@"this message is delate");
        }
    }
}

- (void)changeMessageFromPusher:(NSDictionary*)data {
    NSString *newMessageText = [[data valueForKey:@"data"] valueForKey:@"body"];
    RLMMessage *messageRlm = [self findMessageFromMessageId:[[data valueForKey:@"data"] valueForKey:@"id"]];
    if (messageRlm.mId != nil) {
        [[RLMRealm defaultRealm] beginWriteTransaction];
        messageRlm.message = newMessageText;
        messageRlm.time = messageRlm.time;
        messageRlm.mId = messageRlm.mId;
        messageRlm.role = messageRlm.role;
        messageRlm.isSend = messageRlm.isSend;
        NSUInteger i = [self.messageArray indexOfObject:messageRlm];
        [self.messageArray replaceObjectAtIndex:i withObject:messageRlm];
        
        if (self.messageArray.count == i + 1) {
            RLMMessage *newLastMessage = [self.messageArray lastObject];
            self.chat.lastMessage = newLastMessage.message;
        }
        
        [self.chatTableView beginUpdates];
        [self.chatTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:i inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
        [self.chatTableView endUpdates];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}


- (BOOL)checkIsChange:(NSString*)message {
    if ([message isEqualToString:NSLocalizedString(@"Message deleted", nil)]) {
        return NO;
    } else {
        return YES;
    }
}

- (void)configSendMessage {
    if (self.changeWorld == NO) {
        if (self.chatTextView.text.length == 0) {
            self.chatTextView.text = @"";
            return;
        } else if (self.chatTextView.text.length > 0) {
            [self removeEnters];
            [_audioPlayer play];
            [self.sendButton setEnabled:NO];
            [self checkInternetConnectionFromSendMessage];
        }
    } else {
        [self chengeMessageFromButtonTap];
    }
}

#pragma mark - SendMessage
- (void)stopTimer {
    [self.translateTimer invalidate];
    self.translateTimer = nil;
}

- (void)sendMessage {
    [self startSendMessage];
    RLMMessage *message =  [self createSendMessage];
    [self checkFromNeedSaveDataBaseMessage:message];
    [self clearMessagessView];
    [self insertNewMessage];
    [self configureFieldState];
    [self stopTimer];
    [self endNewMessagessSends];
}

- (void)startSendMessage {
    self.beginTranslate = YES;
    [self.sendButton setEnabled:NO];
    [self.chatTableView setHidden:NO];
    [self.oneMessageView setHidden:YES];
    self.countSpace = 0;
}

- (RLMMessage*)createSendMessage {
    RLMMessage *message = [[RLMMessage alloc] initWithRole:[NSNumber numberWithInt:1] message:self.messageText endTime:[self convert:nil fromNeedDateFormat:@"HH:mm:ss"]];
    message.isSend = NO;
    if (self.messageArray.count != 0) {
        RLMMessage *lastMessage = [self.chat.message lastObject];
        int value = [lastMessage.mId intValue];
        value = value + 1;
        message.mId = @(value);
    } else {
        message.mId = @(0);
    }
    return message;
}

- (void)checkFromNeedSaveDataBaseMessage:(RLMMessage*)message {
    RLMChat *chat = self.chat;
    if (self.chat.message.count >= 30) {
        [self.chat repleceFirstAndAddToLast:message fromChat:self.chat.chatId];
        [self.messageArray addObject:message];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [chat.message addObject:message];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    } else {
        [self.messageArray addObject:message];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [chat.message addObject:message];
        [[RLMRealm defaultRealm] commitWriteTransaction];
    }
}

- (void)clearMessagessView {
    self.translateTextView.text = nil;
    self.chatTextView.text = nil;
}

- (void)insertNewMessage {
    self.sendMessageIndexPath = [NSIndexPath indexPathForRow:self.messageArray.count inSection:0];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:(self.messageArray.count - 1) inSection:0];
    [UIView setAnimationsEnabled:NO];
    [self.chatTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
    [UIView setAnimationsEnabled:YES];
}

- (void)endNewMessagessSends {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:(self.messageArray.count - 1) inSection:0];
    [self.chatTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}


- (void)configureFieldState {
    if (self.beginTranslate == YES) {
        self.translateFullViewConstraint.constant = 40;
        self.chatViewConstraint.constant = 40;
        self.beginTranslate = NO;
        [self updateContentViewFromStartValue:-40];
        [self.view layoutIfNeeded];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArray.count;
}

- (void)cellViewTapped:(UITapGestureRecognizer*)recognizer {
    CGPoint location = [recognizer locationInView:self.chatTableView];
    NSIndexPath *path = [self.chatTableView indexPathForRowAtPoint:location];
    UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:path];
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        [self cellViewTappedMyCell:recognizer];
    } else if ([cell isKindOfClass:[OtherMessageCell class]]) {
        [self cellViewTappedOtherCell:recognizer];
    }
}

- (void)cellViewTappedOtherCell:(UITapGestureRecognizer*)recognizer {
    CGPoint location = [recognizer locationInView:self.chatTableView];
    NSIndexPath *path = [self.chatTableView indexPathForRowAtPoint:location];
    OtherMessageCell *otherCell = [self.chatTableView cellForRowAtIndexPath:path];
    RLMMessage *message = [self.messageArray objectAtIndex:path.row];
    if (![otherCell.message.text isEqualToString:NSLocalizedString(@"Message deleted", nil)]) {
        if (otherCell.isTranslated == NO) {
            if ([self.user.userBuySubscribe isEqualToNumber:@(0)]) {
                [self showSubscribeView];
                return;
            }
            [self translateOtherMessage:otherCell];
        } else if (otherCell.isTranslated == YES) {
            otherCell.message.text = message.message;
            otherCell.isTranslated = NO;
        }
    }
}

- (void)translateOtherMessage:(OtherMessageCell*)otherCell {
    [[RequestManager sharedManager] translateText:otherCell.message.text  withSourceLang:self.user.myLangCode toParsLang:self.user.translateLangCode withSuccessBlock:^(NSDictionary *success) {
        otherCell.message.text = [success valueForKey:@"data"];
        otherCell.isTranslated = YES;
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

- (void)cellViewTappedMyCell:(UITapGestureRecognizer*)recognizer {
    CGPoint location = [recognizer locationInView:self.chatTableView];
    NSIndexPath *path = [self.chatTableView indexPathForRowAtPoint:location];
    MyMassegeCell *myCell = [self.chatTableView cellForRowAtIndexPath:path];
    RLMMessage *message = [self.messageArray objectAtIndex:path.row];
    if (![myCell.message.text isEqualToString:NSLocalizedString(@"Message deleted", nil)]) {
        if (myCell.isTranslated == NO) {
            if ([self.user.userBuySubscribe isEqualToNumber:@(0)]) {
                [self showSubscribeView];
                return;
            }
            [self translateMyMessage:myCell];
        } else if (myCell.isTranslated == YES) {
            myCell.message.text = message.message;
            myCell.isTranslated = NO;
        }
    }
}

- (void)translateMyMessage:(MyMassegeCell*)myCell {
    [[RequestManager sharedManager] translateText:myCell.message.text  withSourceLang:self.user.myLangCode toParsLang:self.user.translateLangCode withSuccessBlock:^(NSDictionary *success) {
        myCell.message.text = [success valueForKey:@"data"];
        myCell.isTranslated = YES;
    } onFalureBlock:^(NSInteger statusCode) {
        
    }];
}

//delegate method
- (void)translate {
    dispatch_queue_t _serialQueue = dispatch_queue_create("com.example.name", DISPATCH_QUEUE_SERIAL);
    dispatch_sync(_serialQueue, ^{
       [self close];
    });
    UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
    
    if ([self.user.userBuySubscribe isEqualToNumber:@(0)]) {
        [self showSubscribeView];
        return;
    }
    
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        [self translateMyMessage];
    } else {
        [self translateOtherMessage];
    }
}

- (void)translateMyMessage {
    MyMassegeCell *myCell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
    if (![myCell.message.text isEqualToString:NSLocalizedString(@"Message deleted", nil)]) {
        [[RequestManager sharedManager] translateText:myCell.message.text  withSourceLang:self.user.myLangCode toParsLang:self.user.translateLangCode withSuccessBlock:^(NSDictionary *success) {
            myCell.message.text = [success valueForKey:@"data"];
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    }
}

- (void)translateOtherMessage {
    OtherMessageCell *otherCell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
    if (![otherCell.message.text isEqualToString:NSLocalizedString(@"Message deleted", nil)]) {
        [[RequestManager sharedManager] translateText:otherCell.message.text  withSourceLang:self.user.myLangCode toParsLang:self.user.translateLangCode withSuccessBlock:^(NSDictionary *success) {
            otherCell.message.text = [success valueForKey:@"data"];
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    }
}

- (void)copy {
    UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        MyMassegeCell *myCell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
        [myCell.message copy:myCell.message];
    } else {
        OtherMessageCell *myCell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
        [myCell.message copy:myCell.message];
    }
    [self close];
}

- (void)change {
    UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
//    RLMMessage *message = [self.messageArray objectAtIndex:self.translateIndexPathRow.row];
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        MyMassegeCell *myCell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
        if ([self checkIsChange:myCell.message.text]) {
            self.chatTextView.textColor = [UIColor blackColor];
            self.chatTextView.text = myCell.message.text;
            [self.chatTextView becomeFirstResponder];
            self.changeWorld = YES;
            [self.chatTableView scrollToRowAtIndexPath:self.translateIndexPathRow atScrollPosition:UITableViewScrollPositionBottom animated:TRUE];
            [self.sendButton setEnabled:YES];
        }
    }
    
    [self close];
}

- (void)remove {
    UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:self.translateIndexPathRow];
    RLMMessage *message = [self.messageArray objectAtIndex:self.translateIndexPathRow.row];
    [self.chatTableView scrollToRowAtIndexPath:self.translateIndexPathRow atScrollPosition:UITableViewScrollPositionBottom animated:TRUE];
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        [[RequestManager sharedManager] removeMessageFromChat:self.chat.chatId messageId:message.mId withSuccessBlock:^(NSDictionary *success) {
            [self.messageArray removeObjectAtIndex:self.translateIndexPathRow.row];
            [self.chatTableView reloadData];
//            RLMMessage *message = self.messageArray[self.translateIndexPathRow.row];
//            [self.messageArray replaceObjectAtIndex:self.translateIndexPathRow.row withObject:message];
            [[RLMRealm defaultRealm] beginWriteTransaction];
            [self.chat.message removeObjectAtIndex:self.translateIndexPathRow.row];
            [[RLMRealm defaultRealm] commitWriteTransaction];
            RLMMessage *newLastMessage = [self.messageArray lastObject];
            [[RLMRealm defaultRealm] beginWriteTransaction];
            self.chat.lastMessage = newLastMessage.message;
            self.chat.sentAt = newLastMessage.time;
            [[RLMRealm defaultRealm] commitWriteTransaction];
        } onFalureBlock:^(NSInteger statusCode) {
            
        }];
    }
    
    [self close];
}

- (void)close {
    [self removePath];
    [self.actionSheet removeFromSuperview];
    [UIView animateWithDuration:0.3 animations:^{
        self.actionSheet.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {

    }];
}

- (void)removePath {
    NSArray *layers = [self.view.layer.sublayers copy];
    for (CALayer *layer in layers) {
        if ([layer.name isEqualToString:@"path"]) {
            [layer removeFromSuperlayer];
        }
    }
}

- (void)addPathToCell:(MyMassegeCell*)cell {
    self.cellPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height) cornerRadius:0];

    CGRect frame = [cell.messageView convertRect:cell.messageView.frame toView:self.view];

    self.circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(frame.origin.x - 48, frame.origin.y - 4, frame.size.width, frame.size.height) cornerRadius:10];
    [self.cellPath appendPath:self.circlePath];
    [self.cellPath setUsesEvenOddFillRule:YES];

    self.circleLayer = [CAShapeLayer layer];
    self.circleLayer.name = @"path";
    self.circleLayer.path = self.cellPath.CGPath;
    self.circleLayer.fillRule = kCAFillRuleEvenOdd;
    self.circleLayer.fillColor = [UIColor blackColor].CGColor;
    self.circleLayer.opacity = 0.5;
    [self.view.layer addSublayer:self.circleLayer];
}

- (void)addPathToOtherCell:(OtherMessageCell*)cell {
    self.cellPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height) cornerRadius:0];
    CGRect frame = [cell.messageView convertRect:cell.messageView.frame toView:super.view];

    CGRect screen = [[UIScreen mainScreen] bounds];
    self.circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(screen.size.width - 48 - cell.messageView.frame.size.width, frame.origin.y - 4, cell.messageView.frame.size.width, cell.messageView.frame.size.height) cornerRadius:10];
    [self.cellPath appendPath:self.circlePath];
    [self.cellPath setUsesEvenOddFillRule:YES];
    
    CAShapeLayer *fillLayer = [CAShapeLayer layer];
    fillLayer.name = @"path";
    fillLayer.path = self.cellPath.CGPath;
    fillLayer.fillRule = kCAFillRuleEvenOdd;
    fillLayer.fillColor = [UIColor blackColor].CGColor;
    fillLayer.opacity = 0.5;
    [self.view.layer addSublayer:fillLayer];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.chatTableView];
    NSIndexPath *indexPath = [self.chatTableView indexPathForRowAtPoint:p];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self dismissKeyboard];
        UITableViewCell *cell = [self.chatTableView cellForRowAtIndexPath:indexPath];
        MessageActionSheet *rootView = [self createMessageActionSheetFromIndexPathRow:indexPath];
        if ([cell isKindOfClass:[MyMassegeCell class]]) {
            MyMassegeCell *cell = [self.chatTableView cellForRowAtIndexPath:indexPath];
            [self createActionSheetFromCell:cell atIndexPath:indexPath];
        } else {
            OtherMessageCell *cell = [self.chatTableView cellForRowAtIndexPath:indexPath];
            [self createActionSheetFromCell:cell atIndexPath:indexPath];
        }
            [self.view addSubview:rootView];
    }
}

- (MessageActionSheet*)createMessageActionSheetFromIndexPathRow:(NSIndexPath*)indexPath {
    MessageActionSheet *rootView = [[[NSBundle mainBundle] loadNibNamed:@"MessageActionSheet" owner:self options:nil] objectAtIndex:0];
    rootView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    
    self.translateIndexPathRow = indexPath;
    self.actionSheet = rootView;
    self.actionSheet.delegate = self;
    
    return rootView;
}

-(void)createActionSheetFromCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath{
    cell = [self.chatTableView cellForRowAtIndexPath:indexPath];
    if ([cell isKindOfClass:[MyMassegeCell class]]) {
        MyMassegeCell *myCell = [self.chatTableView cellForRowAtIndexPath:indexPath];
        if ([self checkIsChange:myCell.message.text]) {
            [self pathToCell:cell];
        }
    } else if ([cell isKindOfClass:[OtherMessageCell class]]) {
        OtherMessageCell *otherCell = [self.chatTableView cellForRowAtIndexPath:indexPath];
        if ([self checkIsChange:otherCell.message.text]) {
            [self pathToCell:cell];
        }
    }
}

- (void)pathToCell:(UITableViewCell*)cell {
    [UIView animateWithDuration:0.3 animations:^{
        if ([cell isKindOfClass:[MyMassegeCell class]]) {
            [self.actionSheet showChange];
        } else if ([cell isKindOfClass:[OtherMessageCell class]]) {
            [self.actionSheet showChangeFromFriend];
        }
        self.actionSheet.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height);
        self.circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(100, 100, 100, 100) cornerRadius:100];
        if ([cell isKindOfClass:[MyMassegeCell class]]) {
            [self addPathToCell:(MyMassegeCell*)cell];
        } else if ([cell isKindOfClass:[OtherMessageCell class]]) {
            [self addPathToOtherCell:(OtherMessageCell*)cell];
        }
    }];
}

- (void)insertRowAtTop {
        self.paging = self.paging + 1;
        [self getChatListHistoryWithPaging:@(self.paging)];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RLMMessage *message =  self.messageArray[indexPath.row];
    UITableViewCell *cell;
    if ([message.role integerValue] == 1) {
        cell = [self myMessageCell:message fromIndexPath:indexPath];
    } else if ([message.role integerValue] == 2) {
        cell = [self otherMessageCell:message fromIndexPath:indexPath];;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (OtherMessageCell*)otherMessageCell:(RLMMessage*)message fromIndexPath:(NSIndexPath*)indexPath {
    OtherMessageCell *otherCell = (OtherMessageCell*)[self.chatTableView dequeueReusableCellWithIdentifier:@"OtherMessageCell"];
    [self configOtherCell:otherCell fromMessage:message];
    [self addTapFromTranslateToOtherCell:otherCell];
    [self addTapHendlerToOtherCell:otherCell fromIndexPath:indexPath];
    [self addLongPressFromShowMenuOtherCell:otherCell];
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", self.selectedFriend.name , self.selectedFriend.lastName];
    [otherCell.avatarView setAvatar:self.selectedFriend.avatar orInitialsView:fullName gradientColorsArray:(NSArray*)self.selectedFriend.gradient];
    [otherCell.avatarView setNeedsDisplay];
    return otherCell;
}

- (MyMassegeCell*)myMessageCell:(RLMMessage*)message fromIndexPath:(NSIndexPath*)indexPath {
    MyMassegeCell *myCell = (MyMassegeCell*)[self.chatTableView dequeueReusableCellWithIdentifier:@"MyMassegeCell"];
    [self configMyCell:myCell fromMessage:message];
    [self addTapHendlerToCell:myCell fromIndexPath:indexPath];
    [self addTapFromTranslateToCell:myCell];
    [self addLongPressFromShowMenu:myCell];
    [self configIsSend:myCell fromMessage:message];
    [myCell.avatarView setAvatar:self.user.avatar orInitialsView:self.user.name];
    return myCell;
}

- (void)configOtherCell:(OtherMessageCell*)cell fromMessage:(RLMMessage*)message{
    cell.messageView.layer.cornerRadius = 10;
    cell.message.text = message.message;
    cell.time.text = message.time;
    [cell.message setUserInteractionEnabled:YES];
    [cell.message setMinimumPressDuration:50];
}

- (void)addTapHendlerToOtherCell:(OtherMessageCell*)cell fromIndexPath:(NSIndexPath*)indexPath {
    KILinkTapHandler tapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        [self tappedLink:string cellForRowAtIndexPath:indexPath];
    };
    cell.message.userHandleLinkTapHandler = tapHandler;
    cell.message.urlLinkTapHandler = tapHandler;
    cell.message.hashtagLinkTapHandler = tapHandler;
    cell.message.delegate = self;
}

- (void)addTapFromTranslateToOtherCell:(OtherMessageCell*)cell {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellViewTapped:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [tapRecognizer setCancelsTouchesInView:NO];
    [cell.messageView addGestureRecognizer:tapRecognizer];
}

- (void)addLongPressFromShowMenuOtherCell:(OtherMessageCell*)cell {
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [lpgr setCancelsTouchesInView:NO];
    [cell.messageView addGestureRecognizer:lpgr];
}

- (void)configIsSend:(MyMassegeCell*)cell fromMessage:(RLMMessage*)message{
    if (message.isSend == YES) {
        [cell.acceptSendMessage setImage:[UIImage imageNamed:@"Check"]];
    } else {
        [cell.acceptSendMessage setImage:[UIImage imageNamed:@"CheckSolo"]];
    }
}

- (void)configMyCell:(MyMassegeCell*)cell fromMessage:(RLMMessage*)message{
    cell.messageView.layer.cornerRadius = 10;
    cell.message.text = message.message;
    cell.time.text = message.time;
    [cell.message setUserInteractionEnabled:YES];
    [cell.message setMinimumPressDuration:50];
}

- (void)addTapHendlerToCell:(MyMassegeCell*)cell fromIndexPath:(NSIndexPath*)indexPath {
    KILinkTapHandler tapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        [self tappedLink:string cellForRowAtIndexPath:indexPath];
    };
    cell.message.userHandleLinkTapHandler = tapHandler;
    cell.message.urlLinkTapHandler = tapHandler;
    cell.message.hashtagLinkTapHandler = tapHandler;
    cell.message.delegate = self;
}

- (void)addTapFromTranslateToCell:(MyMassegeCell*)cell {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cellViewTapped:)];
    tapRecognizer.numberOfTapsRequired = 1;
    [tapRecognizer setCancelsTouchesInView:NO];
    [cell.messageView addGestureRecognizer:tapRecognizer];
}

- (void)addLongPressFromShowMenu:(MyMassegeCell*)cell {
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [lpgr setCancelsTouchesInView:NO];
    [cell.messageView addGestureRecognizer:lpgr];
}

- (void)tappedLink:(NSString *)link cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link] options:@{} completionHandler:nil];
}

- (void)addShafowToView:(UIView *)view {
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.4f;
    view.layer.shadowOffset = CGSizeMake(0.2f, 0.2f);
    view.layer.shadowRadius = 1.0f;
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    [UIAlertController showAlertInViewController:self withTitle:tittle message:message cancelButtonTitle:@"OK" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
}

- (NSString*)convert:(NSString*)date fromNeedDateFormat:(NSString*)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [NSDate new];
    if (date == nil) {
        dateFromString = [NSDate date];
    } else {
        dateFromString = [dateFormatter dateFromString:date];
    }
    
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger sec = [tz secondsFromGMTForDate: dateFromString];
    dateFromString = [NSDate dateWithTimeInterval: sec sinceDate: dateFromString];
    
    if ([format isEqualToString:@"EEEE, MMM d, yyyy"]) {
        [dateFormatter setDateFormat:@"EEEE, MMM d, yyyy"];
    } else if ([format isEqualToString:@"HH:mm:ss"]) {
        [dateFormatter setDateFormat:@"HH:mm:ss"];
    }
    
    return [dateFormatter stringFromDate:dateFromString];
}

- (NSString*)convert:(NSString*)date fromServerDateFormat:(NSString*)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *dateFromString = [NSDate new];
    dateFromString = [dateFormatter dateFromString:date];
//    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
//    NSInteger sec = [tz secondsFromGMTForDate: dateFromString];
//    dateFromString = [NSDate dateWithTimeInterval: sec sinceDate: dateFromString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [dateFormatter stringFromDate:dateFromString];
}

@end
