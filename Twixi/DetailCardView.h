//
//  DetailCardView.h
//  Twixi
//
//  Created by macOS on 06.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Speech/Speech.h>

@interface DetailCardView : UIView <AVSpeechSynthesizerDelegate>

@property (nonatomic, strong) AVSpeechSynthesizer *synthesizer;
@property (weak, nonatomic) IBOutlet UIButton *sound;
@property (weak, nonatomic) IBOutlet UILabel *word;
@property (weak, nonatomic) IBOutlet UILabel *translate;
@property (weak, nonatomic) IBOutlet UILabel *translateViewWord;
@property (weak, nonatomic) IBOutlet UILabel *translateViewTranslate;
@property (weak, nonatomic) IBOutlet UIButton *showMoreButton;
@property (weak, nonatomic) IBOutlet UIButton *translateButton;
@property (weak, nonatomic) IBOutlet UIButton *showMoreTranslateButton;
@property (weak, nonatomic) IBOutlet UIView *bcgView;
@property (weak, nonatomic) IBOutlet UIView *translateView;

@property (weak, nonatomic) IBOutlet UILabel *errorTranlsateCountLabel;


@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *bcgViews;
@property (nonatomic, strong) IBOutletCollection(UIView) NSArray *translateViews;

@property (nonatomic, strong) NSArray *tranlsatersObjects;


@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *tranlsaters;


@property (weak, nonatomic) IBOutlet UIView *flipedView;

@property (weak, nonatomic) IBOutlet UILabel *pretextLabel;//Pretext
@property (weak, nonatomic) IBOutlet UILabel *verbLabel;//Verb
@property (weak, nonatomic) IBOutlet UILabel *adverbLabel;//Adverb
@property (weak, nonatomic) IBOutlet UILabel *adjectiveLabel;//Adjective

@property (weak, nonatomic) IBOutlet UILabel *trnsPretextLabel;//Pretext
@property (weak, nonatomic) IBOutlet UILabel *trnsVerbLabel;//Verb
@property (weak, nonatomic) IBOutlet UILabel *trnsAdverbLabel;//Adverb
@property (weak, nonatomic) IBOutlet UILabel *trnsAdjectiveLabel;//Adjective

//Show example

@property BOOL beginTranslate;

@property BOOL isFliped;
@property int countTranslate;

- (void)customize;

+ (NSArray*)testCardViewsfromView:(UIView*)view;

+ (NSArray*)viewsWithContent:(NSArray*)contentArray fromView:(UIView*)view;

@end
