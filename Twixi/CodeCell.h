//
//  CodeCell.h
//  Twixi
//
//  Created by macOS on 03.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CodeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *codeName;
@property (weak, nonatomic) IBOutlet UILabel *codeValue;
@property (weak, nonatomic) IBOutlet UIImageView *flag;

@end
