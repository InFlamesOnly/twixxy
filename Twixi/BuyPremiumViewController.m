//
//  BuyPremiumViewController.m
//  Twixi
//
//  Created by macOS on 05.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "BuyPremiumViewController.h"
#import "UIView+ShadowToButton.h"
#import <PassKit/PassKit.h>
#import "PCAngularActivityIndicatorView.h"
#import <Stripe.h>
#import "RLMSubscribe.h"
#import "RequestManager.h"
#import <FBSDKAppEvents.h>


static NSString *merchantId = @"merchant.com.Twixi.pay";


@interface BuyPremiumViewController () <PKPaymentAuthorizationViewControllerDelegate, STPAddCardViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *firstPremiumButton;
@property (weak, nonatomic) IBOutlet UIButton *secondPremiumButton;

@property (strong, nonatomic) PCAngularActivityIndicatorView *activity;

@property (weak, nonatomic) IBOutlet PKPaymentButton *paymentButton;
@property BOOL paymentSucceeded;

@property NSString *price;


@end

@implementation BuyPremiumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.paymentButton setHidden:YES];
    self.paymentButton.enabled = [Stripe deviceSupportsApplePay];
    self.secondPremiumButton.layer.borderWidth = 1;
    self.secondPremiumButton.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)addActivityIndicator {
    self.activity = [[PCAngularActivityIndicatorView alloc] initWithActivityIndicatorStyle:PCAngularActivityIndicatorViewStyleLarge];
    
    self.activity.center = [self.view convertPoint:self.view.center fromView:self.view.superview];
    self.activity.color = [UIColor colorWithRed:61.0f/255.0f green:75.0f/255.0f blue:168.0f/255.0f alpha:1];
    [self.activity startAnimating];
    [self.view addSubview:self.activity];
}

-(void)viewDidLayoutSubviews {
    [UIView addGradientToLoginButton:self.firstPremiumButton];
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)buyFirst:(id)sender {
    [self addActivityIndicator];
    self.price = @"12";
    [self handleApplePayButtonTappedWithPrice:@"12"];

    NSLog(@"TAP TO FIRST");
}

- (IBAction)buySecond:(id)sender {
    self.price = @"15";
    [self handleApplePayButtonTappedWithPrice:@"15"];
    NSLog(@"TAP TO SECOND");
}

- (void)handleApplePayButtonTappedWithPrice:(NSString*)price {
    NSString *merchantIdentifier = merchantId;
    PKPaymentRequest *paymentRequest = [Stripe paymentRequestWithMerchantIdentifier:merchantIdentifier country:@"UA" currency:@"usd"];
    paymentRequest.paymentSummaryItems = @[
                                           [PKPaymentSummaryItem summaryItemWithLabel:@"TWIXI" amount:[NSDecimalNumber decimalNumberWithString:price]]
                                           ];
    if ([Stripe canSubmitPaymentRequest:paymentRequest]) {
        // Setup payment authorization view controller
        PKPaymentAuthorizationViewController *paymentAuthorizationViewController = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
        paymentAuthorizationViewController.delegate = self;
        
        // Present payment authorization view controller
        [self presentViewController:paymentAuthorizationViewController animated:YES completion:nil];
    }
    else {
        // There is a problem with your Apple Pay configuration
    }
    // Continued in next step
}
//[FBSDKAppEvents logPurchase:4.32 currency:@"USD"];
- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller didAuthorizePayment:(PKPayment *)payment completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    [[STPAPIClient sharedClient] createTokenWithPayment:payment completion:^(STPToken *token, NSError *error) {
        if (token == nil || error != nil) {
            self.paymentSucceeded = NO;
            // Present error to user...
            return;
        } else {
            
            //TODO реализовать подписку через экран настроек
//            if ([RLMSubscribe allObjects].count > 0) {
                [[RequestManager sharedManager] subscribeWithStripeToken:token.tokenId stripeId:@"yeae" successBlock:^(NSDictionary *success) {
                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                    f.numberStyle = NSNumberFormatterDecimalStyle;
                    NSNumber *myNumber = [f numberFromString:self.price];
                    double price = [myNumber doubleValue];
                    [FBSDKAppEvents logPurchase:price currency:@"USD"];
                } onFalureBlock:^(NSInteger statusCode) {
                    
                }];
//            }
            self.paymentSucceeded = YES;
        }
        
    }];
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.paymentSucceeded) {
            NSLog(@"PAYMEN SUCCES");
            [self showAllertViewWithTittle:@"Ура" andMessage:@"Тестовоая подписка оформлена"];
        } else {
            NSLog(@"PAYMEN FILURE");
        }
    }];
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller didSelectPaymentMethod:(PKPaymentMethod *)paymentMethod completion:(void (^)(NSArray<PKPaymentSummaryItem *> * _Nonnull))completion {
    NSLog(@"didSelectPaymentMethod");
    [self.activity removeFromSuperview];
    completion(@[]);
}

-(void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller didSelectShippingContact:(PKContact *)contact completion:(void (^)(PKPaymentAuthorizationStatus, NSArray<PKShippingMethod *> * _Nonnull, NSArray<PKPaymentSummaryItem *> * _Nonnull))completion {
    NSLog(@"didSelectShippingContact");
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller didSelectShippingMethod:(PKShippingMethod *)shippingMethod completion:(void (^)(PKPaymentAuthorizationStatus, NSArray<PKPaymentSummaryItem *> * _Nonnull))completion {
    NSLog(@"didSelectShippingMethod");
}

- (void)paymentAuthorizationViewControllerWillAuthorizePayment:(PKPaymentAuthorizationViewController *)controller {
    NSLog(@"paymentAuthorizationViewControllerWillAuthorizePayment");
}

- (void)showAllertViewWithTittle:(NSString*) tittle andMessage:(NSString*) message {
    UIAlertController *alertController = [UIAlertController new];
    
    alertController = [UIAlertController alertControllerWithTitle:tittle message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:alertController animated:YES completion:NULL];
}


@end
