//
//  NSString+TelegramNSDateString.m
//  Twixi
//
//  Created by macOS on 23.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import "NSString+TelegramNSDateString.h"

@implementation NSString (TelegramNSDateString)

+ (NSString*)convertResponseToCellsTimeString:(NSString*)dateString {

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [dateFormatter dateFromString:dateString];
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger sec = [tz secondsFromGMTForDate: dateFromString];
    dateFromString = [NSDate dateWithTimeInterval: sec sinceDate: dateFromString];
    NSString *elapsed;
    //---------------------------------------------------------------------------------------------------------------------------------------------
//    NSDate *date = [NSDate dateWithTimestamp:timestamp];
    NSTimeInterval seconds = [[NSDate date] timeIntervalSinceDate:dateFromString];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    if (seconds < 60)
    {
        elapsed = NSLocalizedString(@"Just now", nil);
    }
    else if (seconds < 60 * 60)
    {
        int minutes = (int) (seconds / 60);
        elapsed = [NSString stringWithFormat:@"%@ %d %@",NSLocalizedString(@"last seen", nil), minutes, (minutes > 1) ? NSLocalizedString(@"mins ago", nil) : NSLocalizedString(@"min ago", nil)];
    }
    else if (seconds < 24 * 60 * 60)
    {
        int hours = (int) (seconds / (60 * 60));
        elapsed = [NSString stringWithFormat:@"%@ %d %@",NSLocalizedString(@"last seen", nil), hours, (hours > 1) ? NSLocalizedString(@"hours ago", nil) : NSLocalizedString(@"hour ago", nil)];
    }
    else if (seconds < 7 * 24 * 60 * 60)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"eeee"];
        NSString *data = [[formatter stringFromDate:dateFromString] lowercaseString];
        elapsed = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"last seen", nil),data];
//        elapsed = [formatter stringFromDate:dateFromString];
        
        
    }
    else elapsed = [NSDateFormatter localizedStringFromDate:dateFromString dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    return elapsed;
}



@end
