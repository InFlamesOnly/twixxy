//
//  NSString+TelegramNSDateString.h
//  Twixi
//
//  Created by macOS on 23.04.18.
//  Copyright © 2018 macOS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TelegramNSDateString)

+ (NSString*)convertResponseToCellsTimeString:(NSString*)dateString;

@end
